/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src@4.0/assets/js/main.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src@4.0/assets/js/main.js":
/*!***********************************!*\
  !*** ./src@4.0/assets/js/main.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

// ajax 配合JQ3 引入設置
$(document).ready(function () {
  var el = document.querySelector('html[lang="zh-tw"]');

  if (el) {
    if ($("#header")) {
      $.ajax({
        url: "ajax/_header.html",
        method: "GET",
        dataType: "html"
      }).done(function (data) {
        $("#header").html(data);
        headerFunction();
      });
    }

    if ($("#sideLink")) {
      $.ajax({
        url: "ajax/_sideLink.html",
        method: "GET",
        dataType: "html"
      }).done(function (data) {
        $("#sideLink").html(data);
        toggleSideLink();
      });
    }

    if ($("#footer")) {
      $.ajax({
        url: "ajax/_footer.html",
        method: "GET",
        dataType: "html"
      }).done(function (data) {
        $("#footer").html(data);
        goTop();
        goTopSticky();
      });
    }
  }
}); //英文版的ajax

$(document).ready(function () {
  var el = document.querySelector('html[lang="en"]');

  if (el) {
    if ($("#headerEn")) {
      $.ajax({
        url: "../ajax/_header_en.html",
        method: "GET",
        dataType: "html"
      }).done(function (data) {
        $("#headerEn").html(data);
        headerFunction();
      });
    }

    if ($("#sideLinkEn")) {
      $.ajax({
        url: "../ajax/_sideLink_en.html",
        method: "GET",
        dataType: "html"
      }).done(function (data) {
        $("#sideLinkEn").html(data);
        toggleSideLink();
      });
    }

    if ($("#footerEn")) {
      $.ajax({
        url: "../ajax/_footer_en.html",
        method: "GET",
        dataType: "html"
      }).done(function (data) {
        $("#footerEn").html(data);
        goTop();
        goTopSticky();
      });
    }
  }
});

function toolsListener() {
  window.addEventListener("keydown", function (e) {
    if (e.keyCode === 9) {
      document.body.classList.remove("js-useMouse");
      document.body.classList.add("js-useKeyboard");
    }
  });
  window.addEventListener("mousedown", function (e) {
    document.body.classList.remove("js-useKeyboard");
    document.body.classList.add("js-useMouse");
  });
}

function toggleSideLink() {
  var elTw = document.querySelector('html[lang="zh-tw"]');
  var elEn = document.querySelector('html[lang="en"]');
  document.querySelector("#sideLinkToggleBtn").addEventListener("click", function () {
    this.classList.toggle("js-sideLinkOpened");

    if (elTw) {
      document.querySelector("#sideLink").classList.toggle("js-sideLinkOpened");
    }

    if (elEn) {
      document.querySelector("#sideLinkEn").classList.toggle("js-sideLinkOpened");
    }

    document.querySelector("#page").classList.toggle("js-sideLinkOpened");
    document.querySelector("body").classList.toggle("overflow-hidden"); //打開sideMenu時，把展開的menu關掉

    document.documentElement.classList.remove("js-menuOpened");
    document.querySelector(".l-header-hamburger").classList.remove("js-menuOpened");
    document.querySelector(".l-header-menu").classList.remove("js-menuOpened"); //打開sideMenu時，把gotop推到旁邊去

    document.querySelector("#sideLinkFooter").classList.toggle("js-sideLinkOpened");
  });
  window.addEventListener("resize", function () {
    if (window.innerWidth >= 1200) {
      document.querySelector("#sideLinkToggleBtn").classList.remove("js-sideLinkOpened");

      if (elTw) {
        document.querySelector("#sideLink").classList.remove("js-sideLinkOpened");
      }

      if (elEn) {
        document.querySelector("#sideLinkEn").classList.remove("js-sideLinkOpened");
      }

      document.querySelector("#page").classList.remove("js-sideLinkOpened");
      document.querySelector("body").classList.remove("overflow-hidden"); //[移除]打開sideMenu時，把gotop推到旁邊去

      document.querySelector("#sideLinkFooter").classList.remove("js-sideLinkOpened");
    }
  });
}

function goTop() {
  document.querySelector("#goTop").onclick = function (e) {
    e.preventDefault();
    $("html, body").animate({
      scrollTop: 0
    }, 500);
  };
}

function goTopSticky() {
  var el = document.querySelector("#sideLinkFooter"); //計算footer目前高度

  var footerHeightVar = document.querySelector(".l-footer").clientHeight; // console.log(`footerHeightVar + ${footerHeightVar}`);
  //當下網頁高度(整頁長)

  var scrollHeightVar = document.documentElement.scrollHeight; // console.log(`scrollHeightVar + ${scrollHeightVar}`);
  //當下畫面高度(window height)

  var windowHeightVar = window.innerHeight; // console.log(`windowHeightVar + ${windowHeightVar}`);
  //捲動中的畫面最上端與網頁頂端之間的距離

  var scrollTopVar = document.documentElement.scrollTop; // console.log(`scrollTopVar + ${scrollTopVar}`);
  // console.log(windowHeightVar + scrollTopVar + footerHeightVar);
  //如果整頁長==畫面高度+捲動高度

  if (el) {
    if (scrollHeightVar <= windowHeightVar + scrollTopVar + footerHeightVar) {
      el.classList.remove("js-sticky");
    } else {
      el.classList.add("js-sticky");
    }
  }
}

function goToAnchor() {
  var headerHeight = "";
  window.addEventListener("resize", function () {
    return headerHeight = $(".l-header").height();
  });
  $('.js-goToAnchor').click(function (e) {
    e.preventDefault();
    var target = $(this).attr('href');
    var targetPos = $(target).offset().top;
    var headerHeight = $(".l-header").height();
    $('html,body').animate({
      scrollTop: targetPos - headerHeight
    }, 1000);
    var trigger02 = document.querySelector("#hamburger");
    var target02 = document.querySelector("#menu");
    trigger02.classList.remove("js-menuOpened");
    target02.classList.remove("js-menuOpened");
    document.documentElement.classList.remove("js-menuOpened");
  });
} //風琴用function


function Accordion(el, target, siblings) {
  this.el = el;
  this.target = target;
  this.siblings = siblings;
}

Accordion.prototype.init = function () {
  var triggers = document.querySelectorAll(this.el);
  var target = this.target;
  var siblings = this.siblings;
  Array.prototype.slice.call(triggers).forEach(function (trigger) {
    trigger.addEventListener("click", function () {
      event.preventDefault();

      if (siblings == true) {
        if (this.querySelector(target) !== null) {
          // this.querySelector(target).classList.toggle(
          //   "js-accordionExpended"
          // );
          //為了要能在打開一個時，其他的收合，這邊改用jQuery==>
          $(this).find(target).toggleClass("js-accordionExpended"); // 增加藍線框效果

          $(this).toggleClass("c-accordion-btn-focus");
          $(this).siblings().find(target).removeClass("js-accordionExpended"); // 增加藍線框效果

          $(this).siblings().removeClass("c-accordion-btn-focus");
        } // document.querySelector(trigger.getAttribute("data-target")).classList.toggle("js-accordionExpended");
        //為了要能在打開一個時，其他的收合，這邊改用jQuery==>


        var att = $(trigger).attr("data-target");
        $(att).toggleClass("js-accordionExpended").siblings().removeClass("js-accordionExpended"); // $(`${trigger.getAttribute("data-target")}`).toggleClass("js-accordionExpended").siblings().removeClass("js-accordionExpended");
        //用了jQuery之後，這組會被樓上那個綁在一起，所以可以省略
        // trigger.classList.toggle("js-accordionExpended");
      } else {
        if (this.querySelector(target) !== null) {
          this.querySelector(target).classList.toggle("js-accordionExpended");
        }

        document.querySelector(trigger.getAttribute("data-target")).classList.toggle("js-accordionExpended");
        trigger.classList.toggle("js-accordionExpended");
      }
    });
  });
};

function nodeListToArray(nodeListCollection) {
  return Array.prototype.slice.call(nodeListCollection);
} // const nodeListToArray = (nodeListCollection) => Array.prototype.slice.call(nodeListCollection);

/**
 * 
 * @param {*} el :class name
 * @param {*} target :被影響到的目標
 * @param {*} mediaQuery :斷點設定
 * @說明 "el"與"target" toggle 一個叫js-active的class要用出什麼效果，端看你怎麼寫js-active的css效果展開或收合什麼東西
 */


function toggleVisiable(el, target, mediaQuery) {
  var triggers = document.querySelectorAll(el);
  var target = document.querySelector(target);

  if (target) {
    nodeListToArray(triggers).forEach(function (trigger) {
      trigger.addEventListener("click", function () {
        event.preventDefault();
        this.classList.toggle("js-active");
        target.classList.toggle("js-active");
        var hasMediaQuery = mediaQuery;

        if (hasMediaQuery !== "") {
          var isMobile = window.innerWidth < mediaQuery;

          if (isMobile) {
            document.documentElement.classList.toggle("js-functionMenuOpened");
          }
        } else {
          document.documentElement.classList.remove("js-functionMenuOpened");
        }

        window.addEventListener("resize", function () {
          if (window.innerWidth >= mediaQuery) {
            document.documentElement.classList.remove("js-functionMenuOpened");
          }
        });
      });
    });
  }
}
/*el=觸發對象(開關)*/

/*target=被控制的物件(燈)*/


function clickConfirm(el, target) {
  var triggers = document.querySelectorAll(el);
  var target = document.querySelector(target);

  if (target) {
    nodeListToArray(triggers).forEach(function (trigger) {
      trigger.addEventListener("click", function () {
        event.preventDefault();
        target.classList.remove("js-active");
        document.documentElement.classList.remove("js-functionMenuOpened");
      });
    });
  }
} //↑↑↑通用function---------------------


function createAccordion() {
  if (document.querySelector('.c-accordion-btn')) {
    new Accordion(".c-accordion-btn", ".c-accordion-btn-icon", true).init();
  }
} // TODO: 想辦法這個全域變數要處理一下


var str = 0;

function showAllTab() {
  if (window.innerWidth > 992 && str == 1) {
    $(".tab-pane").removeClass("show active").first().addClass("show active");
    $(".c-tab-linkE").removeClass("active").parent().first().find(".c-tab-linkE").addClass("active");
    str = 0;
  }

  if (window.innerWidth < 992 && str == 0) {
    $(".tab-pane").addClass("show active");
    str = 1;
  } // console.log(str);

}

function togglepFilterSubmenu() {
  // TODO: 有空試試看寫一個手機版可以針對內容高度增加showmore，並且記錄此showmore已經點開過，不會因為rwd就又計算一次
  //手機版顯示全部
  if (document.querySelector('.p-products-search-showMore')) {
    new Accordion(".p-products-search-showMore", undefined, false).init();
  }

  var triggers = document.querySelectorAll(".c-accordion-btn");
  nodeListToArray(triggers).forEach(function (trigger) {
    var el = document.querySelector(trigger.getAttribute("data-target"));
    trigger.addEventListener("click", function () {
      if (el.querySelector(".p-products-search-showMore")) {
        el.querySelector(".p-products-search-showMore").classList.remove("js-accordionExpended");
        el.querySelector(".c-accordion-content-layout").classList.remove("js-accordionExpended");
      }
    });
  }); //手機版開關選單
  //商品專區

  toggleVisiable(".close", ".p-products-search", "");
  toggleVisiable(".p-products-search-btn", ".p-products-search", 992);
  clickConfirm("#js-confirm", ".p-products-search"); //門市查詢

  toggleVisiable(".v-dropdown-btn", ".v-dropdown-menu", 992);
  toggleVisiable(".close", ".v-dropdown-menu", "");
  clickConfirm("#js-confirm", ".v-dropdown-menu"); // if ($(".v-dropdown-btn").hasClass("js-active")) {
  //   document.querySelector("body").addEventListener("click", function () {
  //     document.querySelector(".v-dropdown-btn").classList.remove("js-active");
  //     document.querySelector(".v-dropdown-menu").classList.remove("js-active");
  //   });
  // }
}

function confirmIfDoubleMenuOpened(el, mediaQuery) {
  if (window.innerWidth < mediaQuery && $(el).hasClass("js-active") && document.documentElement.classList == "") {
    document.documentElement.classList.add("js-menuOpened");
  }
} // 輪播相關--------------------------


function Slick(el, slidesToShow, slidesPerRow, rows, responsive) {
  this.el = el;
  this.slidesToShow = slidesToShow;
  this.slidesPerRow = slidesPerRow;
  this.rows = rows;
  this.responsive = responsive;
}

;

Slick.prototype.init = function () {
  $(this.el + " .v-slick").slick({
    arrows: true,
    slidesToShow: this.slidesToShow,
    slidesPerRow: this.slidesPerRow,
    rows: this.rows,
    prevArrow: "<button type=\"button\" class=\"v-slick-arrows-prev\"><i class=\"fal fa-angle-left\"></i></button>",
    nextArrow: "<button type=\"button\" class=\"v-slick-arrows-next\"><i class=\"fal fa-angle-right\"></i></button>",
    // autoplay: true,
    // lazyLoad: "progressive",
    responsive: this.responsive
  });
};

function setStoreTableHeightAtMobile() {
  triggers = document.querySelectorAll('.c-shopTable-rwd-verA tbody tr');
  nodeListToArray(triggers).forEach(function (trigger) {
    var triggerTd = trigger.querySelectorAll('td');
    var num01 = triggerTd[0].clientHeight;
    var num02 = triggerTd[1].clientHeight;
    var num03 = triggerTd[2].clientHeight;
    var totalNum = num01 + num02 + num03;

    if (window.innerWidth < 1200 && trigger.classList.contains('js-active') == false) {
      trigger.style.height = "".concat(totalNum, "px");
    } else if (window.innerWidth > 1200) {
      trigger.style.height = "";
      trigger.classList.remove('js-active');
      trigger.querySelector("i").classList.remove("js-submenuOpened");
    }

    window.addEventListener("resize", function () {
      if (window.innerWidth < 1200 && trigger.classList.contains('js-active') == false) {
        trigger.style.height = "".concat(totalNum, "px");
      } else if (window.innerWidth > 1200) {
        trigger.style.height = "";
        trigger.classList.remove('js-active');
        trigger.querySelector("i").classList.remove("js-submenuOpened");
      }
    });
  });
}

function toggleStoreTableHeightAtMobile() {
  triggers = document.querySelectorAll('.c-shopTable-rwd-verA tbody tr');
  nodeListToArray(triggers).forEach(function (trigger) {
    var triggerTd = trigger.querySelectorAll('td');
    var num01 = triggerTd[0].clientHeight;
    var num02 = triggerTd[1].clientHeight;
    var num03 = triggerTd[2].clientHeight;
    var totalNum = num01 + num02 + num03;
    trigger.querySelector("td:first-child").addEventListener('click', function () {
      if (window.innerWidth < 1200 && trigger.style.height == "") {
        trigger.style.height = "".concat(totalNum, "px");
        trigger.classList.remove('js-active');
        trigger.querySelector("i").classList.remove("js-submenuOpened");
      } else {
        trigger.style.height = "";
        trigger.classList.add("js-active");
        trigger.querySelector("i").classList.add("js-submenuOpened");
      }
    });
  });
} // TODO: 有空看一下為什麼new第三個就出事了

var videoSlick;

function createSlicks() {
  if (document.querySelector(".v-slick")) {
    var targets = ["#news", "#recommandation"];
 
    targets.forEach(function (target) {
      new Slick(target, 4, 1, 1, [{
        breakpoint: 992,
        settings: {
          slidesToShow: 2
        }
      }, {
        breakpoint: 576,
        settings: {
          slidesToShow: 2,
          arrows: false
        }
      }]).init();
    });
     
   
    videoSlick = new Slick("#video", 1, 2, 2, [{
      breakpoint: 992,
      settings: {
        slidesToShow: 2,
        slidesPerRow: 1,
        rows: 1
      }
    }, {
      breakpoint: 576,
      settings: {
        slidesToShow: 2,
        slidesPerRow: 1,
        rows: 1,
        arrows: false
      }
    }]).init();
  }
}

function rionetSlick() {
  var el = ".v-rionetSlick";

  if (document.querySelector(el)) {
    $(el).slick({
      // centerMode: true,
      // centerPadding: '60px',
      slidesToShow: 4,
      arrows: true,
      prevArrow: "<button type=\"button\" class=\"v-tabBtnSlick-prev\"><i class=\"fal fa-angle-left\"></i></button>",
      nextArrow: "<button type=\"button\" class=\"v-tabBtnSlick-next\"><i class=\"fal fa-angle-right\"></i></button>",
      lazyLoad: "progressive",
      responsive: [{
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
          dots: true
        }
      }, {
        breakpoint: 576,
        settings: {
          slidesToShow: 2,
          arrows: false,
          // variableWidth: true,
          dots: true
        }
      }]
    }); //   $('.v-tabBtnSlick-prev').on('click', function(){
    //     $('.v-tabBtnSlick').slick('slickPrev');
    //  });
    //   $('.v-tabBtnSlick-next').on('click', function(){
    //     $('.v-tabBtnSlick').slick('slickNext');
    //  });
  }
}

function tabBtnSlick() {
  var el = ".v-tabBtnSlick";

  if (document.querySelector(el)) {
    var tabLength = document.querySelector(el).querySelectorAll('.c-tab-container').length;
    var slidesToShowNum = tabLength > 6 ? 6 : tabLength; // console.log("slidesToShowNum = " + slidesToShowNum);

    $(el).slick({
      slidesToShow: slidesToShowNum,
      slidesToScroll: 1,
      arrows: true,
      prevArrow: "<button type=\"button\" class=\"v-tabBtnSlick-prev\"><i class=\"fal fa-angle-left\"></i></button>",
      nextArrow: "<button type=\"button\" class=\"v-tabBtnSlick-next\"><i class=\"fal fa-angle-right\"></i></button>",
      lazyLoad: "progressive",
      infinite: false,
      responsive: [{
        breakpoint: 1200,
        settings: {
          slidesToShow: 4,
          variableWidth: false // slidesToScroll: 5,
          // infinite: true,
          // centerMode: false,

        }
      }, {
        breakpoint: 992,
        settings: {
          slidesToShow: 4,
          variableWidth: false // infinite: true,
          // centerMode: false,

        }
      }, {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          variableWidth: true,
          // infinite: false,
          arrows: false
        }
      }]
    });
  }
}

function tabBtnSlickFixed() {
  var el = ".v-tabBtnSlick";

  if (document.querySelector(el)) {
    var tabLength = document.querySelector(el).querySelectorAll('.c-tab-container').length; // var slidesToShowNum = (tabLength > 6) ? 6 : tabLength;
    // console.log(tabLength);

    if (window.innerWidth >= 1200 && tabLength <= 6) {
      $(".slick-track").addClass("text-md-center"); // document.querySelector('.slick-track').classList.add('text-md-center');
    }

    if (window.innerWidth >= 768 && window.innerWidth < 1200 && tabLength < 5) {
      $(".slick-track").addClass("text-md-center"); // document.querySelector('.slick-track').classList.add('text-md-center');
    } // if (window.innerWidth < 768) {
    // document.querySelector('.slick-track').classList.remove('text-md-center');
    // }

  }
}

function tabBtnSlickMembers() {
  var el = ".v-tabBtnSlick-members";

  if (document.querySelector(el)) {
    $(el).slick({
      slidesToShow: 6,
      slidesToScroll: 1,
      arrows: true,
      prevArrow: "<button type=\"button\" class=\"v-tabBtnSlick-prev\"><i class=\"fal fa-angle-left\"></i></button>",
      nextArrow: "<button type=\"button\" class=\"v-tabBtnSlick-next\"><i class=\"fal fa-angle-right\"></i></button>",
      lazyLoad: "progressive",
      infinite: false,
      responsive: [{
        breakpoint: 992,
        settings: {
          slidesToShow: 5,
          variableWidth: false
        }
      }, {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
          variableWidth: true,
          arrows: false
        }
      }]
    });
  }
}

function tabBtnSlickFixedMembers() {
  var el = ".v-tabBtnSlick-members";

  if (document.querySelector(el)) {
    if (window.innerWidth >= 768) {
      $(".v-tabBtnSlick-members .slick-track").addClass("text-md-center");
    }
  }
} // End 輪播相關--------------------------


function warrantyLink() {
  if (document.querySelectorAll(".p-index-warranty-link").length) {
    var triggers = document.querySelectorAll(".p-index-warranty-link");
    nodeListToArray(triggers).forEach(function (trigger) {
      trigger.addEventListener("mouseover", function () {
        trigger.querySelector(".c-btn").classList.add("js-btnHover");
      });
      trigger.addEventListener("mouseout", function () {
        trigger.querySelector(".c-btn").classList.remove("js-btnHover");
      });
    });
  }
} //手機版漢堡選單


function toggleMobileMenu(mediaQuery) {
  var trigger = document.querySelector("#hamburger");
  var target = document.querySelector("#menu");
  trigger.addEventListener("click", function () {
    this.classList.toggle("js-menuOpened");
    target.classList.toggle("js-menuOpened");
    document.documentElement.classList.toggle("js-menuOpened");
  });
  window.addEventListener("resize", function () {
    if (window.innerWidth >= mediaQuery) {
      trigger.classList.remove("js-menuOpened");
      target.classList.remove("js-menuOpened");
      document.documentElement.classList.remove("js-menuOpened");
    }
  });
} //手機版風琴折疊選單


function toggleMobileSubmenu(mediaQuery) {
  var triggers = document.querySelectorAll(".l-header-menu-link");
  var targets = document.querySelectorAll(".l-header-submenu--hidden");
  nodeListToArray(triggers).forEach(function (trigger) {
    if (trigger.nextElementSibling !== null) {
      trigger.addEventListener("click", function (e) {
        e.preventDefault();
        var target = trigger.nextElementSibling;

        if (window.innerWidth < mediaQuery) {
          target.classList.toggle("js-submenuOpened");
          trigger.querySelector("i").classList.toggle("js-submenuOpened"); //為了開一個關一個，用jQuery加寫

          $(target).parents().siblings().find(".l-header-submenu , i").removeClass("js-submenuOpened");
        }
      });
    }
  });
  window.addEventListener("resize", function () {
    if (window.innerWidth >= mediaQuery) {
      nodeListToArray(triggers).forEach(function (trigger) {
        var target = trigger.nextElementSibling;
        target.classList.remove("js-submenuOpened");
        trigger.querySelector("i").classList.remove("js-submenuOpened");
      });
    }
  });
}

function sibingMobileSubmenu() {
  $(".l-header-menu-item").on("click", function () {
    $(this).siblings().find(".l-header-submenu").removeClass("js-submenuOpened");
  });
}

function togglePcHoverState(mediaQuery) {
  var triggers = document.querySelectorAll(".l-header-menu-item");
  nodeListToArray(triggers).forEach(function (trigger) {
    if (trigger.querySelector(".l-header-submenu") !== null) {
      trigger.addEventListener("mouseover", function () {
        trigger.querySelector(".l-header-menu-link").classList.add("js-hover");
      });
      trigger.addEventListener("mouseout", function () {
        trigger.querySelector(".l-header-menu-link").classList.remove("js-hover");
      });
    }
  });
  window.addEventListener("resize", function () {
    if (window.innerWidth < mediaQuery) {
      Array.prototype.slice.call(document.querySelectorAll(".l-header-menu-link")).forEach(function (item) {
        item.classList.remove("js-hover");
      });
    }
  });
}

function headerFunction() {
  var breakpoint = 1200;
  toggleMobileMenu(breakpoint);
  toggleMobileSubmenu(breakpoint);
  togglePcHoverState(breakpoint);
}

function lazyLoad() {
  if (document.querySelector("img[data-src]")) {
    var observer = lozad();
    observer.observe();

    if (-1 !== navigator.userAgent.indexOf("MSIE") || navigator.appVersion.indexOf("Trident/") > 0) {
      var iframe = document.querySelector("iframe");
      iframe.setAttribute("src", "https://www.youtube.com/embed/O7pNpR3Py68");
    }
  }
} //計算「最新消息」、「人氣推薦」這類的卡片列表的標題字數，讓大家的高度一樣


function autoFixHeight(con) {
  var el = document.querySelectorAll(con);
  var thisHeight = -1;
  var maxHeight = -1;
  var breakpoint = 768; //為了ie不支援nodelist的forEach修正

  nodeListToArray(el).forEach(function (item) {
    item.style.height = ""; //清空之前的style

    thisHeight = item.clientHeight; //取得已經減過高度的

    maxHeight = maxHeight > thisHeight ? maxHeight : thisHeight;
  });

  if (document.body.clientWidth > breakpoint) {
    nodeListToArray(el).forEach(function (item) {
      item.style.height = "".concat(maxHeight, "px");
    });
  }
}

function textHide(num, con) {
  var el = document.querySelectorAll(con); //ie才執行這個超過字數增加刪節號，其他瀏覽器靠css語法即可
  // if (
  //   navigator.userAgent.indexOf("MSIE") !== -1 ||
  //   navigator.appVersion.indexOf("Trident/") > 0
  // ) {
  //   Array.prototype.slice.call(el).forEach(function (item) {
  //     var txt = item.innerText;
  //     if (txt.length > num) {
  //       txtContent = txt.substring(0, num - 1) + "...";
  //       item.innerHTML = txtContent;
  //     }
  //   });
  // }

  nodeListToArray(el).forEach(function (item) {
    var txt = item.innerText;

    if (txt.length > num) {
      txtContent = txt.substring(0, num - 1) + "...";
      item.innerHTML = txtContent;
    } else {
      txtContent = txt.substring(0, num) + "...";
      item.innerHTML = txtContent;
    }
  });
  autoFixHeight(con);
}

function clearCheckBox(el, target) {
  if (document.querySelector(el)) {
    var trigger = document.querySelector(el);
    var targets = document.querySelectorAll(target);
    trigger.addEventListener("click", function () {
      event.preventDefault();
      trigger.blur();
      Array.prototype.slice.call(targets).forEach(function (trigger) {
        trigger.checked = false;
      });
    });
  }
}

function cardContentFunction() {
  if (document.querySelector(".c-card-body-title") !== null) {
    autoFixHeight(".c-card-body-title");
  } //人氣推薦內文字數限制


  if (document.querySelector(".c-card-body-txt") !== null) {
    textHide(48, ".c-card-body-txt");
  }
}

function toolTips() {
  $('[data-toggle="tooltip"]').tooltip({
    placement: 'right',
    trigger: 'hover',
    offset: 30
  });
  $('[data-toggle="tooltip"]').on('inserted.bs.tooltip', function () {
    var getTootipsId = "#" + $(this).attr("aria-describedby"); // console.log(getTootipsId);
    // console.log($(this).attr("data-original-title") == "德國蔡司數位影像系統");

    if ($(this).attr("data-original-title") == "德國蔡司數位影像系統") {
      $(getTootipsId).find('.tooltip-inner').html("德國蔡司<br>數位影像系統");
    }

    if ($(this).attr("data-original-title") == "法國依視路全方位視覺檢測系統") {
      $(getTootipsId).find('.tooltip-inner').html("法國依視路<br>全方位視覺檢測系統");
    }
  });
}

function aos() {
  if (document.querySelector("[data-aos]")) {
    AOS.init({
      once: true
    });
  }
}

function ytHandler() {
  if (document.querySelector("[data-url]")) {
    var triggers = document.querySelectorAll("[data-url]");
    var target = document.querySelector("iframe");
    
    //對應的ad key 可以自行塞入你要的key 
    var index= 0;
    
    Array.prototype.slice.call(triggers).forEach(function (trigger) {
      //塞入對應的AD key值
      trigger.setAttribute("index", index++);
      
      //ad被點擊事件
      trigger.onclick = function () {
    	
    	/*=====此處 為抓DB AD廣告圖片資料=====
    	var triggerIndex = trigger.getAttribute("index"); 抓取ad key data
        $.get("url",{}, function(result){
    	   $("#slick_ad").html("請塞入廣告圖片html資料");
      	   $('#slick_ad').slick('slickGoTo', 0);
        })
    	========================*/
    	  
    	 
    	var triggerIndex = trigger.getAttribute("index");
    	$('#slick_ad').slick('slickGoTo', triggerIndex);
        
    	target.setAttribute("src", trigger.getAttribute("data-url"));
        //alert(+trigger.getAttribute("data-url"));
        trigger.classList.add("js-active");
        Array.prototype.slice.call(triggers).filter(function (item) {
          return item !== trigger;
        }).forEach(function (item) {
          item.classList.remove("js-active");
        });
      };
    });
  }
}

function cardJustifyContent() {
  var el = $(".js-cardJustifyContent");

  if (el) {
    for (var i = 0; i < el.length; i++) {
      var elChildrenLength = el.eq(i).children('div').length; // console.log(elChildrenLength);

      if (elChildrenLength <= 2) {
        el.eq(i).addClass("justify-content-center");
      } else {
        el.eq(i).removeClass("justify-content-center");
      }
    }
  }
}

function storeFilterNotification(inputContainer, targetEl) {
  var el = document.querySelector(inputContainer);
  var target = document.querySelector(targetEl);

  if (el) {
    // console.log(inputContainer + " + " + target);
    var triggers = el.querySelectorAll("input[type='checkbox']"); // console.log(triggers);

    Array.prototype.slice.call(triggers).forEach(function (trigger) {
      trigger.addEventListener("click", function () {
        var checkedNum = el.querySelectorAll("input[type=checkbox]:checked").length; // console.log(checkedNum);

        if (checkedNum > 0) {
          target.classList.add("js-inputChecked");
        } else {
          target.classList.remove("js-inputChecked");
        }
      });
    });
    var clearAllBtnEl = document.querySelector("#js-clearCheckBoxes");
    clearAllBtnEl.addEventListener("click", function () {
      target.classList.remove("js-inputChecked");
    });
  }
} //滾軸式日期選單


function dateMobiscroll() {
  if ($('#date')) {
    $('#date').mobiscroll().date({
      theme: $.mobiscroll.defaults.theme,
      // Specify theme like: theme: 'ios' or omit setting to use default 
      mode: 'mixed',
      // Specify scroller mode like: mode: 'mixed' or omit setting to use default 
      display: 'modal',
      // Specify display mode like: display: 'bottom' or omit setting to use default 
      lang: 'zh' // Specify language like: lang: 'pl' or omit setting to use default 

    });
  }
} //下拉選單跳轉超連結


function selectURL() {
  var el = document.querySelector(".js-selectURL");

  if (el) {
    el.addEventListener("change", function () {
      // window.open(this.options[this.selectedIndex].value);
      location.href = this.options[this.selectedIndex].value; // console.log(this.options[this.selectedIndex].value);
    });
  }
} //呼叫function-網頁載入完成後


$(document).ready(function () {
  selectURL();
  toolsListener();
  createSlicks();
  tabBtnSlick();
  tabBtnSlickFixed(); //會員權益專用

  tabBtnSlickMembers();
  tabBtnSlickFixedMembers();
  rionetSlick();
  warrantyLink();
  lazyLoad();
  createAccordion(); // createVideoSwiper();

  aos();
  ytHandler();
  togglepFilterSubmenu();
  clearCheckBox("#js-clearCheckBoxes", "input[type='checkbox']");
  setStoreTableHeightAtMobile(); // $('[data-toggle="popover"]').popover();

  toolTips();
  cardJustifyContent();
  storeFilterNotification(".v-dropdown-menu", ".v-dropdown-btn");
  storeFilterNotification(".p-products-search", ".p-products-search-btn");
  toggleStoreTableHeightAtMobile();
  dateMobiscroll();
  goToAnchor(); // goTopSticky();
});

window.onload = function () {
  cardContentFunction();
  showAllTab();
}; //呼叫function-視窗大小變更


$(window).resize(function () {
  cardContentFunction();
  showAllTab(); // setStoreTableHeightAtMobile();

  confirmIfDoubleMenuOpened(".p-products-search", 992);
  tabBtnSlickFixed(); // goToAnchor();
}); //呼叫function-捲動

$(window).scroll(function () {
  goTopSticky(); // sideLinkSticky();
});

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vc3JjQDQuMC9hc3NldHMvanMvbWFpbi5qcyJdLCJuYW1lcyI6WyIkIiwiZG9jdW1lbnQiLCJyZWFkeSIsImVsIiwicXVlcnlTZWxlY3RvciIsImFqYXgiLCJ1cmwiLCJtZXRob2QiLCJkYXRhVHlwZSIsImRvbmUiLCJkYXRhIiwiaHRtbCIsImhlYWRlckZ1bmN0aW9uIiwidG9nZ2xlU2lkZUxpbmsiLCJnb1RvcCIsImdvVG9wU3RpY2t5IiwidG9vbHNMaXN0ZW5lciIsIndpbmRvdyIsImFkZEV2ZW50TGlzdGVuZXIiLCJlIiwia2V5Q29kZSIsImJvZHkiLCJjbGFzc0xpc3QiLCJyZW1vdmUiLCJhZGQiLCJlbFR3IiwiZWxFbiIsInRvZ2dsZSIsImRvY3VtZW50RWxlbWVudCIsImlubmVyV2lkdGgiLCJvbmNsaWNrIiwicHJldmVudERlZmF1bHQiLCJhbmltYXRlIiwic2Nyb2xsVG9wIiwiZm9vdGVySGVpZ2h0VmFyIiwiY2xpZW50SGVpZ2h0Iiwic2Nyb2xsSGVpZ2h0VmFyIiwic2Nyb2xsSGVpZ2h0Iiwid2luZG93SGVpZ2h0VmFyIiwiaW5uZXJIZWlnaHQiLCJzY3JvbGxUb3BWYXIiLCJnb1RvQW5jaG9yIiwiaGVhZGVySGVpZ2h0IiwiaGVpZ2h0IiwiY2xpY2siLCJ0YXJnZXQiLCJhdHRyIiwidGFyZ2V0UG9zIiwib2Zmc2V0IiwidG9wIiwidHJpZ2dlcjAyIiwidGFyZ2V0MDIiLCJBY2NvcmRpb24iLCJzaWJsaW5ncyIsInByb3RvdHlwZSIsImluaXQiLCJ0cmlnZ2VycyIsInF1ZXJ5U2VsZWN0b3JBbGwiLCJBcnJheSIsInNsaWNlIiwiY2FsbCIsImZvckVhY2giLCJ0cmlnZ2VyIiwiZXZlbnQiLCJmaW5kIiwidG9nZ2xlQ2xhc3MiLCJyZW1vdmVDbGFzcyIsImF0dCIsImdldEF0dHJpYnV0ZSIsIm5vZGVMaXN0VG9BcnJheSIsIm5vZGVMaXN0Q29sbGVjdGlvbiIsInRvZ2dsZVZpc2lhYmxlIiwibWVkaWFRdWVyeSIsImhhc01lZGlhUXVlcnkiLCJpc01vYmlsZSIsImNsaWNrQ29uZmlybSIsImNyZWF0ZUFjY29yZGlvbiIsInN0ciIsInNob3dBbGxUYWIiLCJmaXJzdCIsImFkZENsYXNzIiwicGFyZW50IiwidG9nZ2xlcEZpbHRlclN1Ym1lbnUiLCJ1bmRlZmluZWQiLCJjb25maXJtSWZEb3VibGVNZW51T3BlbmVkIiwiaGFzQ2xhc3MiLCJTbGljayIsInNsaWRlc1RvU2hvdyIsInNsaWRlc1BlclJvdyIsInJvd3MiLCJyZXNwb25zaXZlIiwic2xpY2siLCJhcnJvd3MiLCJwcmV2QXJyb3ciLCJuZXh0QXJyb3ciLCJzZXRTdG9yZVRhYmxlSGVpZ2h0QXRNb2JpbGUiLCJ0cmlnZ2VyVGQiLCJudW0wMSIsIm51bTAyIiwibnVtMDMiLCJ0b3RhbE51bSIsImNvbnRhaW5zIiwic3R5bGUiLCJ0b2dnbGVTdG9yZVRhYmxlSGVpZ2h0QXRNb2JpbGUiLCJjcmVhdGVTbGlja3MiLCJ0YXJnZXRzIiwiYnJlYWtwb2ludCIsInNldHRpbmdzIiwicmlvbmV0U2xpY2siLCJsYXp5TG9hZCIsImRvdHMiLCJ0YWJCdG5TbGljayIsInRhYkxlbmd0aCIsImxlbmd0aCIsInNsaWRlc1RvU2hvd051bSIsInNsaWRlc1RvU2Nyb2xsIiwiaW5maW5pdGUiLCJ2YXJpYWJsZVdpZHRoIiwidGFiQnRuU2xpY2tGaXhlZCIsInRhYkJ0blNsaWNrTWVtYmVycyIsInRhYkJ0blNsaWNrRml4ZWRNZW1iZXJzIiwid2FycmFudHlMaW5rIiwidG9nZ2xlTW9iaWxlTWVudSIsInRvZ2dsZU1vYmlsZVN1Ym1lbnUiLCJuZXh0RWxlbWVudFNpYmxpbmciLCJwYXJlbnRzIiwic2liaW5nTW9iaWxlU3VibWVudSIsIm9uIiwidG9nZ2xlUGNIb3ZlclN0YXRlIiwiaXRlbSIsIm9ic2VydmVyIiwibG96YWQiLCJvYnNlcnZlIiwibmF2aWdhdG9yIiwidXNlckFnZW50IiwiaW5kZXhPZiIsImFwcFZlcnNpb24iLCJpZnJhbWUiLCJzZXRBdHRyaWJ1dGUiLCJhdXRvRml4SGVpZ2h0IiwiY29uIiwidGhpc0hlaWdodCIsIm1heEhlaWdodCIsImNsaWVudFdpZHRoIiwidGV4dEhpZGUiLCJudW0iLCJ0eHQiLCJpbm5lclRleHQiLCJ0eHRDb250ZW50Iiwic3Vic3RyaW5nIiwiaW5uZXJIVE1MIiwiY2xlYXJDaGVja0JveCIsImJsdXIiLCJjaGVja2VkIiwiY2FyZENvbnRlbnRGdW5jdGlvbiIsInRvb2xUaXBzIiwidG9vbHRpcCIsInBsYWNlbWVudCIsImdldFRvb3RpcHNJZCIsImFvcyIsIkFPUyIsIm9uY2UiLCJ5dEhhbmRsZXIiLCJhbGVydCIsImZpbHRlciIsImNhcmRKdXN0aWZ5Q29udGVudCIsImkiLCJlbENoaWxkcmVuTGVuZ3RoIiwiZXEiLCJjaGlsZHJlbiIsInN0b3JlRmlsdGVyTm90aWZpY2F0aW9uIiwiaW5wdXRDb250YWluZXIiLCJ0YXJnZXRFbCIsImNoZWNrZWROdW0iLCJjbGVhckFsbEJ0bkVsIiwiZGF0ZU1vYmlzY3JvbGwiLCJtb2Jpc2Nyb2xsIiwiZGF0ZSIsInRoZW1lIiwiZGVmYXVsdHMiLCJtb2RlIiwiZGlzcGxheSIsImxhbmciLCJzZWxlY3RVUkwiLCJsb2NhdGlvbiIsImhyZWYiLCJvcHRpb25zIiwic2VsZWN0ZWRJbmRleCIsInZhbHVlIiwib25sb2FkIiwicmVzaXplIiwic2Nyb2xsIl0sIm1hcHBpbmdzIjoiO1FBQUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7OztRQUdBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwwQ0FBMEMsZ0NBQWdDO1FBQzFFO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0Esd0RBQXdELGtCQUFrQjtRQUMxRTtRQUNBLGlEQUFpRCxjQUFjO1FBQy9EOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSx5Q0FBeUMsaUNBQWlDO1FBQzFFLGdIQUFnSCxtQkFBbUIsRUFBRTtRQUNySTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDJCQUEyQiwwQkFBMEIsRUFBRTtRQUN2RCxpQ0FBaUMsZUFBZTtRQUNoRDtRQUNBO1FBQ0E7O1FBRUE7UUFDQSxzREFBc0QsK0RBQStEOztRQUVySDtRQUNBOzs7UUFHQTtRQUNBOzs7Ozs7Ozs7Ozs7QUNsRkE7QUFDQUEsQ0FBQyxDQUFDQyxRQUFELENBQUQsQ0FBWUMsS0FBWixDQUFrQixZQUFZO0FBQzVCLE1BQUlDLEVBQUUsR0FBR0YsUUFBUSxDQUFDRyxhQUFULENBQXVCLG9CQUF2QixDQUFUOztBQUNBLE1BQUlELEVBQUosRUFBUTtBQUNOLFFBQUlILENBQUMsQ0FBQyxTQUFELENBQUwsRUFBa0I7QUFDaEJBLE9BQUMsQ0FBQ0ssSUFBRixDQUFPO0FBQ0xDLFdBQUcsRUFBRSxtQkFEQTtBQUVMQyxjQUFNLEVBQUUsS0FGSDtBQUdMQyxnQkFBUSxFQUFFO0FBSEwsT0FBUCxFQUlHQyxJQUpILENBSVEsVUFBVUMsSUFBVixFQUFnQjtBQUN0QlYsU0FBQyxDQUFDLFNBQUQsQ0FBRCxDQUFhVyxJQUFiLENBQWtCRCxJQUFsQjtBQUNBRSxzQkFBYztBQUNmLE9BUEQ7QUFRRDs7QUFDRCxRQUFJWixDQUFDLENBQUMsV0FBRCxDQUFMLEVBQW9CO0FBQ2xCQSxPQUFDLENBQUNLLElBQUYsQ0FBTztBQUNMQyxXQUFHLEVBQUUscUJBREE7QUFFTEMsY0FBTSxFQUFFLEtBRkg7QUFHTEMsZ0JBQVEsRUFBRTtBQUhMLE9BQVAsRUFJR0MsSUFKSCxDQUlRLFVBQVVDLElBQVYsRUFBZ0I7QUFDdEJWLFNBQUMsQ0FBQyxXQUFELENBQUQsQ0FBZVcsSUFBZixDQUFvQkQsSUFBcEI7QUFDQUcsc0JBQWM7QUFDZixPQVBEO0FBUUQ7O0FBQ0QsUUFBSWIsQ0FBQyxDQUFDLFNBQUQsQ0FBTCxFQUFrQjtBQUNoQkEsT0FBQyxDQUFDSyxJQUFGLENBQU87QUFDTEMsV0FBRyxFQUFFLG1CQURBO0FBRUxDLGNBQU0sRUFBRSxLQUZIO0FBR0xDLGdCQUFRLEVBQUU7QUFITCxPQUFQLEVBSUdDLElBSkgsQ0FJUSxVQUFVQyxJQUFWLEVBQWdCO0FBQ3RCVixTQUFDLENBQUMsU0FBRCxDQUFELENBQWFXLElBQWIsQ0FBa0JELElBQWxCO0FBQ0FJLGFBQUs7QUFDTEMsbUJBQVc7QUFDWixPQVJEO0FBU0Q7QUFDRjtBQUVGLENBcENELEUsQ0FxQ0E7O0FBQ0FmLENBQUMsQ0FBQ0MsUUFBRCxDQUFELENBQVlDLEtBQVosQ0FBa0IsWUFBWTtBQUM1QixNQUFJQyxFQUFFLEdBQUdGLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QixpQkFBdkIsQ0FBVDs7QUFDQSxNQUFJRCxFQUFKLEVBQVE7QUFDTixRQUFJSCxDQUFDLENBQUMsV0FBRCxDQUFMLEVBQW9CO0FBQ2xCQSxPQUFDLENBQUNLLElBQUYsQ0FBTztBQUNMQyxXQUFHLEVBQUUseUJBREE7QUFFTEMsY0FBTSxFQUFFLEtBRkg7QUFHTEMsZ0JBQVEsRUFBRTtBQUhMLE9BQVAsRUFJR0MsSUFKSCxDQUlRLFVBQVVDLElBQVYsRUFBZ0I7QUFDdEJWLFNBQUMsQ0FBQyxXQUFELENBQUQsQ0FBZVcsSUFBZixDQUFvQkQsSUFBcEI7QUFDQUUsc0JBQWM7QUFDZixPQVBEO0FBUUQ7O0FBQ0QsUUFBSVosQ0FBQyxDQUFDLGFBQUQsQ0FBTCxFQUFzQjtBQUNwQkEsT0FBQyxDQUFDSyxJQUFGLENBQU87QUFDTEMsV0FBRyxFQUFFLDJCQURBO0FBRUxDLGNBQU0sRUFBRSxLQUZIO0FBR0xDLGdCQUFRLEVBQUU7QUFITCxPQUFQLEVBSUdDLElBSkgsQ0FJUSxVQUFVQyxJQUFWLEVBQWdCO0FBQ3RCVixTQUFDLENBQUMsYUFBRCxDQUFELENBQWlCVyxJQUFqQixDQUFzQkQsSUFBdEI7QUFDQUcsc0JBQWM7QUFDZixPQVBEO0FBUUQ7O0FBQ0QsUUFBSWIsQ0FBQyxDQUFDLFdBQUQsQ0FBTCxFQUFvQjtBQUNsQkEsT0FBQyxDQUFDSyxJQUFGLENBQU87QUFDTEMsV0FBRyxFQUFFLHlCQURBO0FBRUxDLGNBQU0sRUFBRSxLQUZIO0FBR0xDLGdCQUFRLEVBQUU7QUFITCxPQUFQLEVBSUdDLElBSkgsQ0FJUSxVQUFVQyxJQUFWLEVBQWdCO0FBQ3RCVixTQUFDLENBQUMsV0FBRCxDQUFELENBQWVXLElBQWYsQ0FBb0JELElBQXBCO0FBQ0FJLGFBQUs7QUFDTEMsbUJBQVc7QUFDWixPQVJEO0FBU0Q7QUFDRjtBQUNGLENBbkNEOztBQXFDQSxTQUFTQyxhQUFULEdBQXlCO0FBQ3ZCQyxRQUFNLENBQUNDLGdCQUFQLENBQXdCLFNBQXhCLEVBQW1DLFVBQVVDLENBQVYsRUFBYTtBQUM5QyxRQUFJQSxDQUFDLENBQUNDLE9BQUYsS0FBYyxDQUFsQixFQUFxQjtBQUNuQm5CLGNBQVEsQ0FBQ29CLElBQVQsQ0FBY0MsU0FBZCxDQUF3QkMsTUFBeEIsQ0FBK0IsYUFBL0I7QUFDQXRCLGNBQVEsQ0FBQ29CLElBQVQsQ0FBY0MsU0FBZCxDQUF3QkUsR0FBeEIsQ0FBNEIsZ0JBQTVCO0FBQ0Q7QUFDRixHQUxEO0FBTUFQLFFBQU0sQ0FBQ0MsZ0JBQVAsQ0FBd0IsV0FBeEIsRUFBcUMsVUFBVUMsQ0FBVixFQUFhO0FBQ2hEbEIsWUFBUSxDQUFDb0IsSUFBVCxDQUFjQyxTQUFkLENBQXdCQyxNQUF4QixDQUErQixnQkFBL0I7QUFDQXRCLFlBQVEsQ0FBQ29CLElBQVQsQ0FBY0MsU0FBZCxDQUF3QkUsR0FBeEIsQ0FBNEIsYUFBNUI7QUFDRCxHQUhEO0FBSUQ7O0FBRUQsU0FBU1gsY0FBVCxHQUEwQjtBQUN4QixNQUFJWSxJQUFJLEdBQUd4QixRQUFRLENBQUNHLGFBQVQsQ0FBdUIsb0JBQXZCLENBQVg7QUFDQSxNQUFJc0IsSUFBSSxHQUFHekIsUUFBUSxDQUFDRyxhQUFULENBQXVCLGlCQUF2QixDQUFYO0FBQ0FILFVBQVEsQ0FBQ0csYUFBVCxDQUF1QixvQkFBdkIsRUFBNkNjLGdCQUE3QyxDQUE4RCxPQUE5RCxFQUF1RSxZQUFZO0FBQ2pGLFNBQUtJLFNBQUwsQ0FBZUssTUFBZixDQUFzQixtQkFBdEI7O0FBQ0EsUUFBSUYsSUFBSixFQUFVO0FBQ1J4QixjQUFRLENBQUNHLGFBQVQsQ0FBdUIsV0FBdkIsRUFBb0NrQixTQUFwQyxDQUE4Q0ssTUFBOUMsQ0FBcUQsbUJBQXJEO0FBQ0Q7O0FBQ0QsUUFBSUQsSUFBSixFQUFVO0FBQ1J6QixjQUFRLENBQUNHLGFBQVQsQ0FBdUIsYUFBdkIsRUFBc0NrQixTQUF0QyxDQUFnREssTUFBaEQsQ0FBdUQsbUJBQXZEO0FBQ0Q7O0FBQ0QxQixZQUFRLENBQUNHLGFBQVQsQ0FBdUIsT0FBdkIsRUFBZ0NrQixTQUFoQyxDQUEwQ0ssTUFBMUMsQ0FBaUQsbUJBQWpEO0FBQ0ExQixZQUFRLENBQUNHLGFBQVQsQ0FBdUIsTUFBdkIsRUFBK0JrQixTQUEvQixDQUF5Q0ssTUFBekMsQ0FBZ0QsaUJBQWhELEVBVGlGLENBVWpGOztBQUNBMUIsWUFBUSxDQUFDMkIsZUFBVCxDQUF5Qk4sU0FBekIsQ0FBbUNDLE1BQW5DLENBQTBDLGVBQTFDO0FBQ0F0QixZQUFRLENBQUNHLGFBQVQsQ0FBdUIscUJBQXZCLEVBQThDa0IsU0FBOUMsQ0FBd0RDLE1BQXhELENBQStELGVBQS9EO0FBQ0F0QixZQUFRLENBQUNHLGFBQVQsQ0FBdUIsZ0JBQXZCLEVBQXlDa0IsU0FBekMsQ0FBbURDLE1BQW5ELENBQTBELGVBQTFELEVBYmlGLENBY2pGOztBQUNBdEIsWUFBUSxDQUFDRyxhQUFULENBQXVCLGlCQUF2QixFQUEwQ2tCLFNBQTFDLENBQW9ESyxNQUFwRCxDQUEyRCxtQkFBM0Q7QUFDRCxHQWhCRDtBQWlCQVYsUUFBTSxDQUFDQyxnQkFBUCxDQUF3QixRQUF4QixFQUFrQyxZQUFZO0FBQzVDLFFBQUlELE1BQU0sQ0FBQ1ksVUFBUCxJQUFxQixJQUF6QixFQUErQjtBQUM3QjVCLGNBQVEsQ0FBQ0csYUFBVCxDQUF1QixvQkFBdkIsRUFBNkNrQixTQUE3QyxDQUF1REMsTUFBdkQsQ0FBOEQsbUJBQTlEOztBQUNBLFVBQUlFLElBQUosRUFBVTtBQUNSeEIsZ0JBQVEsQ0FBQ0csYUFBVCxDQUF1QixXQUF2QixFQUFvQ2tCLFNBQXBDLENBQThDQyxNQUE5QyxDQUFxRCxtQkFBckQ7QUFDRDs7QUFDRCxVQUFJRyxJQUFKLEVBQVU7QUFDUnpCLGdCQUFRLENBQUNHLGFBQVQsQ0FBdUIsYUFBdkIsRUFBc0NrQixTQUF0QyxDQUFnREMsTUFBaEQsQ0FBdUQsbUJBQXZEO0FBQ0Q7O0FBQ0R0QixjQUFRLENBQUNHLGFBQVQsQ0FBdUIsT0FBdkIsRUFBZ0NrQixTQUFoQyxDQUEwQ0MsTUFBMUMsQ0FBaUQsbUJBQWpEO0FBQ0F0QixjQUFRLENBQUNHLGFBQVQsQ0FBdUIsTUFBdkIsRUFBK0JrQixTQUEvQixDQUF5Q0MsTUFBekMsQ0FBZ0QsaUJBQWhELEVBVDZCLENBVTdCOztBQUNBdEIsY0FBUSxDQUFDRyxhQUFULENBQXVCLGlCQUF2QixFQUEwQ2tCLFNBQTFDLENBQW9EQyxNQUFwRCxDQUEyRCxtQkFBM0Q7QUFDRDtBQUNGLEdBZEQ7QUFlRDs7QUFFRCxTQUFTVCxLQUFULEdBQWlCO0FBQ2ZiLFVBQVEsQ0FBQ0csYUFBVCxDQUF1QixRQUF2QixFQUFpQzBCLE9BQWpDLEdBQTJDLFVBQVVYLENBQVYsRUFBYTtBQUN0REEsS0FBQyxDQUFDWSxjQUFGO0FBQ0EvQixLQUFDLENBQUMsWUFBRCxDQUFELENBQWdCZ0MsT0FBaEIsQ0FBd0I7QUFDdEJDLGVBQVMsRUFBRTtBQURXLEtBQXhCLEVBR0UsR0FIRjtBQUtELEdBUEQ7QUFRRDs7QUFFRCxTQUFTbEIsV0FBVCxHQUF1QjtBQUNyQixNQUFJWixFQUFFLEdBQUdGLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QixpQkFBdkIsQ0FBVCxDQURxQixDQUVyQjs7QUFDQSxNQUFJOEIsZUFBZSxHQUFHakMsUUFBUSxDQUFDRyxhQUFULENBQXVCLFdBQXZCLEVBQW9DK0IsWUFBMUQsQ0FIcUIsQ0FJckI7QUFDQTs7QUFDQSxNQUFJQyxlQUFlLEdBQUduQyxRQUFRLENBQUMyQixlQUFULENBQXlCUyxZQUEvQyxDQU5xQixDQU9yQjtBQUNBOztBQUNBLE1BQUlDLGVBQWUsR0FBR3JCLE1BQU0sQ0FBQ3NCLFdBQTdCLENBVHFCLENBVXJCO0FBQ0E7O0FBQ0EsTUFBSUMsWUFBWSxHQUFHdkMsUUFBUSxDQUFDMkIsZUFBVCxDQUF5QkssU0FBNUMsQ0FacUIsQ0FhckI7QUFDQTtBQUNBOztBQUNBLE1BQUk5QixFQUFKLEVBQVE7QUFDTixRQUFJaUMsZUFBZSxJQUFJRSxlQUFlLEdBQUdFLFlBQWxCLEdBQWlDTixlQUF4RCxFQUF5RTtBQUN2RS9CLFFBQUUsQ0FBQ21CLFNBQUgsQ0FBYUMsTUFBYixDQUFvQixXQUFwQjtBQUNELEtBRkQsTUFFTztBQUNMcEIsUUFBRSxDQUFDbUIsU0FBSCxDQUFhRSxHQUFiLENBQWlCLFdBQWpCO0FBQ0Q7QUFDRjtBQUNGOztBQUNELFNBQVNpQixVQUFULEdBQXNCO0FBQ3BCLE1BQUlDLFlBQVksR0FBRyxFQUFuQjtBQUNBekIsUUFBTSxDQUFDQyxnQkFBUCxDQUF3QixRQUF4QixFQUFrQyxZQUFZO0FBQzVDLFdBQU93QixZQUFZLEdBQUcxQyxDQUFDLENBQUMsV0FBRCxDQUFELENBQWUyQyxNQUFmLEVBQXRCO0FBQ0QsR0FGRDtBQUdBM0MsR0FBQyxDQUFDLGdCQUFELENBQUQsQ0FBb0I0QyxLQUFwQixDQUEwQixVQUFVekIsQ0FBVixFQUFhO0FBQ3JDQSxLQUFDLENBQUNZLGNBQUY7QUFDQSxRQUFJYyxNQUFNLEdBQUc3QyxDQUFDLENBQUMsSUFBRCxDQUFELENBQVE4QyxJQUFSLENBQWEsTUFBYixDQUFiO0FBQ0EsUUFBSUMsU0FBUyxHQUFHL0MsQ0FBQyxDQUFDNkMsTUFBRCxDQUFELENBQVVHLE1BQVYsR0FBbUJDLEdBQW5DO0FBQ0EsUUFBSVAsWUFBWSxHQUFHMUMsQ0FBQyxDQUFDLFdBQUQsQ0FBRCxDQUFlMkMsTUFBZixFQUFuQjtBQUNBM0MsS0FBQyxDQUFDLFdBQUQsQ0FBRCxDQUFlZ0MsT0FBZixDQUF1QjtBQUNyQkMsZUFBUyxFQUFFYyxTQUFTLEdBQUdMO0FBREYsS0FBdkIsRUFFRyxJQUZIO0FBR0EsUUFBSVEsU0FBUyxHQUFHakQsUUFBUSxDQUFDRyxhQUFULENBQXVCLFlBQXZCLENBQWhCO0FBQ0EsUUFBSStDLFFBQVEsR0FBR2xELFFBQVEsQ0FBQ0csYUFBVCxDQUF1QixPQUF2QixDQUFmO0FBQ0E4QyxhQUFTLENBQUM1QixTQUFWLENBQW9CQyxNQUFwQixDQUEyQixlQUEzQjtBQUNBNEIsWUFBUSxDQUFDN0IsU0FBVCxDQUFtQkMsTUFBbkIsQ0FBMEIsZUFBMUI7QUFDQXRCLFlBQVEsQ0FBQzJCLGVBQVQsQ0FBeUJOLFNBQXpCLENBQW1DQyxNQUFuQyxDQUEwQyxlQUExQztBQUNELEdBYkQ7QUFjRCxDLENBQ0Q7OztBQUNBLFNBQVM2QixTQUFULENBQW1CakQsRUFBbkIsRUFBdUIwQyxNQUF2QixFQUErQlEsUUFBL0IsRUFBeUM7QUFDdkMsT0FBS2xELEVBQUwsR0FBVUEsRUFBVjtBQUNBLE9BQUswQyxNQUFMLEdBQWNBLE1BQWQ7QUFDQSxPQUFLUSxRQUFMLEdBQWdCQSxRQUFoQjtBQUNEOztBQUNERCxTQUFTLENBQUNFLFNBQVYsQ0FBb0JDLElBQXBCLEdBQTJCLFlBQVk7QUFDckMsTUFBSUMsUUFBUSxHQUFHdkQsUUFBUSxDQUFDd0QsZ0JBQVQsQ0FBMEIsS0FBS3RELEVBQS9CLENBQWY7QUFDQSxNQUFJMEMsTUFBTSxHQUFHLEtBQUtBLE1BQWxCO0FBQ0EsTUFBSVEsUUFBUSxHQUFHLEtBQUtBLFFBQXBCO0FBQ0FLLE9BQUssQ0FBQ0osU0FBTixDQUFnQkssS0FBaEIsQ0FBc0JDLElBQXRCLENBQTJCSixRQUEzQixFQUFxQ0ssT0FBckMsQ0FBNkMsVUFBVUMsT0FBVixFQUFtQjtBQUM5REEsV0FBTyxDQUFDNUMsZ0JBQVIsQ0FBeUIsT0FBekIsRUFBa0MsWUFBWTtBQUM1QzZDLFdBQUssQ0FBQ2hDLGNBQU47O0FBQ0EsVUFBSXNCLFFBQVEsSUFBSSxJQUFoQixFQUFzQjtBQUNwQixZQUFJLEtBQUtqRCxhQUFMLENBQW1CeUMsTUFBbkIsTUFBK0IsSUFBbkMsRUFBeUM7QUFDdkM7QUFDQTtBQUNBO0FBQ0E7QUFDQTdDLFdBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUWdFLElBQVIsQ0FBYW5CLE1BQWIsRUFBcUJvQixXQUFyQixDQUFpQyxzQkFBakMsRUFMdUMsQ0FNdkM7O0FBQ0FqRSxXQUFDLENBQUMsSUFBRCxDQUFELENBQVFpRSxXQUFSLENBQW9CLHVCQUFwQjtBQUNBakUsV0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFRcUQsUUFBUixHQUFtQlcsSUFBbkIsQ0FBd0JuQixNQUF4QixFQUFnQ3FCLFdBQWhDLENBQTRDLHNCQUE1QyxFQVJ1QyxDQVN2Qzs7QUFDQWxFLFdBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUXFELFFBQVIsR0FBbUJhLFdBQW5CLENBQStCLHVCQUEvQjtBQUNELFNBWm1CLENBYXBCO0FBQ0E7OztBQUNBLFlBQUlDLEdBQUcsR0FBR25FLENBQUMsQ0FBQzhELE9BQUQsQ0FBRCxDQUFXaEIsSUFBWCxDQUFnQixhQUFoQixDQUFWO0FBQ0E5QyxTQUFDLENBQUNtRSxHQUFELENBQUQsQ0FBT0YsV0FBUCxDQUFtQixzQkFBbkIsRUFBMkNaLFFBQTNDLEdBQXNEYSxXQUF0RCxDQUFrRSxzQkFBbEUsRUFoQm9CLENBaUJwQjtBQUNBO0FBQ0E7QUFDRCxPQXBCRCxNQW9CTztBQUNMLFlBQUksS0FBSzlELGFBQUwsQ0FBbUJ5QyxNQUFuQixNQUErQixJQUFuQyxFQUF5QztBQUN2QyxlQUFLekMsYUFBTCxDQUFtQnlDLE1BQW5CLEVBQTJCdkIsU0FBM0IsQ0FBcUNLLE1BQXJDLENBQ0Usc0JBREY7QUFHRDs7QUFDRDFCLGdCQUFRLENBQUNHLGFBQVQsQ0FBdUIwRCxPQUFPLENBQUNNLFlBQVIsQ0FBcUIsYUFBckIsQ0FBdkIsRUFBNEQ5QyxTQUE1RCxDQUFzRUssTUFBdEUsQ0FBNkUsc0JBQTdFO0FBQ0FtQyxlQUFPLENBQUN4QyxTQUFSLENBQWtCSyxNQUFsQixDQUF5QixzQkFBekI7QUFDRDtBQUNGLEtBL0JEO0FBZ0NELEdBakNEO0FBa0NELENBdENEOztBQXdDQSxTQUFTMEMsZUFBVCxDQUF5QkMsa0JBQXpCLEVBQTZDO0FBQzNDLFNBQU9aLEtBQUssQ0FBQ0osU0FBTixDQUFnQkssS0FBaEIsQ0FBc0JDLElBQXRCLENBQTJCVSxrQkFBM0IsQ0FBUDtBQUNELEMsQ0FDRDs7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBRUEsU0FBU0MsY0FBVCxDQUF3QnBFLEVBQXhCLEVBQTRCMEMsTUFBNUIsRUFBb0MyQixVQUFwQyxFQUFnRDtBQUM5QyxNQUFJaEIsUUFBUSxHQUFHdkQsUUFBUSxDQUFDd0QsZ0JBQVQsQ0FBMEJ0RCxFQUExQixDQUFmO0FBQ0EsTUFBSTBDLE1BQU0sR0FBRzVDLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QnlDLE1BQXZCLENBQWI7O0FBQ0EsTUFBSUEsTUFBSixFQUFZO0FBQ1Z3QixtQkFBZSxDQUFDYixRQUFELENBQWYsQ0FBMEJLLE9BQTFCLENBQWtDLFVBQVVDLE9BQVYsRUFBbUI7QUFDbkRBLGFBQU8sQ0FBQzVDLGdCQUFSLENBQXlCLE9BQXpCLEVBQWtDLFlBQVk7QUFDNUM2QyxhQUFLLENBQUNoQyxjQUFOO0FBQ0EsYUFBS1QsU0FBTCxDQUFlSyxNQUFmLENBQXNCLFdBQXRCO0FBQ0FrQixjQUFNLENBQUN2QixTQUFQLENBQWlCSyxNQUFqQixDQUF3QixXQUF4QjtBQUNBLFlBQUk4QyxhQUFhLEdBQUdELFVBQXBCOztBQUNBLFlBQUlDLGFBQWEsS0FBSyxFQUF0QixFQUEwQjtBQUN4QixjQUFJQyxRQUFRLEdBQUd6RCxNQUFNLENBQUNZLFVBQVAsR0FBb0IyQyxVQUFuQzs7QUFDQSxjQUFJRSxRQUFKLEVBQWM7QUFDWnpFLG9CQUFRLENBQUMyQixlQUFULENBQXlCTixTQUF6QixDQUFtQ0ssTUFBbkMsQ0FBMEMsdUJBQTFDO0FBQ0Q7QUFDRixTQUxELE1BS087QUFDTDFCLGtCQUFRLENBQUMyQixlQUFULENBQXlCTixTQUF6QixDQUFtQ0MsTUFBbkMsQ0FBMEMsdUJBQTFDO0FBQ0Q7O0FBQ0ROLGNBQU0sQ0FBQ0MsZ0JBQVAsQ0FBd0IsUUFBeEIsRUFBa0MsWUFBWTtBQUM1QyxjQUFJRCxNQUFNLENBQUNZLFVBQVAsSUFBcUIyQyxVQUF6QixFQUFxQztBQUNuQ3ZFLG9CQUFRLENBQUMyQixlQUFULENBQXlCTixTQUF6QixDQUFtQ0MsTUFBbkMsQ0FBMEMsdUJBQTFDO0FBQ0Q7QUFDRixTQUpEO0FBS0QsT0FsQkQ7QUFtQkQsS0FwQkQ7QUFxQkQ7QUFDRjtBQUNEOztBQUNBOzs7QUFDQSxTQUFTb0QsWUFBVCxDQUFzQnhFLEVBQXRCLEVBQTBCMEMsTUFBMUIsRUFBa0M7QUFDaEMsTUFBSVcsUUFBUSxHQUFHdkQsUUFBUSxDQUFDd0QsZ0JBQVQsQ0FBMEJ0RCxFQUExQixDQUFmO0FBQ0EsTUFBSTBDLE1BQU0sR0FBRzVDLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QnlDLE1BQXZCLENBQWI7O0FBQ0EsTUFBSUEsTUFBSixFQUFZO0FBQ1Z3QixtQkFBZSxDQUFDYixRQUFELENBQWYsQ0FBMEJLLE9BQTFCLENBQWtDLFVBQVVDLE9BQVYsRUFBbUI7QUFDbkRBLGFBQU8sQ0FBQzVDLGdCQUFSLENBQXlCLE9BQXpCLEVBQWtDLFlBQVk7QUFDNUM2QyxhQUFLLENBQUNoQyxjQUFOO0FBQ0FjLGNBQU0sQ0FBQ3ZCLFNBQVAsQ0FBaUJDLE1BQWpCLENBQXdCLFdBQXhCO0FBQ0F0QixnQkFBUSxDQUFDMkIsZUFBVCxDQUF5Qk4sU0FBekIsQ0FBbUNDLE1BQW5DLENBQTBDLHVCQUExQztBQUNELE9BSkQ7QUFLRCxLQU5EO0FBT0Q7QUFDRixDLENBQ0Q7OztBQUNBLFNBQVNxRCxlQUFULEdBQTJCO0FBQ3pCLE1BQUkzRSxRQUFRLENBQUNHLGFBQVQsQ0FBdUIsa0JBQXZCLENBQUosRUFBZ0Q7QUFDOUMsUUFBSWdELFNBQUosQ0FBYyxrQkFBZCxFQUFrQyx1QkFBbEMsRUFBMkQsSUFBM0QsRUFBaUVHLElBQWpFO0FBQ0Q7QUFDRixDLENBQ0Q7OztBQUNBLElBQUlzQixHQUFHLEdBQUcsQ0FBVjs7QUFFQSxTQUFTQyxVQUFULEdBQXNCO0FBQ3BCLE1BQUk3RCxNQUFNLENBQUNZLFVBQVAsR0FBb0IsR0FBcEIsSUFBMkJnRCxHQUFHLElBQUksQ0FBdEMsRUFBeUM7QUFDdkM3RSxLQUFDLENBQUMsV0FBRCxDQUFELENBQWVrRSxXQUFmLENBQTJCLGFBQTNCLEVBQTBDYSxLQUExQyxHQUFrREMsUUFBbEQsQ0FBMkQsYUFBM0Q7QUFDQWhGLEtBQUMsQ0FBQyxjQUFELENBQUQsQ0FBa0JrRSxXQUFsQixDQUE4QixRQUE5QixFQUF3Q2UsTUFBeEMsR0FBaURGLEtBQWpELEdBQXlEZixJQUF6RCxDQUE4RCxjQUE5RCxFQUE4RWdCLFFBQTlFLENBQXVGLFFBQXZGO0FBQ0FILE9BQUcsR0FBRyxDQUFOO0FBQ0Q7O0FBQ0QsTUFBSTVELE1BQU0sQ0FBQ1ksVUFBUCxHQUFvQixHQUFwQixJQUEyQmdELEdBQUcsSUFBSSxDQUF0QyxFQUF5QztBQUN2QzdFLEtBQUMsQ0FBQyxXQUFELENBQUQsQ0FBZWdGLFFBQWYsQ0FBd0IsYUFBeEI7QUFDQUgsT0FBRyxHQUFHLENBQU47QUFDRCxHQVRtQixDQVVwQjs7QUFDRDs7QUFFRCxTQUFTSyxvQkFBVCxHQUFnQztBQUM5QjtBQUNBO0FBQ0EsTUFBSWpGLFFBQVEsQ0FBQ0csYUFBVCxDQUF1Qiw2QkFBdkIsQ0FBSixFQUEyRDtBQUN6RCxRQUFJZ0QsU0FBSixDQUFjLDZCQUFkLEVBQTZDK0IsU0FBN0MsRUFBd0QsS0FBeEQsRUFBK0Q1QixJQUEvRDtBQUNEOztBQUNELE1BQUlDLFFBQVEsR0FBR3ZELFFBQVEsQ0FBQ3dELGdCQUFULENBQTBCLGtCQUExQixDQUFmO0FBQ0FZLGlCQUFlLENBQUNiLFFBQUQsQ0FBZixDQUEwQkssT0FBMUIsQ0FBa0MsVUFBVUMsT0FBVixFQUFtQjtBQUNuRCxRQUFJM0QsRUFBRSxHQUFHRixRQUFRLENBQUNHLGFBQVQsQ0FBdUIwRCxPQUFPLENBQUNNLFlBQVIsQ0FBcUIsYUFBckIsQ0FBdkIsQ0FBVDtBQUNBTixXQUFPLENBQUM1QyxnQkFBUixDQUF5QixPQUF6QixFQUFrQyxZQUFZO0FBQzVDLFVBQUlmLEVBQUUsQ0FBQ0MsYUFBSCxDQUFpQiw2QkFBakIsQ0FBSixFQUFxRDtBQUNuREQsVUFBRSxDQUFDQyxhQUFILENBQWlCLDZCQUFqQixFQUFnRGtCLFNBQWhELENBQTBEQyxNQUExRCxDQUFpRSxzQkFBakU7QUFDQXBCLFVBQUUsQ0FBQ0MsYUFBSCxDQUFpQiw2QkFBakIsRUFBZ0RrQixTQUFoRCxDQUEwREMsTUFBMUQsQ0FBaUUsc0JBQWpFO0FBQ0Q7QUFDRixLQUxEO0FBTUQsR0FSRCxFQVA4QixDQWdCOUI7QUFDQTs7QUFDQWdELGdCQUFjLENBQUMsUUFBRCxFQUFXLG9CQUFYLEVBQWlDLEVBQWpDLENBQWQ7QUFDQUEsZ0JBQWMsQ0FBQyx3QkFBRCxFQUEyQixvQkFBM0IsRUFBaUQsR0FBakQsQ0FBZDtBQUNBSSxjQUFZLENBQUMsYUFBRCxFQUFnQixvQkFBaEIsQ0FBWixDQXBCOEIsQ0FxQjlCOztBQUNBSixnQkFBYyxDQUFDLGlCQUFELEVBQW9CLGtCQUFwQixFQUF3QyxHQUF4QyxDQUFkO0FBQ0FBLGdCQUFjLENBQUMsUUFBRCxFQUFXLGtCQUFYLEVBQStCLEVBQS9CLENBQWQ7QUFDQUksY0FBWSxDQUFDLGFBQUQsRUFBZ0Isa0JBQWhCLENBQVosQ0F4QjhCLENBeUI5QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFFRDs7QUFFRCxTQUFTUyx5QkFBVCxDQUFtQ2pGLEVBQW5DLEVBQXVDcUUsVUFBdkMsRUFBbUQ7QUFDakQsTUFBSXZELE1BQU0sQ0FBQ1ksVUFBUCxHQUFvQjJDLFVBQXBCLElBQWtDeEUsQ0FBQyxDQUFDRyxFQUFELENBQUQsQ0FBTWtGLFFBQU4sQ0FBZSxXQUFmLENBQWxDLElBQWlFcEYsUUFBUSxDQUFDMkIsZUFBVCxDQUF5Qk4sU0FBekIsSUFBc0MsRUFBM0csRUFBK0c7QUFDN0dyQixZQUFRLENBQUMyQixlQUFULENBQXlCTixTQUF6QixDQUFtQ0UsR0FBbkMsQ0FBdUMsZUFBdkM7QUFDRDtBQUNGLEMsQ0FDRDs7O0FBQ0EsU0FBUzhELEtBQVQsQ0FBZW5GLEVBQWYsRUFBbUJvRixZQUFuQixFQUFpQ0MsWUFBakMsRUFBK0NDLElBQS9DLEVBQXFEQyxVQUFyRCxFQUFpRTtBQUMvRCxPQUFLdkYsRUFBTCxHQUFVQSxFQUFWO0FBQ0EsT0FBS29GLFlBQUwsR0FBb0JBLFlBQXBCO0FBQ0EsT0FBS0MsWUFBTCxHQUFvQkEsWUFBcEI7QUFDQSxPQUFLQyxJQUFMLEdBQVlBLElBQVo7QUFDQSxPQUFLQyxVQUFMLEdBQWtCQSxVQUFsQjtBQUNEOztBQUFBOztBQUVESixLQUFLLENBQUNoQyxTQUFOLENBQWdCQyxJQUFoQixHQUF1QixZQUFZO0FBQ2pDdkQsR0FBQyxDQUFDLEtBQUtHLEVBQUwsR0FBVSxXQUFYLENBQUQsQ0FBeUJ3RixLQUF6QixDQUErQjtBQUM3QkMsVUFBTSxFQUFFLElBRHFCO0FBRTdCTCxnQkFBWSxFQUFFLEtBQUtBLFlBRlU7QUFHN0JDLGdCQUFZLEVBQUUsS0FBS0EsWUFIVTtBQUk3QkMsUUFBSSxFQUFFLEtBQUtBLElBSmtCO0FBSzdCSSxhQUFTLHNHQUxvQjtBQU03QkMsYUFBUyx1R0FOb0I7QUFPN0I7QUFDQTtBQUNBSixjQUFVLEVBQUUsS0FBS0E7QUFUWSxHQUEvQjtBQVdELENBWkQ7O0FBY0EsU0FBU0ssMkJBQVQsR0FBdUM7QUFDckN2QyxVQUFRLEdBQUd2RCxRQUFRLENBQUN3RCxnQkFBVCxDQUEwQixnQ0FBMUIsQ0FBWDtBQUNBWSxpQkFBZSxDQUFDYixRQUFELENBQWYsQ0FBMEJLLE9BQTFCLENBQWtDLFVBQVVDLE9BQVYsRUFBbUI7QUFDbkQsUUFBSWtDLFNBQVMsR0FBR2xDLE9BQU8sQ0FBQ0wsZ0JBQVIsQ0FBeUIsSUFBekIsQ0FBaEI7QUFDQSxRQUFJd0MsS0FBSyxHQUFHRCxTQUFTLENBQUMsQ0FBRCxDQUFULENBQWE3RCxZQUF6QjtBQUNBLFFBQUkrRCxLQUFLLEdBQUdGLFNBQVMsQ0FBQyxDQUFELENBQVQsQ0FBYTdELFlBQXpCO0FBQ0EsUUFBSWdFLEtBQUssR0FBR0gsU0FBUyxDQUFDLENBQUQsQ0FBVCxDQUFhN0QsWUFBekI7QUFDQSxRQUFJaUUsUUFBUSxHQUFHSCxLQUFLLEdBQUdDLEtBQVIsR0FBZ0JDLEtBQS9COztBQUNBLFFBQUlsRixNQUFNLENBQUNZLFVBQVAsR0FBb0IsSUFBcEIsSUFBNEJpQyxPQUFPLENBQUN4QyxTQUFSLENBQWtCK0UsUUFBbEIsQ0FBMkIsV0FBM0IsS0FBMkMsS0FBM0UsRUFBa0Y7QUFDaEZ2QyxhQUFPLENBQUN3QyxLQUFSLENBQWMzRCxNQUFkLGFBQTBCeUQsUUFBMUI7QUFDRCxLQUZELE1BRU8sSUFBSW5GLE1BQU0sQ0FBQ1ksVUFBUCxHQUFvQixJQUF4QixFQUE4QjtBQUNuQ2lDLGFBQU8sQ0FBQ3dDLEtBQVIsQ0FBYzNELE1BQWQsR0FBdUIsRUFBdkI7QUFDQW1CLGFBQU8sQ0FBQ3hDLFNBQVIsQ0FBa0JDLE1BQWxCLENBQXlCLFdBQXpCO0FBQ0F1QyxhQUFPLENBQUMxRCxhQUFSLENBQXNCLEdBQXRCLEVBQTJCa0IsU0FBM0IsQ0FBcUNDLE1BQXJDLENBQTRDLGtCQUE1QztBQUNEOztBQUNETixVQUFNLENBQUNDLGdCQUFQLENBQXdCLFFBQXhCLEVBQWtDLFlBQVk7QUFDNUMsVUFBSUQsTUFBTSxDQUFDWSxVQUFQLEdBQW9CLElBQXBCLElBQTRCaUMsT0FBTyxDQUFDeEMsU0FBUixDQUFrQitFLFFBQWxCLENBQTJCLFdBQTNCLEtBQTJDLEtBQTNFLEVBQWtGO0FBQ2hGdkMsZUFBTyxDQUFDd0MsS0FBUixDQUFjM0QsTUFBZCxhQUEwQnlELFFBQTFCO0FBQ0QsT0FGRCxNQUVPLElBQUluRixNQUFNLENBQUNZLFVBQVAsR0FBb0IsSUFBeEIsRUFBOEI7QUFDbkNpQyxlQUFPLENBQUN3QyxLQUFSLENBQWMzRCxNQUFkLEdBQXVCLEVBQXZCO0FBQ0FtQixlQUFPLENBQUN4QyxTQUFSLENBQWtCQyxNQUFsQixDQUF5QixXQUF6QjtBQUNBdUMsZUFBTyxDQUFDMUQsYUFBUixDQUFzQixHQUF0QixFQUEyQmtCLFNBQTNCLENBQXFDQyxNQUFyQyxDQUE0QyxrQkFBNUM7QUFDRDtBQUNGLEtBUkQ7QUFTRCxHQXRCRDtBQXVCRDs7QUFFRCxTQUFTZ0YsOEJBQVQsR0FBMEM7QUFDeEMvQyxVQUFRLEdBQUd2RCxRQUFRLENBQUN3RCxnQkFBVCxDQUEwQixnQ0FBMUIsQ0FBWDtBQUNBWSxpQkFBZSxDQUFDYixRQUFELENBQWYsQ0FBMEJLLE9BQTFCLENBQWtDLFVBQVVDLE9BQVYsRUFBbUI7QUFDbkQsUUFBSWtDLFNBQVMsR0FBR2xDLE9BQU8sQ0FBQ0wsZ0JBQVIsQ0FBeUIsSUFBekIsQ0FBaEI7QUFDQSxRQUFJd0MsS0FBSyxHQUFHRCxTQUFTLENBQUMsQ0FBRCxDQUFULENBQWE3RCxZQUF6QjtBQUNBLFFBQUkrRCxLQUFLLEdBQUdGLFNBQVMsQ0FBQyxDQUFELENBQVQsQ0FBYTdELFlBQXpCO0FBQ0EsUUFBSWdFLEtBQUssR0FBR0gsU0FBUyxDQUFDLENBQUQsQ0FBVCxDQUFhN0QsWUFBekI7QUFDQSxRQUFJaUUsUUFBUSxHQUFHSCxLQUFLLEdBQUdDLEtBQVIsR0FBZ0JDLEtBQS9CO0FBQ0FyQyxXQUFPLENBQUMxRCxhQUFSLENBQXNCLGdCQUF0QixFQUF3Q2MsZ0JBQXhDLENBQXlELE9BQXpELEVBQWtFLFlBQVk7QUFDNUUsVUFBSUQsTUFBTSxDQUFDWSxVQUFQLEdBQW9CLElBQXBCLElBQTRCaUMsT0FBTyxDQUFDd0MsS0FBUixDQUFjM0QsTUFBZCxJQUF3QixFQUF4RCxFQUE0RDtBQUMxRG1CLGVBQU8sQ0FBQ3dDLEtBQVIsQ0FBYzNELE1BQWQsYUFBMEJ5RCxRQUExQjtBQUNBdEMsZUFBTyxDQUFDeEMsU0FBUixDQUFrQkMsTUFBbEIsQ0FBeUIsV0FBekI7QUFDQXVDLGVBQU8sQ0FBQzFELGFBQVIsQ0FBc0IsR0FBdEIsRUFBMkJrQixTQUEzQixDQUFxQ0MsTUFBckMsQ0FBNEMsa0JBQTVDO0FBQ0QsT0FKRCxNQUlPO0FBQ0x1QyxlQUFPLENBQUN3QyxLQUFSLENBQWMzRCxNQUFkLEdBQXVCLEVBQXZCO0FBQ0FtQixlQUFPLENBQUN4QyxTQUFSLENBQWtCRSxHQUFsQixDQUFzQixXQUF0QjtBQUNBc0MsZUFBTyxDQUFDMUQsYUFBUixDQUFzQixHQUF0QixFQUEyQmtCLFNBQTNCLENBQXFDRSxHQUFyQyxDQUF5QyxrQkFBekM7QUFDRDtBQUNGLEtBVkQ7QUFXRCxHQWpCRDtBQWtCRCxDLENBRUQ7OztBQUNBLFNBQVNnRixZQUFULEdBQXdCO0FBQ3RCLE1BQUl2RyxRQUFRLENBQUNHLGFBQVQsQ0FBdUIsVUFBdkIsQ0FBSixFQUF3QztBQUN0QyxRQUFJcUcsT0FBTyxHQUFHLENBQUMsT0FBRCxFQUFVLGlCQUFWLENBQWQ7QUFDQUEsV0FBTyxDQUFDNUMsT0FBUixDQUFnQixVQUFVaEIsTUFBVixFQUFrQjtBQUNoQyxVQUFJeUMsS0FBSixDQUFVekMsTUFBVixFQUFrQixDQUFsQixFQUFxQixDQUFyQixFQUF3QixDQUF4QixFQUEyQixDQUFDO0FBQzFCNkQsa0JBQVUsRUFBRSxHQURjO0FBRTFCQyxnQkFBUSxFQUFFO0FBQ1JwQixzQkFBWSxFQUFFO0FBRE47QUFGZ0IsT0FBRCxFQU0zQjtBQUNFbUIsa0JBQVUsRUFBRSxHQURkO0FBRUVDLGdCQUFRLEVBQUU7QUFDUnBCLHNCQUFZLEVBQUUsQ0FETjtBQUVSSyxnQkFBTSxFQUFFO0FBRkE7QUFGWixPQU4yQixDQUEzQixFQWFHckMsSUFiSDtBQWNELEtBZkQ7QUFnQkEsUUFBSStCLEtBQUosQ0FBVSxRQUFWLEVBQW9CLENBQXBCLEVBQXVCLENBQXZCLEVBQTBCLENBQTFCLEVBQTZCLENBQUM7QUFDNUJvQixnQkFBVSxFQUFFLEdBRGdCO0FBRTVCQyxjQUFRLEVBQUU7QUFDUnBCLG9CQUFZLEVBQUUsQ0FETjtBQUVSQyxvQkFBWSxFQUFFLENBRk47QUFHUkMsWUFBSSxFQUFFO0FBSEU7QUFGa0IsS0FBRCxFQVE3QjtBQUNFaUIsZ0JBQVUsRUFBRSxHQURkO0FBRUVDLGNBQVEsRUFBRTtBQUNScEIsb0JBQVksRUFBRSxDQUROO0FBRVJDLG9CQUFZLEVBQUUsQ0FGTjtBQUdSQyxZQUFJLEVBQUUsQ0FIRTtBQUlSRyxjQUFNLEVBQUU7QUFKQTtBQUZaLEtBUjZCLENBQTdCLEVBaUJHckMsSUFqQkg7QUFrQkQ7QUFDRjs7QUFFRCxTQUFTcUQsV0FBVCxHQUF1QjtBQUNyQixNQUFJekcsRUFBRSxHQUFHLGdCQUFUOztBQUNBLE1BQUlGLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QkQsRUFBdkIsQ0FBSixFQUFnQztBQUM5QkgsS0FBQyxDQUFDRyxFQUFELENBQUQsQ0FBTXdGLEtBQU4sQ0FBWTtBQUNWO0FBQ0E7QUFDQUosa0JBQVksRUFBRSxDQUhKO0FBSVZLLFlBQU0sRUFBRSxJQUpFO0FBS1ZDLGVBQVMscUdBTEM7QUFNVkMsZUFBUyxzR0FOQztBQU9WZSxjQUFRLEVBQUUsYUFQQTtBQVFWbkIsZ0JBQVUsRUFBRSxDQUFDO0FBQ1hnQixrQkFBVSxFQUFFLEdBREQ7QUFFWEMsZ0JBQVEsRUFBRTtBQUNScEIsc0JBQVksRUFBRSxDQUROO0FBRVJ1QixjQUFJLEVBQUU7QUFGRTtBQUZDLE9BQUQsRUFPWjtBQUNFSixrQkFBVSxFQUFFLEdBRGQ7QUFFRUMsZ0JBQVEsRUFBRTtBQUNScEIsc0JBQVksRUFBRSxDQUROO0FBRVJLLGdCQUFNLEVBQUUsS0FGQTtBQUdSO0FBQ0FrQixjQUFJLEVBQUU7QUFKRTtBQUZaLE9BUFk7QUFSRixLQUFaLEVBRDhCLENBMkI5QjtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDRDtBQUNGOztBQUVELFNBQVNDLFdBQVQsR0FBdUI7QUFDckIsTUFBSTVHLEVBQUUsR0FBRyxnQkFBVDs7QUFDQSxNQUFJRixRQUFRLENBQUNHLGFBQVQsQ0FBdUJELEVBQXZCLENBQUosRUFBZ0M7QUFDOUIsUUFBSTZHLFNBQVMsR0FBRy9HLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QkQsRUFBdkIsRUFBMkJzRCxnQkFBM0IsQ0FBNEMsa0JBQTVDLEVBQWdFd0QsTUFBaEY7QUFDQSxRQUFJQyxlQUFlLEdBQUlGLFNBQVMsR0FBRyxDQUFiLEdBQWtCLENBQWxCLEdBQXNCQSxTQUE1QyxDQUY4QixDQUc5Qjs7QUFDQWhILEtBQUMsQ0FBQ0csRUFBRCxDQUFELENBQU13RixLQUFOLENBQVk7QUFDVkosa0JBQVksRUFBRTJCLGVBREo7QUFFVkMsb0JBQWMsRUFBRSxDQUZOO0FBR1Z2QixZQUFNLEVBQUUsSUFIRTtBQUlWQyxlQUFTLHFHQUpDO0FBS1ZDLGVBQVMsc0dBTEM7QUFNVmUsY0FBUSxFQUFFLGFBTkE7QUFPVk8sY0FBUSxFQUFFLEtBUEE7QUFRVjFCLGdCQUFVLEVBQUUsQ0FBQztBQUNYZ0Isa0JBQVUsRUFBRSxJQUREO0FBRVhDLGdCQUFRLEVBQUU7QUFDUnBCLHNCQUFZLEVBQUUsQ0FETjtBQUVSOEIsdUJBQWEsRUFBRSxLQUZQLENBR1I7QUFDQTtBQUNBOztBQUxRO0FBRkMsT0FBRCxFQVNUO0FBQ0RYLGtCQUFVLEVBQUUsR0FEWDtBQUVEQyxnQkFBUSxFQUFFO0FBQ1JwQixzQkFBWSxFQUFFLENBRE47QUFFUjhCLHVCQUFhLEVBQUUsS0FGUCxDQUdSO0FBQ0E7O0FBSlE7QUFGVCxPQVRTLEVBa0JaO0FBQ0VYLGtCQUFVLEVBQUUsR0FEZDtBQUVFQyxnQkFBUSxFQUFFO0FBQ1JwQixzQkFBWSxFQUFFLENBRE47QUFFUjhCLHVCQUFhLEVBQUUsSUFGUDtBQUdSO0FBQ0F6QixnQkFBTSxFQUFFO0FBSkE7QUFGWixPQWxCWTtBQVJGLEtBQVo7QUFxQ0Q7QUFDRjs7QUFFRCxTQUFTMEIsZ0JBQVQsR0FBNEI7QUFDMUIsTUFBSW5ILEVBQUUsR0FBRyxnQkFBVDs7QUFDQSxNQUFJRixRQUFRLENBQUNHLGFBQVQsQ0FBdUJELEVBQXZCLENBQUosRUFBZ0M7QUFDOUIsUUFBSTZHLFNBQVMsR0FBRy9HLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QkQsRUFBdkIsRUFBMkJzRCxnQkFBM0IsQ0FBNEMsa0JBQTVDLEVBQWdFd0QsTUFBaEYsQ0FEOEIsQ0FFOUI7QUFDQTs7QUFDQSxRQUFJaEcsTUFBTSxDQUFDWSxVQUFQLElBQXFCLElBQXJCLElBQTZCbUYsU0FBUyxJQUFJLENBQTlDLEVBQWlEO0FBQy9DaEgsT0FBQyxDQUFDLGNBQUQsQ0FBRCxDQUFrQmdGLFFBQWxCLENBQTJCLGdCQUEzQixFQUQrQyxDQUUvQztBQUNEOztBQUNELFFBQUkvRCxNQUFNLENBQUNZLFVBQVAsSUFBcUIsR0FBckIsSUFBNEJaLE1BQU0sQ0FBQ1ksVUFBUCxHQUFvQixJQUFoRCxJQUF3RG1GLFNBQVMsR0FBRyxDQUF4RSxFQUEyRTtBQUN6RWhILE9BQUMsQ0FBQyxjQUFELENBQUQsQ0FBa0JnRixRQUFsQixDQUEyQixnQkFBM0IsRUFEeUUsQ0FFekU7QUFDRCxLQVg2QixDQVk5QjtBQUNBO0FBQ0E7O0FBQ0Q7QUFDRjs7QUFFRCxTQUFTdUMsa0JBQVQsR0FBOEI7QUFDNUIsTUFBSXBILEVBQUUsR0FBRyx3QkFBVDs7QUFDQSxNQUFJRixRQUFRLENBQUNHLGFBQVQsQ0FBdUJELEVBQXZCLENBQUosRUFBZ0M7QUFDOUJILEtBQUMsQ0FBQ0csRUFBRCxDQUFELENBQU13RixLQUFOLENBQVk7QUFDVkosa0JBQVksRUFBRSxDQURKO0FBRVY0QixvQkFBYyxFQUFFLENBRk47QUFHVnZCLFlBQU0sRUFBRSxJQUhFO0FBSVZDLGVBQVMscUdBSkM7QUFLVkMsZUFBUyxzR0FMQztBQU1WZSxjQUFRLEVBQUUsYUFOQTtBQU9WTyxjQUFRLEVBQUUsS0FQQTtBQVFWMUIsZ0JBQVUsRUFBRSxDQUFDO0FBQ1hnQixrQkFBVSxFQUFFLEdBREQ7QUFFWEMsZ0JBQVEsRUFBRTtBQUNScEIsc0JBQVksRUFBRSxDQUROO0FBRVI4Qix1QkFBYSxFQUFFO0FBRlA7QUFGQyxPQUFELEVBT1o7QUFDRVgsa0JBQVUsRUFBRSxHQURkO0FBRUVDLGdCQUFRLEVBQUU7QUFDUnBCLHNCQUFZLEVBQUUsQ0FETjtBQUVSOEIsdUJBQWEsRUFBRSxJQUZQO0FBR1J6QixnQkFBTSxFQUFFO0FBSEE7QUFGWixPQVBZO0FBUkYsS0FBWjtBQXlCRDtBQUNGOztBQUVELFNBQVM0Qix1QkFBVCxHQUFtQztBQUNqQyxNQUFJckgsRUFBRSxHQUFHLHdCQUFUOztBQUNBLE1BQUlGLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QkQsRUFBdkIsQ0FBSixFQUFnQztBQUM5QixRQUFJYyxNQUFNLENBQUNZLFVBQVAsSUFBcUIsR0FBekIsRUFBOEI7QUFDNUI3QixPQUFDLENBQUMscUNBQUQsQ0FBRCxDQUF5Q2dGLFFBQXpDLENBQWtELGdCQUFsRDtBQUNEO0FBQ0Y7QUFDRixDLENBQ0Q7OztBQUNBLFNBQVN5QyxZQUFULEdBQXdCO0FBQ3RCLE1BQUl4SCxRQUFRLENBQUN3RCxnQkFBVCxDQUEwQix3QkFBMUIsRUFBb0R3RCxNQUF4RCxFQUFnRTtBQUM5RCxRQUFJekQsUUFBUSxHQUFHdkQsUUFBUSxDQUFDd0QsZ0JBQVQsQ0FBMEIsd0JBQTFCLENBQWY7QUFDQVksbUJBQWUsQ0FBQ2IsUUFBRCxDQUFmLENBQTBCSyxPQUExQixDQUFrQyxVQUFVQyxPQUFWLEVBQW1CO0FBQ25EQSxhQUFPLENBQUM1QyxnQkFBUixDQUF5QixXQUF6QixFQUFzQyxZQUFZO0FBQ2hENEMsZUFBTyxDQUFDMUQsYUFBUixDQUFzQixRQUF0QixFQUFnQ2tCLFNBQWhDLENBQTBDRSxHQUExQyxDQUE4QyxhQUE5QztBQUNELE9BRkQ7QUFHQXNDLGFBQU8sQ0FBQzVDLGdCQUFSLENBQXlCLFVBQXpCLEVBQXFDLFlBQVk7QUFDL0M0QyxlQUFPLENBQUMxRCxhQUFSLENBQXNCLFFBQXRCLEVBQWdDa0IsU0FBaEMsQ0FBMENDLE1BQTFDLENBQWlELGFBQWpEO0FBQ0QsT0FGRDtBQUdELEtBUEQ7QUFRRDtBQUNGLEMsQ0FDRDs7O0FBQ0EsU0FBU21HLGdCQUFULENBQTBCbEQsVUFBMUIsRUFBc0M7QUFDcEMsTUFBSVYsT0FBTyxHQUFHN0QsUUFBUSxDQUFDRyxhQUFULENBQXVCLFlBQXZCLENBQWQ7QUFDQSxNQUFJeUMsTUFBTSxHQUFHNUMsUUFBUSxDQUFDRyxhQUFULENBQXVCLE9BQXZCLENBQWI7QUFFQTBELFNBQU8sQ0FBQzVDLGdCQUFSLENBQXlCLE9BQXpCLEVBQWtDLFlBQVk7QUFDNUMsU0FBS0ksU0FBTCxDQUFlSyxNQUFmLENBQXNCLGVBQXRCO0FBQ0FrQixVQUFNLENBQUN2QixTQUFQLENBQWlCSyxNQUFqQixDQUF3QixlQUF4QjtBQUNBMUIsWUFBUSxDQUFDMkIsZUFBVCxDQUF5Qk4sU0FBekIsQ0FBbUNLLE1BQW5DLENBQTBDLGVBQTFDO0FBQ0QsR0FKRDtBQU1BVixRQUFNLENBQUNDLGdCQUFQLENBQXdCLFFBQXhCLEVBQWtDLFlBQVk7QUFDNUMsUUFBSUQsTUFBTSxDQUFDWSxVQUFQLElBQXFCMkMsVUFBekIsRUFBcUM7QUFDbkNWLGFBQU8sQ0FBQ3hDLFNBQVIsQ0FBa0JDLE1BQWxCLENBQXlCLGVBQXpCO0FBQ0FzQixZQUFNLENBQUN2QixTQUFQLENBQWlCQyxNQUFqQixDQUF3QixlQUF4QjtBQUNBdEIsY0FBUSxDQUFDMkIsZUFBVCxDQUF5Qk4sU0FBekIsQ0FBbUNDLE1BQW5DLENBQTBDLGVBQTFDO0FBQ0Q7QUFDRixHQU5EO0FBT0QsQyxDQUNEOzs7QUFDQSxTQUFTb0csbUJBQVQsQ0FBNkJuRCxVQUE3QixFQUF5QztBQUN2QyxNQUFJaEIsUUFBUSxHQUFHdkQsUUFBUSxDQUFDd0QsZ0JBQVQsQ0FBMEIscUJBQTFCLENBQWY7QUFDQSxNQUFJZ0QsT0FBTyxHQUFHeEcsUUFBUSxDQUFDd0QsZ0JBQVQsQ0FBMEIsMkJBQTFCLENBQWQ7QUFDQVksaUJBQWUsQ0FBQ2IsUUFBRCxDQUFmLENBQTBCSyxPQUExQixDQUFrQyxVQUFVQyxPQUFWLEVBQW1CO0FBQ25ELFFBQUlBLE9BQU8sQ0FBQzhELGtCQUFSLEtBQStCLElBQW5DLEVBQXlDO0FBQ3ZDOUQsYUFBTyxDQUFDNUMsZ0JBQVIsQ0FBeUIsT0FBekIsRUFBa0MsVUFBVUMsQ0FBVixFQUFhO0FBQzdDQSxTQUFDLENBQUNZLGNBQUY7QUFDQSxZQUFJYyxNQUFNLEdBQUdpQixPQUFPLENBQUM4RCxrQkFBckI7O0FBQ0EsWUFBSTNHLE1BQU0sQ0FBQ1ksVUFBUCxHQUFvQjJDLFVBQXhCLEVBQW9DO0FBQ2xDM0IsZ0JBQU0sQ0FBQ3ZCLFNBQVAsQ0FBaUJLLE1BQWpCLENBQXdCLGtCQUF4QjtBQUNBbUMsaUJBQU8sQ0FBQzFELGFBQVIsQ0FBc0IsR0FBdEIsRUFBMkJrQixTQUEzQixDQUFxQ0ssTUFBckMsQ0FBNEMsa0JBQTVDLEVBRmtDLENBR2xDOztBQUNBM0IsV0FBQyxDQUFDNkMsTUFBRCxDQUFELENBQVVnRixPQUFWLEdBQW9CeEUsUUFBcEIsR0FBK0JXLElBQS9CLENBQW9DLHVCQUFwQyxFQUE2REUsV0FBN0QsQ0FBeUUsa0JBQXpFO0FBQ0Q7QUFDRixPQVREO0FBVUQ7QUFDRixHQWJEO0FBZUFqRCxRQUFNLENBQUNDLGdCQUFQLENBQXdCLFFBQXhCLEVBQWtDLFlBQVk7QUFDNUMsUUFBSUQsTUFBTSxDQUFDWSxVQUFQLElBQXFCMkMsVUFBekIsRUFBcUM7QUFDbkNILHFCQUFlLENBQUNiLFFBQUQsQ0FBZixDQUEwQkssT0FBMUIsQ0FBa0MsVUFBVUMsT0FBVixFQUFtQjtBQUNuRCxZQUFJakIsTUFBTSxHQUFHaUIsT0FBTyxDQUFDOEQsa0JBQXJCO0FBQ0EvRSxjQUFNLENBQUN2QixTQUFQLENBQWlCQyxNQUFqQixDQUF3QixrQkFBeEI7QUFDQXVDLGVBQU8sQ0FBQzFELGFBQVIsQ0FBc0IsR0FBdEIsRUFBMkJrQixTQUEzQixDQUFxQ0MsTUFBckMsQ0FBNEMsa0JBQTVDO0FBQ0QsT0FKRDtBQUtEO0FBQ0YsR0FSRDtBQVNEOztBQUVELFNBQVN1RyxtQkFBVCxHQUErQjtBQUM3QjlILEdBQUMsQ0FBQyxxQkFBRCxDQUFELENBQXlCK0gsRUFBekIsQ0FBNEIsT0FBNUIsRUFBcUMsWUFBWTtBQUMvQy9ILEtBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUXFELFFBQVIsR0FBbUJXLElBQW5CLENBQXdCLG1CQUF4QixFQUE2Q0UsV0FBN0MsQ0FBeUQsa0JBQXpEO0FBQ0QsR0FGRDtBQUdEOztBQUVELFNBQVM4RCxrQkFBVCxDQUE0QnhELFVBQTVCLEVBQXdDO0FBQ3RDLE1BQUloQixRQUFRLEdBQUd2RCxRQUFRLENBQUN3RCxnQkFBVCxDQUEwQixxQkFBMUIsQ0FBZjtBQUNBWSxpQkFBZSxDQUFDYixRQUFELENBQWYsQ0FBMEJLLE9BQTFCLENBQWtDLFVBQVVDLE9BQVYsRUFBbUI7QUFDbkQsUUFBSUEsT0FBTyxDQUFDMUQsYUFBUixDQUFzQixtQkFBdEIsTUFBK0MsSUFBbkQsRUFBeUQ7QUFDdkQwRCxhQUFPLENBQUM1QyxnQkFBUixDQUF5QixXQUF6QixFQUFzQyxZQUFZO0FBQ2hENEMsZUFBTyxDQUFDMUQsYUFBUixDQUFzQixxQkFBdEIsRUFBNkNrQixTQUE3QyxDQUF1REUsR0FBdkQsQ0FBMkQsVUFBM0Q7QUFDRCxPQUZEO0FBR0FzQyxhQUFPLENBQUM1QyxnQkFBUixDQUF5QixVQUF6QixFQUFxQyxZQUFZO0FBQy9DNEMsZUFBTyxDQUNKMUQsYUFESCxDQUNpQixxQkFEakIsRUFFR2tCLFNBRkgsQ0FFYUMsTUFGYixDQUVvQixVQUZwQjtBQUdELE9BSkQ7QUFLRDtBQUNGLEdBWEQ7QUFhQU4sUUFBTSxDQUFDQyxnQkFBUCxDQUF3QixRQUF4QixFQUFrQyxZQUFZO0FBQzVDLFFBQUlELE1BQU0sQ0FBQ1ksVUFBUCxHQUFvQjJDLFVBQXhCLEVBQW9DO0FBQ2xDZCxXQUFLLENBQUNKLFNBQU4sQ0FBZ0JLLEtBQWhCLENBQ0dDLElBREgsQ0FDUTNELFFBQVEsQ0FBQ3dELGdCQUFULENBQTBCLHFCQUExQixDQURSLEVBRUdJLE9BRkgsQ0FFVyxVQUFVb0UsSUFBVixFQUFnQjtBQUN2QkEsWUFBSSxDQUFDM0csU0FBTCxDQUFlQyxNQUFmLENBQXNCLFVBQXRCO0FBQ0QsT0FKSDtBQUtEO0FBQ0YsR0FSRDtBQVNEOztBQUVELFNBQVNYLGNBQVQsR0FBMEI7QUFDeEIsTUFBSThGLFVBQVUsR0FBRyxJQUFqQjtBQUNBZ0Isa0JBQWdCLENBQUNoQixVQUFELENBQWhCO0FBQ0FpQixxQkFBbUIsQ0FBQ2pCLFVBQUQsQ0FBbkI7QUFDQXNCLG9CQUFrQixDQUFDdEIsVUFBRCxDQUFsQjtBQUNEOztBQUVELFNBQVNHLFFBQVQsR0FBb0I7QUFDbEIsTUFBSTVHLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QixlQUF2QixDQUFKLEVBQTZDO0FBQzNDLFFBQUk4SCxRQUFRLEdBQUdDLEtBQUssRUFBcEI7QUFDQUQsWUFBUSxDQUFDRSxPQUFUOztBQUNBLFFBQ0UsQ0FBQyxDQUFELEtBQU9DLFNBQVMsQ0FBQ0MsU0FBVixDQUFvQkMsT0FBcEIsQ0FBNEIsTUFBNUIsQ0FBUCxJQUNBRixTQUFTLENBQUNHLFVBQVYsQ0FBcUJELE9BQXJCLENBQTZCLFVBQTdCLElBQTJDLENBRjdDLEVBR0U7QUFDQSxVQUFJRSxNQUFNLEdBQUd4SSxRQUFRLENBQUNHLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBYjtBQUNBcUksWUFBTSxDQUFDQyxZQUFQLENBQW9CLEtBQXBCLEVBQTJCLDJDQUEzQjtBQUNEO0FBQ0Y7QUFDRixDLENBQ0Q7OztBQUNBLFNBQVNDLGFBQVQsQ0FBdUJDLEdBQXZCLEVBQTRCO0FBQzFCLE1BQUl6SSxFQUFFLEdBQUdGLFFBQVEsQ0FBQ3dELGdCQUFULENBQTBCbUYsR0FBMUIsQ0FBVDtBQUNBLE1BQUlDLFVBQVUsR0FBRyxDQUFDLENBQWxCO0FBQ0EsTUFBSUMsU0FBUyxHQUFHLENBQUMsQ0FBakI7QUFDQSxNQUFJcEMsVUFBVSxHQUFHLEdBQWpCLENBSjBCLENBSzFCOztBQUNBckMsaUJBQWUsQ0FBQ2xFLEVBQUQsQ0FBZixDQUFvQjBELE9BQXBCLENBQTRCLFVBQVVvRSxJQUFWLEVBQWdCO0FBQzFDQSxRQUFJLENBQUMzQixLQUFMLENBQVczRCxNQUFYLEdBQW9CLEVBQXBCLENBRDBDLENBQ2xCOztBQUN4QmtHLGNBQVUsR0FBR1osSUFBSSxDQUFDOUYsWUFBbEIsQ0FGMEMsQ0FFVjs7QUFDaEMyRyxhQUFTLEdBQUdBLFNBQVMsR0FBR0QsVUFBWixHQUF5QkMsU0FBekIsR0FBcUNELFVBQWpEO0FBQ0QsR0FKRDs7QUFLQSxNQUFJNUksUUFBUSxDQUFDb0IsSUFBVCxDQUFjMEgsV0FBZCxHQUE0QnJDLFVBQWhDLEVBQTRDO0FBQzFDckMsbUJBQWUsQ0FBQ2xFLEVBQUQsQ0FBZixDQUFvQjBELE9BQXBCLENBQTRCLFVBQVVvRSxJQUFWLEVBQWdCO0FBQzFDQSxVQUFJLENBQUMzQixLQUFMLENBQVczRCxNQUFYLGFBQXVCbUcsU0FBdkI7QUFDRCxLQUZEO0FBR0Q7QUFDRjs7QUFFRCxTQUFTRSxRQUFULENBQWtCQyxHQUFsQixFQUF1QkwsR0FBdkIsRUFBNEI7QUFDMUIsTUFBSXpJLEVBQUUsR0FBR0YsUUFBUSxDQUFDd0QsZ0JBQVQsQ0FBMEJtRixHQUExQixDQUFULENBRDBCLENBRTFCO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQUNBdkUsaUJBQWUsQ0FBQ2xFLEVBQUQsQ0FBZixDQUFvQjBELE9BQXBCLENBQTRCLFVBQVVvRSxJQUFWLEVBQWdCO0FBQzFDLFFBQUlpQixHQUFHLEdBQUdqQixJQUFJLENBQUNrQixTQUFmOztBQUNBLFFBQUlELEdBQUcsQ0FBQ2pDLE1BQUosR0FBYWdDLEdBQWpCLEVBQXNCO0FBQ3BCRyxnQkFBVSxHQUFHRixHQUFHLENBQUNHLFNBQUosQ0FBYyxDQUFkLEVBQWlCSixHQUFHLEdBQUcsQ0FBdkIsSUFBNEIsS0FBekM7QUFDQWhCLFVBQUksQ0FBQ3FCLFNBQUwsR0FBaUJGLFVBQWpCO0FBQ0QsS0FIRCxNQUdPO0FBQ0xBLGdCQUFVLEdBQUdGLEdBQUcsQ0FBQ0csU0FBSixDQUFjLENBQWQsRUFBaUJKLEdBQWpCLElBQXdCLEtBQXJDO0FBQ0FoQixVQUFJLENBQUNxQixTQUFMLEdBQWlCRixVQUFqQjtBQUNEO0FBQ0YsR0FURDtBQVVBVCxlQUFhLENBQUNDLEdBQUQsQ0FBYjtBQUNEOztBQUVELFNBQVNXLGFBQVQsQ0FBdUJwSixFQUF2QixFQUEyQjBDLE1BQTNCLEVBQW1DO0FBQ2pDLE1BQUk1QyxRQUFRLENBQUNHLGFBQVQsQ0FBdUJELEVBQXZCLENBQUosRUFBZ0M7QUFDOUIsUUFBSTJELE9BQU8sR0FBRzdELFFBQVEsQ0FBQ0csYUFBVCxDQUF1QkQsRUFBdkIsQ0FBZDtBQUNBLFFBQUlzRyxPQUFPLEdBQUd4RyxRQUFRLENBQUN3RCxnQkFBVCxDQUEwQlosTUFBMUIsQ0FBZDtBQUNBaUIsV0FBTyxDQUFDNUMsZ0JBQVIsQ0FBeUIsT0FBekIsRUFBa0MsWUFBWTtBQUM1QzZDLFdBQUssQ0FBQ2hDLGNBQU47QUFDQStCLGFBQU8sQ0FBQzBGLElBQVI7QUFDQTlGLFdBQUssQ0FBQ0osU0FBTixDQUFnQkssS0FBaEIsQ0FBc0JDLElBQXRCLENBQTJCNkMsT0FBM0IsRUFBb0M1QyxPQUFwQyxDQUE0QyxVQUFVQyxPQUFWLEVBQW1CO0FBQzdEQSxlQUFPLENBQUMyRixPQUFSLEdBQWtCLEtBQWxCO0FBQ0QsT0FGRDtBQUdELEtBTkQ7QUFPRDtBQUNGOztBQUVELFNBQVNDLG1CQUFULEdBQStCO0FBQzdCLE1BQUl6SixRQUFRLENBQUNHLGFBQVQsQ0FBdUIsb0JBQXZCLE1BQWlELElBQXJELEVBQTJEO0FBQ3pEdUksaUJBQWEsQ0FBQyxvQkFBRCxDQUFiO0FBQ0QsR0FINEIsQ0FJN0I7OztBQUNBLE1BQUkxSSxRQUFRLENBQUNHLGFBQVQsQ0FBdUIsa0JBQXZCLE1BQStDLElBQW5ELEVBQXlEO0FBQ3ZENEksWUFBUSxDQUFDLEVBQUQsRUFBSyxrQkFBTCxDQUFSO0FBQ0Q7QUFDRjs7QUFFRCxTQUFTVyxRQUFULEdBQW9CO0FBQ2xCM0osR0FBQyxDQUFDLHlCQUFELENBQUQsQ0FBNkI0SixPQUE3QixDQUFxQztBQUNuQ0MsYUFBUyxFQUFFLE9BRHdCO0FBRW5DL0YsV0FBTyxFQUFFLE9BRjBCO0FBR25DZCxVQUFNLEVBQUU7QUFIMkIsR0FBckM7QUFLQWhELEdBQUMsQ0FBQyx5QkFBRCxDQUFELENBQTZCK0gsRUFBN0IsQ0FBZ0MscUJBQWhDLEVBQXVELFlBQVk7QUFDakUsUUFBSStCLFlBQVksR0FBRyxNQUFNOUosQ0FBQyxDQUFDLElBQUQsQ0FBRCxDQUFROEMsSUFBUixDQUFhLGtCQUFiLENBQXpCLENBRGlFLENBRWpFO0FBQ0E7O0FBQ0EsUUFBSTlDLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUThDLElBQVIsQ0FBYSxxQkFBYixLQUF1QyxZQUEzQyxFQUF5RDtBQUN2RDlDLE9BQUMsQ0FBQzhKLFlBQUQsQ0FBRCxDQUFnQjlGLElBQWhCLENBQXFCLGdCQUFyQixFQUF1Q3JELElBQXZDLENBQTRDLGdCQUE1QztBQUNEOztBQUNELFFBQUlYLENBQUMsQ0FBQyxJQUFELENBQUQsQ0FBUThDLElBQVIsQ0FBYSxxQkFBYixLQUF1QyxnQkFBM0MsRUFBNkQ7QUFDM0Q5QyxPQUFDLENBQUM4SixZQUFELENBQUQsQ0FBZ0I5RixJQUFoQixDQUFxQixnQkFBckIsRUFBdUNyRCxJQUF2QyxDQUE0QyxvQkFBNUM7QUFDRDtBQUNGLEdBVkQ7QUFXRDs7QUFFRCxTQUFTb0osR0FBVCxHQUFlO0FBQ2IsTUFBSTlKLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QixZQUF2QixDQUFKLEVBQTBDO0FBQ3hDNEosT0FBRyxDQUFDekcsSUFBSixDQUFTO0FBQ1AwRyxVQUFJLEVBQUU7QUFEQyxLQUFUO0FBR0Q7QUFDRjs7QUFFRCxTQUFTQyxTQUFULEdBQXFCO0FBQ25CLE1BQUlqSyxRQUFRLENBQUNHLGFBQVQsQ0FBdUIsWUFBdkIsQ0FBSixFQUEwQztBQUN4QyxRQUFJb0QsUUFBUSxHQUFHdkQsUUFBUSxDQUFDd0QsZ0JBQVQsQ0FBMEIsWUFBMUIsQ0FBZjtBQUNBLFFBQUlaLE1BQU0sR0FBRzVDLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QixRQUF2QixDQUFiO0FBRUFzRCxTQUFLLENBQUNKLFNBQU4sQ0FBZ0JLLEtBQWhCLENBQXNCQyxJQUF0QixDQUEyQkosUUFBM0IsRUFBcUNLLE9BQXJDLENBQTZDLFVBQVVDLE9BQVYsRUFBbUI7QUFDOURBLGFBQU8sQ0FBQ2hDLE9BQVIsR0FBa0IsWUFBWTtBQUM1QmUsY0FBTSxDQUFDNkYsWUFBUCxDQUFvQixLQUFwQixFQUEyQjVFLE9BQU8sQ0FBQ00sWUFBUixDQUFxQixVQUFyQixDQUEzQjtBQUNBK0YsYUFBSyxDQUFDckcsT0FBTyxDQUFDTSxZQUFSLENBQXFCLFVBQXJCLENBQUQsQ0FBTDtBQUNBTixlQUFPLENBQUN4QyxTQUFSLENBQWtCRSxHQUFsQixDQUFzQixXQUF0QjtBQUVBa0MsYUFBSyxDQUFDSixTQUFOLENBQWdCSyxLQUFoQixDQUNHQyxJQURILENBQ1FKLFFBRFIsRUFFRzRHLE1BRkgsQ0FFVSxVQUFVbkMsSUFBVixFQUFnQjtBQUN0QixpQkFBT0EsSUFBSSxLQUFLbkUsT0FBaEI7QUFDRCxTQUpILEVBS0dELE9BTEgsQ0FLVyxVQUFVb0UsSUFBVixFQUFnQjtBQUN2QkEsY0FBSSxDQUFDM0csU0FBTCxDQUFlQyxNQUFmLENBQXNCLFdBQXRCO0FBQ0QsU0FQSDtBQVFELE9BYkQ7QUFjRCxLQWZEO0FBZ0JEO0FBQ0Y7O0FBRUQsU0FBUzhJLGtCQUFULEdBQThCO0FBQzVCLE1BQUlsSyxFQUFFLEdBQUdILENBQUMsQ0FBQyx3QkFBRCxDQUFWOztBQUNBLE1BQUlHLEVBQUosRUFBUTtBQUNOLFNBQUssSUFBSW1LLENBQUMsR0FBRyxDQUFiLEVBQWdCQSxDQUFDLEdBQUduSyxFQUFFLENBQUM4RyxNQUF2QixFQUErQnFELENBQUMsRUFBaEMsRUFBb0M7QUFDbEMsVUFBSUMsZ0JBQWdCLEdBQUdwSyxFQUFFLENBQUNxSyxFQUFILENBQU1GLENBQU4sRUFBU0csUUFBVCxDQUFrQixLQUFsQixFQUF5QnhELE1BQWhELENBRGtDLENBRWxDOztBQUNBLFVBQUlzRCxnQkFBZ0IsSUFBSSxDQUF4QixFQUEyQjtBQUN6QnBLLFVBQUUsQ0FBQ3FLLEVBQUgsQ0FBTUYsQ0FBTixFQUFTdEYsUUFBVCxDQUFrQix3QkFBbEI7QUFDRCxPQUZELE1BRU87QUFDTDdFLFVBQUUsQ0FBQ3FLLEVBQUgsQ0FBTUYsQ0FBTixFQUFTcEcsV0FBVCxDQUFxQix3QkFBckI7QUFDRDtBQUNGO0FBQ0Y7QUFDRjs7QUFFRCxTQUFTd0csdUJBQVQsQ0FBaUNDLGNBQWpDLEVBQWlEQyxRQUFqRCxFQUEyRDtBQUN6RCxNQUFJekssRUFBRSxHQUFHRixRQUFRLENBQUNHLGFBQVQsQ0FBdUJ1SyxjQUF2QixDQUFUO0FBQ0EsTUFBSTlILE1BQU0sR0FBRzVDLFFBQVEsQ0FBQ0csYUFBVCxDQUF1QndLLFFBQXZCLENBQWI7O0FBQ0EsTUFBSXpLLEVBQUosRUFBUTtBQUNOO0FBQ0EsUUFBSXFELFFBQVEsR0FBR3JELEVBQUUsQ0FBQ3NELGdCQUFILENBQW9CLHdCQUFwQixDQUFmLENBRk0sQ0FHTjs7QUFDQUMsU0FBSyxDQUFDSixTQUFOLENBQWdCSyxLQUFoQixDQUFzQkMsSUFBdEIsQ0FBMkJKLFFBQTNCLEVBQXFDSyxPQUFyQyxDQUE2QyxVQUFVQyxPQUFWLEVBQW1CO0FBQzlEQSxhQUFPLENBQUM1QyxnQkFBUixDQUF5QixPQUF6QixFQUFrQyxZQUFZO0FBQzVDLFlBQUkySixVQUFVLEdBQUcxSyxFQUFFLENBQUNzRCxnQkFBSCxDQUFvQiw4QkFBcEIsRUFBb0R3RCxNQUFyRSxDQUQ0QyxDQUU1Qzs7QUFDQSxZQUFJNEQsVUFBVSxHQUFHLENBQWpCLEVBQW9CO0FBQ2xCaEksZ0JBQU0sQ0FBQ3ZCLFNBQVAsQ0FBaUJFLEdBQWpCLENBQXFCLGlCQUFyQjtBQUNELFNBRkQsTUFFTztBQUNMcUIsZ0JBQU0sQ0FBQ3ZCLFNBQVAsQ0FBaUJDLE1BQWpCLENBQXdCLGlCQUF4QjtBQUNEO0FBQ0YsT0FSRDtBQVNELEtBVkQ7QUFXQSxRQUFJdUosYUFBYSxHQUFHN0ssUUFBUSxDQUFDRyxhQUFULENBQXVCLHFCQUF2QixDQUFwQjtBQUNBMEssaUJBQWEsQ0FBQzVKLGdCQUFkLENBQStCLE9BQS9CLEVBQXdDLFlBQVk7QUFDbEQyQixZQUFNLENBQUN2QixTQUFQLENBQWlCQyxNQUFqQixDQUF3QixpQkFBeEI7QUFDRCxLQUZEO0FBR0Q7QUFDRixDLENBQ0Q7OztBQUNBLFNBQVN3SixjQUFULEdBQTBCO0FBQ3hCLE1BQUkvSyxDQUFDLENBQUMsT0FBRCxDQUFMLEVBQWdCO0FBQ2RBLEtBQUMsQ0FBQyxPQUFELENBQUQsQ0FBV2dMLFVBQVgsR0FBd0JDLElBQXhCLENBQTZCO0FBQzNCQyxXQUFLLEVBQUVsTCxDQUFDLENBQUNnTCxVQUFGLENBQWFHLFFBQWIsQ0FBc0JELEtBREY7QUFDUztBQUNwQ0UsVUFBSSxFQUFFLE9BRnFCO0FBRVo7QUFDZkMsYUFBTyxFQUFFLE9BSGtCO0FBR1Q7QUFDbEJDLFVBQUksRUFBRSxJQUpxQixDQUloQjs7QUFKZ0IsS0FBN0I7QUFNRDtBQUVGLEMsQ0FDRDs7O0FBQ0EsU0FBU0MsU0FBVCxHQUFxQjtBQUNuQixNQUFJcEwsRUFBRSxHQUFHRixRQUFRLENBQUNHLGFBQVQsQ0FBdUIsZUFBdkIsQ0FBVDs7QUFDQSxNQUFJRCxFQUFKLEVBQVE7QUFDTkEsTUFBRSxDQUFDZSxnQkFBSCxDQUFvQixRQUFwQixFQUE4QixZQUFZO0FBQ3hDO0FBQ0FzSyxjQUFRLENBQUNDLElBQVQsR0FBZ0IsS0FBS0MsT0FBTCxDQUFhLEtBQUtDLGFBQWxCLEVBQWlDQyxLQUFqRCxDQUZ3QyxDQUd4QztBQUNELEtBSkQ7QUFLRDtBQUNGLEMsQ0FDRDs7O0FBQ0E1TCxDQUFDLENBQUNDLFFBQUQsQ0FBRCxDQUFZQyxLQUFaLENBQWtCLFlBQVk7QUFDNUJxTCxXQUFTO0FBQ1R2SyxlQUFhO0FBQ2J3RixjQUFZO0FBQ1pPLGFBQVc7QUFDWE8sa0JBQWdCLEdBTFksQ0FNNUI7O0FBQ0FDLG9CQUFrQjtBQUNsQkMseUJBQXVCO0FBQ3ZCWixhQUFXO0FBQ1hhLGNBQVk7QUFDWlosVUFBUTtBQUNSakMsaUJBQWUsR0FaYSxDQWE1Qjs7QUFDQW1GLEtBQUc7QUFDSEcsV0FBUztBQUNUaEYsc0JBQW9CO0FBQ3BCcUUsZUFBYSxDQUFDLHFCQUFELEVBQXdCLHdCQUF4QixDQUFiO0FBQ0F4RCw2QkFBMkIsR0FsQkMsQ0FtQjVCOztBQUNBNEQsVUFBUTtBQUNSVSxvQkFBa0I7QUFDbEJLLHlCQUF1QixDQUFDLGtCQUFELEVBQXFCLGlCQUFyQixDQUF2QjtBQUNBQSx5QkFBdUIsQ0FBQyxvQkFBRCxFQUF1Qix3QkFBdkIsQ0FBdkI7QUFDQW5FLGdDQUE4QjtBQUM5QndFLGdCQUFjO0FBQ2R0SSxZQUFVLEdBMUJrQixDQTJCNUI7QUFDRCxDQTVCRDs7QUE2QkF4QixNQUFNLENBQUM0SyxNQUFQLEdBQWdCLFlBQVk7QUFDMUJuQyxxQkFBbUI7QUFDbkI1RSxZQUFVO0FBQ1gsQ0FIRCxDLENBSUE7OztBQUNBOUUsQ0FBQyxDQUFDaUIsTUFBRCxDQUFELENBQVU2SyxNQUFWLENBQWlCLFlBQVk7QUFDM0JwQyxxQkFBbUI7QUFDbkI1RSxZQUFVLEdBRmlCLENBRzNCOztBQUNBTSwyQkFBeUIsQ0FBQyxvQkFBRCxFQUF1QixHQUF2QixDQUF6QjtBQUNBa0Msa0JBQWdCLEdBTFcsQ0FNM0I7QUFDRCxDQVBELEUsQ0FRQTs7QUFDQXRILENBQUMsQ0FBQ2lCLE1BQUQsQ0FBRCxDQUFVOEssTUFBVixDQUFpQixZQUFZO0FBQzNCaEwsYUFBVyxHQURnQixDQUUzQjtBQUNELENBSEQsRSIsImZpbGUiOiJtYWluLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9zcmNANC4wL2Fzc2V0cy9qcy9tYWluLmpzXCIpO1xuIiwiLy8gYWpheCDphY3lkIhKUTMg5byV5YWl6Kit572uXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7XG4gIHZhciBlbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ2h0bWxbbGFuZz1cInpoLXR3XCJdJyk7XG4gIGlmIChlbCkge1xuICAgIGlmICgkKFwiI2hlYWRlclwiKSkge1xuICAgICAgJC5hamF4KHtcbiAgICAgICAgdXJsOiBcImFqYXgvX2hlYWRlci5odG1sXCIsXG4gICAgICAgIG1ldGhvZDogXCJHRVRcIixcbiAgICAgICAgZGF0YVR5cGU6IFwiaHRtbFwiLFxuICAgICAgfSkuZG9uZShmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAkKFwiI2hlYWRlclwiKS5odG1sKGRhdGEpO1xuICAgICAgICBoZWFkZXJGdW5jdGlvbigpO1xuICAgICAgfSk7XG4gICAgfVxuICAgIGlmICgkKFwiI3NpZGVMaW5rXCIpKSB7XG4gICAgICAkLmFqYXgoe1xuICAgICAgICB1cmw6IFwiYWpheC9fc2lkZUxpbmsuaHRtbFwiLFxuICAgICAgICBtZXRob2Q6IFwiR0VUXCIsXG4gICAgICAgIGRhdGFUeXBlOiBcImh0bWxcIixcbiAgICAgIH0pLmRvbmUoZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgJChcIiNzaWRlTGlua1wiKS5odG1sKGRhdGEpO1xuICAgICAgICB0b2dnbGVTaWRlTGluaygpO1xuICAgICAgfSk7XG4gICAgfVxuICAgIGlmICgkKFwiI2Zvb3RlclwiKSkge1xuICAgICAgJC5hamF4KHtcbiAgICAgICAgdXJsOiBcImFqYXgvX2Zvb3Rlci5odG1sXCIsXG4gICAgICAgIG1ldGhvZDogXCJHRVRcIixcbiAgICAgICAgZGF0YVR5cGU6IFwiaHRtbFwiLFxuICAgICAgfSkuZG9uZShmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAkKFwiI2Zvb3RlclwiKS5odG1sKGRhdGEpO1xuICAgICAgICBnb1RvcCgpO1xuICAgICAgICBnb1RvcFN0aWNreSgpO1xuICAgICAgfSk7XG4gICAgfVxuICB9XG5cbn0pO1xuLy/oi7HmlofniYjnmoRhamF4XG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7XG4gIHZhciBlbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ2h0bWxbbGFuZz1cImVuXCJdJyk7XG4gIGlmIChlbCkge1xuICAgIGlmICgkKFwiI2hlYWRlckVuXCIpKSB7XG4gICAgICAkLmFqYXgoe1xuICAgICAgICB1cmw6IFwiLi4vYWpheC9faGVhZGVyX2VuLmh0bWxcIixcbiAgICAgICAgbWV0aG9kOiBcIkdFVFwiLFxuICAgICAgICBkYXRhVHlwZTogXCJodG1sXCIsXG4gICAgICB9KS5kb25lKGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgICQoXCIjaGVhZGVyRW5cIikuaHRtbChkYXRhKTtcbiAgICAgICAgaGVhZGVyRnVuY3Rpb24oKTtcbiAgICAgIH0pO1xuICAgIH1cbiAgICBpZiAoJChcIiNzaWRlTGlua0VuXCIpKSB7XG4gICAgICAkLmFqYXgoe1xuICAgICAgICB1cmw6IFwiLi4vYWpheC9fc2lkZUxpbmtfZW4uaHRtbFwiLFxuICAgICAgICBtZXRob2Q6IFwiR0VUXCIsXG4gICAgICAgIGRhdGFUeXBlOiBcImh0bWxcIixcbiAgICAgIH0pLmRvbmUoZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgJChcIiNzaWRlTGlua0VuXCIpLmh0bWwoZGF0YSk7XG4gICAgICAgIHRvZ2dsZVNpZGVMaW5rKCk7XG4gICAgICB9KTtcbiAgICB9XG4gICAgaWYgKCQoXCIjZm9vdGVyRW5cIikpIHtcbiAgICAgICQuYWpheCh7XG4gICAgICAgIHVybDogXCIuLi9hamF4L19mb290ZXJfZW4uaHRtbFwiLFxuICAgICAgICBtZXRob2Q6IFwiR0VUXCIsXG4gICAgICAgIGRhdGFUeXBlOiBcImh0bWxcIixcbiAgICAgIH0pLmRvbmUoZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgJChcIiNmb290ZXJFblwiKS5odG1sKGRhdGEpO1xuICAgICAgICBnb1RvcCgpO1xuICAgICAgICBnb1RvcFN0aWNreSgpO1xuICAgICAgfSk7XG4gICAgfVxuICB9XG59KTtcblxuZnVuY3Rpb24gdG9vbHNMaXN0ZW5lcigpIHtcbiAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoXCJrZXlkb3duXCIsIGZ1bmN0aW9uIChlKSB7XG4gICAgaWYgKGUua2V5Q29kZSA9PT0gOSkge1xuICAgICAgZG9jdW1lbnQuYm9keS5jbGFzc0xpc3QucmVtb3ZlKFwianMtdXNlTW91c2VcIik7XG4gICAgICBkb2N1bWVudC5ib2R5LmNsYXNzTGlzdC5hZGQoXCJqcy11c2VLZXlib2FyZFwiKTtcbiAgICB9XG4gIH0pO1xuICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlZG93blwiLCBmdW5jdGlvbiAoZSkge1xuICAgIGRvY3VtZW50LmJvZHkuY2xhc3NMaXN0LnJlbW92ZShcImpzLXVzZUtleWJvYXJkXCIpO1xuICAgIGRvY3VtZW50LmJvZHkuY2xhc3NMaXN0LmFkZChcImpzLXVzZU1vdXNlXCIpO1xuICB9KTtcbn1cblxuZnVuY3Rpb24gdG9nZ2xlU2lkZUxpbmsoKSB7XG4gIHZhciBlbFR3ID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignaHRtbFtsYW5nPVwiemgtdHdcIl0nKTtcbiAgdmFyIGVsRW4gPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdodG1sW2xhbmc9XCJlblwiXScpO1xuICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3NpZGVMaW5rVG9nZ2xlQnRuXCIpLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBmdW5jdGlvbiAoKSB7XG4gICAgdGhpcy5jbGFzc0xpc3QudG9nZ2xlKFwianMtc2lkZUxpbmtPcGVuZWRcIik7XG4gICAgaWYgKGVsVHcpIHtcbiAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjc2lkZUxpbmtcIikuY2xhc3NMaXN0LnRvZ2dsZShcImpzLXNpZGVMaW5rT3BlbmVkXCIpO1xuICAgIH1cbiAgICBpZiAoZWxFbikge1xuICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNzaWRlTGlua0VuXCIpLmNsYXNzTGlzdC50b2dnbGUoXCJqcy1zaWRlTGlua09wZW5lZFwiKTtcbiAgICB9XG4gICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNwYWdlXCIpLmNsYXNzTGlzdC50b2dnbGUoXCJqcy1zaWRlTGlua09wZW5lZFwiKTtcbiAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiYm9keVwiKS5jbGFzc0xpc3QudG9nZ2xlKFwib3ZlcmZsb3ctaGlkZGVuXCIpO1xuICAgIC8v5omT6ZaLc2lkZU1lbnXmmYLvvIzmiorlsZXplovnmoRtZW516Zec5o6JXG4gICAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1tZW51T3BlbmVkXCIpO1xuICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIubC1oZWFkZXItaGFtYnVyZ2VyXCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1tZW51T3BlbmVkXCIpO1xuICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIubC1oZWFkZXItbWVudVwiKS5jbGFzc0xpc3QucmVtb3ZlKFwianMtbWVudU9wZW5lZFwiKTtcbiAgICAvL+aJk+mWi3NpZGVNZW515pmC77yM5oqKZ290b3DmjqjliLDml4HpgorljrtcbiAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3NpZGVMaW5rRm9vdGVyXCIpLmNsYXNzTGlzdC50b2dnbGUoXCJqcy1zaWRlTGlua09wZW5lZFwiKTtcbiAgfSk7XG4gIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwicmVzaXplXCIsIGZ1bmN0aW9uICgpIHtcbiAgICBpZiAod2luZG93LmlubmVyV2lkdGggPj0gMTIwMCkge1xuICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNzaWRlTGlua1RvZ2dsZUJ0blwiKS5jbGFzc0xpc3QucmVtb3ZlKFwianMtc2lkZUxpbmtPcGVuZWRcIik7XG4gICAgICBpZiAoZWxUdykge1xuICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3NpZGVMaW5rXCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1zaWRlTGlua09wZW5lZFwiKTtcbiAgICAgIH1cbiAgICAgIGlmIChlbEVuKSB7XG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjc2lkZUxpbmtFblwiKS5jbGFzc0xpc3QucmVtb3ZlKFwianMtc2lkZUxpbmtPcGVuZWRcIik7XG4gICAgICB9XG4gICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3BhZ2VcIikuY2xhc3NMaXN0LnJlbW92ZShcImpzLXNpZGVMaW5rT3BlbmVkXCIpO1xuICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcImJvZHlcIikuY2xhc3NMaXN0LnJlbW92ZShcIm92ZXJmbG93LWhpZGRlblwiKTtcbiAgICAgIC8vW+enu+mZpF3miZPplotzaWRlTWVudeaZgu+8jOaKimdvdG9w5o6o5Yiw5peB6YKK5Y67XG4gICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3NpZGVMaW5rRm9vdGVyXCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1zaWRlTGlua09wZW5lZFwiKTtcbiAgICB9XG4gIH0pO1xufVxuXG5mdW5jdGlvbiBnb1RvcCgpIHtcbiAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNnb1RvcFwiKS5vbmNsaWNrID0gZnVuY3Rpb24gKGUpIHtcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgJChcImh0bWwsIGJvZHlcIikuYW5pbWF0ZSh7XG4gICAgICBzY3JvbGxUb3A6IDAsXG4gICAgfSxcbiAgICAgIDUwMFxuICAgICk7XG4gIH07XG59XG5cbmZ1bmN0aW9uIGdvVG9wU3RpY2t5KCkge1xuICB2YXIgZWwgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI3NpZGVMaW5rRm9vdGVyXCIpO1xuICAvL+ioiOeul2Zvb3RlcuebruWJjemrmOW6plxuICB2YXIgZm9vdGVySGVpZ2h0VmFyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi5sLWZvb3RlclwiKS5jbGllbnRIZWlnaHQ7XG4gIC8vIGNvbnNvbGUubG9nKGBmb290ZXJIZWlnaHRWYXIgKyAke2Zvb3RlckhlaWdodFZhcn1gKTtcbiAgLy/nlbbkuIvntrLpoIHpq5jluqYo5pW06aCB6ZW3KVxuICB2YXIgc2Nyb2xsSGVpZ2h0VmFyID0gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnNjcm9sbEhlaWdodDtcbiAgLy8gY29uc29sZS5sb2coYHNjcm9sbEhlaWdodFZhciArICR7c2Nyb2xsSGVpZ2h0VmFyfWApO1xuICAvL+eVtuS4i+eVq+mdoumrmOW6pih3aW5kb3cgaGVpZ2h0KVxuICB2YXIgd2luZG93SGVpZ2h0VmFyID0gd2luZG93LmlubmVySGVpZ2h0O1xuICAvLyBjb25zb2xlLmxvZyhgd2luZG93SGVpZ2h0VmFyICsgJHt3aW5kb3dIZWlnaHRWYXJ9YCk7XG4gIC8v5o2y5YuV5Lit55qE55Wr6Z2i5pyA5LiK56uv6IiH57ay6aCB6aCC56uv5LmL6ZaT55qE6Led6ZuiXG4gIHZhciBzY3JvbGxUb3BWYXIgPSBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuc2Nyb2xsVG9wO1xuICAvLyBjb25zb2xlLmxvZyhgc2Nyb2xsVG9wVmFyICsgJHtzY3JvbGxUb3BWYXJ9YCk7XG4gIC8vIGNvbnNvbGUubG9nKHdpbmRvd0hlaWdodFZhciArIHNjcm9sbFRvcFZhciArIGZvb3RlckhlaWdodFZhcik7XG4gIC8v5aaC5p6c5pW06aCB6ZW3PT3nlavpnaLpq5jluqYr5o2y5YuV6auY5bqmXG4gIGlmIChlbCkge1xuICAgIGlmIChzY3JvbGxIZWlnaHRWYXIgPD0gd2luZG93SGVpZ2h0VmFyICsgc2Nyb2xsVG9wVmFyICsgZm9vdGVySGVpZ2h0VmFyKSB7XG4gICAgICBlbC5jbGFzc0xpc3QucmVtb3ZlKFwianMtc3RpY2t5XCIpO1xuICAgIH0gZWxzZSB7XG4gICAgICBlbC5jbGFzc0xpc3QuYWRkKFwianMtc3RpY2t5XCIpO1xuICAgIH1cbiAgfVxufVxuZnVuY3Rpb24gZ29Ub0FuY2hvcigpIHtcbiAgdmFyIGhlYWRlckhlaWdodCA9IFwiXCI7XG4gIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwicmVzaXplXCIsIGZ1bmN0aW9uICgpIHtcbiAgICByZXR1cm4gaGVhZGVySGVpZ2h0ID0gJChcIi5sLWhlYWRlclwiKS5oZWlnaHQoKTtcbiAgfSk7XG4gICQoJy5qcy1nb1RvQW5jaG9yJykuY2xpY2soZnVuY3Rpb24gKGUpIHtcbiAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgdmFyIHRhcmdldCA9ICQodGhpcykuYXR0cignaHJlZicpO1xuICAgIHZhciB0YXJnZXRQb3MgPSAkKHRhcmdldCkub2Zmc2V0KCkudG9wO1xuICAgIHZhciBoZWFkZXJIZWlnaHQgPSAkKFwiLmwtaGVhZGVyXCIpLmhlaWdodCgpO1xuICAgICQoJ2h0bWwsYm9keScpLmFuaW1hdGUoe1xuICAgICAgc2Nyb2xsVG9wOiB0YXJnZXRQb3MgLSBoZWFkZXJIZWlnaHRcbiAgICB9LCAxMDAwKTtcbiAgICB2YXIgdHJpZ2dlcjAyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNoYW1idXJnZXJcIik7XG4gICAgdmFyIHRhcmdldDAyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNtZW51XCIpO1xuICAgIHRyaWdnZXIwMi5jbGFzc0xpc3QucmVtb3ZlKFwianMtbWVudU9wZW5lZFwiKTtcbiAgICB0YXJnZXQwMi5jbGFzc0xpc3QucmVtb3ZlKFwianMtbWVudU9wZW5lZFwiKTtcbiAgICBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZShcImpzLW1lbnVPcGVuZWRcIik7XG4gIH0pO1xufVxuLy/poqjnkLTnlKhmdW5jdGlvblxuZnVuY3Rpb24gQWNjb3JkaW9uKGVsLCB0YXJnZXQsIHNpYmxpbmdzKSB7XG4gIHRoaXMuZWwgPSBlbDtcbiAgdGhpcy50YXJnZXQgPSB0YXJnZXQ7XG4gIHRoaXMuc2libGluZ3MgPSBzaWJsaW5ncztcbn1cbkFjY29yZGlvbi5wcm90b3R5cGUuaW5pdCA9IGZ1bmN0aW9uICgpIHtcbiAgdmFyIHRyaWdnZXJzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCh0aGlzLmVsKTtcbiAgdmFyIHRhcmdldCA9IHRoaXMudGFyZ2V0O1xuICB2YXIgc2libGluZ3MgPSB0aGlzLnNpYmxpbmdzO1xuICBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbCh0cmlnZ2VycykuZm9yRWFjaChmdW5jdGlvbiAodHJpZ2dlcikge1xuICAgIHRyaWdnZXIuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICBpZiAoc2libGluZ3MgPT0gdHJ1ZSkge1xuICAgICAgICBpZiAodGhpcy5xdWVyeVNlbGVjdG9yKHRhcmdldCkgIT09IG51bGwpIHtcbiAgICAgICAgICAvLyB0aGlzLnF1ZXJ5U2VsZWN0b3IodGFyZ2V0KS5jbGFzc0xpc3QudG9nZ2xlKFxuICAgICAgICAgIC8vICAgXCJqcy1hY2NvcmRpb25FeHBlbmRlZFwiXG4gICAgICAgICAgLy8gKTtcbiAgICAgICAgICAvL+eCuuS6huimgeiDveWcqOaJk+mWi+S4gOWAi+aZgu+8jOWFtuS7lueahOaUtuWQiO+8jOmAmemCiuaUueeUqGpRdWVyeT09PlxuICAgICAgICAgICQodGhpcykuZmluZCh0YXJnZXQpLnRvZ2dsZUNsYXNzKFwianMtYWNjb3JkaW9uRXhwZW5kZWRcIik7XG4gICAgICAgICAgLy8g5aKe5Yqg6JeN57ea5qGG5pWI5p6cXG4gICAgICAgICAgJCh0aGlzKS50b2dnbGVDbGFzcyhcImMtYWNjb3JkaW9uLWJ0bi1mb2N1c1wiKTtcbiAgICAgICAgICAkKHRoaXMpLnNpYmxpbmdzKCkuZmluZCh0YXJnZXQpLnJlbW92ZUNsYXNzKFwianMtYWNjb3JkaW9uRXhwZW5kZWRcIik7XG4gICAgICAgICAgLy8g5aKe5Yqg6JeN57ea5qGG5pWI5p6cXG4gICAgICAgICAgJCh0aGlzKS5zaWJsaW5ncygpLnJlbW92ZUNsYXNzKFwiYy1hY2NvcmRpb24tYnRuLWZvY3VzXCIpO1xuICAgICAgICB9XG4gICAgICAgIC8vIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IodHJpZ2dlci5nZXRBdHRyaWJ1dGUoXCJkYXRhLXRhcmdldFwiKSkuY2xhc3NMaXN0LnRvZ2dsZShcImpzLWFjY29yZGlvbkV4cGVuZGVkXCIpO1xuICAgICAgICAvL+eCuuS6huimgeiDveWcqOaJk+mWi+S4gOWAi+aZgu+8jOWFtuS7lueahOaUtuWQiO+8jOmAmemCiuaUueeUqGpRdWVyeT09PlxuICAgICAgICB2YXIgYXR0ID0gJCh0cmlnZ2VyKS5hdHRyKFwiZGF0YS10YXJnZXRcIik7XG4gICAgICAgICQoYXR0KS50b2dnbGVDbGFzcyhcImpzLWFjY29yZGlvbkV4cGVuZGVkXCIpLnNpYmxpbmdzKCkucmVtb3ZlQ2xhc3MoXCJqcy1hY2NvcmRpb25FeHBlbmRlZFwiKTtcbiAgICAgICAgLy8gJChgJHt0cmlnZ2VyLmdldEF0dHJpYnV0ZShcImRhdGEtdGFyZ2V0XCIpfWApLnRvZ2dsZUNsYXNzKFwianMtYWNjb3JkaW9uRXhwZW5kZWRcIikuc2libGluZ3MoKS5yZW1vdmVDbGFzcyhcImpzLWFjY29yZGlvbkV4cGVuZGVkXCIpO1xuICAgICAgICAvL+eUqOS6hmpRdWVyeeS5i+W+jO+8jOmAmee1hOacg+iiq+aok+S4iumCo+WAi+e2geWcqOS4gOi1t++8jOaJgOS7peWPr+S7peecgeeVpVxuICAgICAgICAvLyB0cmlnZ2VyLmNsYXNzTGlzdC50b2dnbGUoXCJqcy1hY2NvcmRpb25FeHBlbmRlZFwiKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGlmICh0aGlzLnF1ZXJ5U2VsZWN0b3IodGFyZ2V0KSAhPT0gbnVsbCkge1xuICAgICAgICAgIHRoaXMucXVlcnlTZWxlY3Rvcih0YXJnZXQpLmNsYXNzTGlzdC50b2dnbGUoXG4gICAgICAgICAgICBcImpzLWFjY29yZGlvbkV4cGVuZGVkXCJcbiAgICAgICAgICApO1xuICAgICAgICB9XG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IodHJpZ2dlci5nZXRBdHRyaWJ1dGUoXCJkYXRhLXRhcmdldFwiKSkuY2xhc3NMaXN0LnRvZ2dsZShcImpzLWFjY29yZGlvbkV4cGVuZGVkXCIpO1xuICAgICAgICB0cmlnZ2VyLmNsYXNzTGlzdC50b2dnbGUoXCJqcy1hY2NvcmRpb25FeHBlbmRlZFwiKTtcbiAgICAgIH1cbiAgICB9KTtcbiAgfSk7XG59XG5cbmZ1bmN0aW9uIG5vZGVMaXN0VG9BcnJheShub2RlTGlzdENvbGxlY3Rpb24pIHtcbiAgcmV0dXJuIEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKG5vZGVMaXN0Q29sbGVjdGlvbik7XG59XG4vLyBjb25zdCBub2RlTGlzdFRvQXJyYXkgPSAobm9kZUxpc3RDb2xsZWN0aW9uKSA9PiBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbChub2RlTGlzdENvbGxlY3Rpb24pO1xuLyoqXG4gKiBcbiAqIEBwYXJhbSB7Kn0gZWwgOmNsYXNzIG5hbWVcbiAqIEBwYXJhbSB7Kn0gdGFyZ2V0IDrooqvlvbHpn7/liLDnmoTnm67mqJlcbiAqIEBwYXJhbSB7Kn0gbWVkaWFRdWVyeSA65pa36bue6Kit5a6aXG4gKiBA6Kqq5piOIFwiZWxcIuiIh1widGFyZ2V0XCIgdG9nZ2xlIOS4gOWAi+WPq2pzLWFjdGl2ZeeahGNsYXNz6KaB55So5Ye65LuA6bq85pWI5p6c77yM56uv55yL5L2g5oCO6bq85a+ranMtYWN0aXZl55qEY3Nz5pWI5p6c5bGV6ZaL5oiW5pS25ZCI5LuA6bq85p2x6KW/XG4gKi9cblxuZnVuY3Rpb24gdG9nZ2xlVmlzaWFibGUoZWwsIHRhcmdldCwgbWVkaWFRdWVyeSkge1xuICB2YXIgdHJpZ2dlcnMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKGVsKTtcbiAgdmFyIHRhcmdldCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IodGFyZ2V0KTtcbiAgaWYgKHRhcmdldCkge1xuICAgIG5vZGVMaXN0VG9BcnJheSh0cmlnZ2VycykuZm9yRWFjaChmdW5jdGlvbiAodHJpZ2dlcikge1xuICAgICAgdHJpZ2dlci5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICB0aGlzLmNsYXNzTGlzdC50b2dnbGUoXCJqcy1hY3RpdmVcIik7XG4gICAgICAgIHRhcmdldC5jbGFzc0xpc3QudG9nZ2xlKFwianMtYWN0aXZlXCIpO1xuICAgICAgICB2YXIgaGFzTWVkaWFRdWVyeSA9IG1lZGlhUXVlcnk7XG4gICAgICAgIGlmIChoYXNNZWRpYVF1ZXJ5ICE9PSBcIlwiKSB7XG4gICAgICAgICAgdmFyIGlzTW9iaWxlID0gd2luZG93LmlubmVyV2lkdGggPCBtZWRpYVF1ZXJ5O1xuICAgICAgICAgIGlmIChpc01vYmlsZSkge1xuICAgICAgICAgICAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTGlzdC50b2dnbGUoXCJqcy1mdW5jdGlvbk1lbnVPcGVuZWRcIik7XG4gICAgICAgICAgfVxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgIGRvY3VtZW50LmRvY3VtZW50RWxlbWVudC5jbGFzc0xpc3QucmVtb3ZlKFwianMtZnVuY3Rpb25NZW51T3BlbmVkXCIpO1xuICAgICAgICB9XG4gICAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwicmVzaXplXCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICBpZiAod2luZG93LmlubmVyV2lkdGggPj0gbWVkaWFRdWVyeSkge1xuICAgICAgICAgICAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1mdW5jdGlvbk1lbnVPcGVuZWRcIik7XG4gICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG59XG4vKmVsPeinuOeZvOWwjeixoSjplovpl5wpKi9cbi8qdGFyZ2V0Peiiq+aOp+WItueahOeJqeS7tijnh4gpKi9cbmZ1bmN0aW9uIGNsaWNrQ29uZmlybShlbCwgdGFyZ2V0KSB7XG4gIHZhciB0cmlnZ2VycyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoZWwpO1xuICB2YXIgdGFyZ2V0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3Rvcih0YXJnZXQpO1xuICBpZiAodGFyZ2V0KSB7XG4gICAgbm9kZUxpc3RUb0FycmF5KHRyaWdnZXJzKS5mb3JFYWNoKGZ1bmN0aW9uICh0cmlnZ2VyKSB7XG4gICAgICB0cmlnZ2VyLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIHRhcmdldC5jbGFzc0xpc3QucmVtb3ZlKFwianMtYWN0aXZlXCIpO1xuICAgICAgICBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZShcImpzLWZ1bmN0aW9uTWVudU9wZW5lZFwiKTtcbiAgICAgIH0pO1xuICAgIH0pO1xuICB9XG59XG4vL+KGkeKGkeKGkemAmueUqGZ1bmN0aW9uLS0tLS0tLS0tLS0tLS0tLS0tLS0tXG5mdW5jdGlvbiBjcmVhdGVBY2NvcmRpb24oKSB7XG4gIGlmIChkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCcuYy1hY2NvcmRpb24tYnRuJykpIHtcbiAgICBuZXcgQWNjb3JkaW9uKFwiLmMtYWNjb3JkaW9uLWJ0blwiLCBcIi5jLWFjY29yZGlvbi1idG4taWNvblwiLCB0cnVlKS5pbml0KCk7XG4gIH1cbn1cbi8vIFRPRE86IOaDs+i+puazlemAmeWAi+WFqOWfn+iuiuaVuOimgeiZleeQhuS4gOS4i1xudmFyIHN0ciA9IDA7XG5cbmZ1bmN0aW9uIHNob3dBbGxUYWIoKSB7XG4gIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA+IDk5MiAmJiBzdHIgPT0gMSkge1xuICAgICQoXCIudGFiLXBhbmVcIikucmVtb3ZlQ2xhc3MoXCJzaG93IGFjdGl2ZVwiKS5maXJzdCgpLmFkZENsYXNzKFwic2hvdyBhY3RpdmVcIik7XG4gICAgJChcIi5jLXRhYi1saW5rRVwiKS5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKS5wYXJlbnQoKS5maXJzdCgpLmZpbmQoXCIuYy10YWItbGlua0VcIikuYWRkQ2xhc3MoXCJhY3RpdmVcIik7XG4gICAgc3RyID0gMDtcbiAgfVxuICBpZiAod2luZG93LmlubmVyV2lkdGggPCA5OTIgJiYgc3RyID09IDApIHtcbiAgICAkKFwiLnRhYi1wYW5lXCIpLmFkZENsYXNzKFwic2hvdyBhY3RpdmVcIik7XG4gICAgc3RyID0gMTtcbiAgfVxuICAvLyBjb25zb2xlLmxvZyhzdHIpO1xufVxuXG5mdW5jdGlvbiB0b2dnbGVwRmlsdGVyU3VibWVudSgpIHtcbiAgLy8gVE9ETzog5pyJ56m66Kmm6Kmm55yL5a+r5LiA5YCL5omL5qmf54mI5Y+v5Lul6Yed5bCN5YWn5a656auY5bqm5aKe5Yqgc2hvd21vcmXvvIzkuKbkuJToqJjpjITmraRzaG93bW9yZeW3sue2k+m7numWi+mBju+8jOS4jeacg+WboOeCunJ3ZOWwseWPiOioiOeul+S4gOasoVxuICAvL+aJi+apn+eJiOmhr+ekuuWFqOmDqFxuICBpZiAoZG9jdW1lbnQucXVlcnlTZWxlY3RvcignLnAtcHJvZHVjdHMtc2VhcmNoLXNob3dNb3JlJykpIHtcbiAgICBuZXcgQWNjb3JkaW9uKFwiLnAtcHJvZHVjdHMtc2VhcmNoLXNob3dNb3JlXCIsIHVuZGVmaW5lZCwgZmFsc2UpLmluaXQoKTtcbiAgfVxuICB2YXIgdHJpZ2dlcnMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKFwiLmMtYWNjb3JkaW9uLWJ0blwiKTtcbiAgbm9kZUxpc3RUb0FycmF5KHRyaWdnZXJzKS5mb3JFYWNoKGZ1bmN0aW9uICh0cmlnZ2VyKSB7XG4gICAgdmFyIGVsID0gZG9jdW1lbnQucXVlcnlTZWxlY3Rvcih0cmlnZ2VyLmdldEF0dHJpYnV0ZShcImRhdGEtdGFyZ2V0XCIpKTtcbiAgICB0cmlnZ2VyLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBmdW5jdGlvbiAoKSB7XG4gICAgICBpZiAoZWwucXVlcnlTZWxlY3RvcihcIi5wLXByb2R1Y3RzLXNlYXJjaC1zaG93TW9yZVwiKSkge1xuICAgICAgICBlbC5xdWVyeVNlbGVjdG9yKFwiLnAtcHJvZHVjdHMtc2VhcmNoLXNob3dNb3JlXCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1hY2NvcmRpb25FeHBlbmRlZFwiKTtcbiAgICAgICAgZWwucXVlcnlTZWxlY3RvcihcIi5jLWFjY29yZGlvbi1jb250ZW50LWxheW91dFwiKS5jbGFzc0xpc3QucmVtb3ZlKFwianMtYWNjb3JkaW9uRXhwZW5kZWRcIik7XG4gICAgICB9XG4gICAgfSk7XG4gIH0pO1xuICAvL+aJi+apn+eJiOmWi+mXnOmBuOWWrlxuICAvL+WVhuWTgeWwiOWNgFxuICB0b2dnbGVWaXNpYWJsZShcIi5jbG9zZVwiLCBcIi5wLXByb2R1Y3RzLXNlYXJjaFwiLCBcIlwiKTtcbiAgdG9nZ2xlVmlzaWFibGUoXCIucC1wcm9kdWN0cy1zZWFyY2gtYnRuXCIsIFwiLnAtcHJvZHVjdHMtc2VhcmNoXCIsIDk5Mik7XG4gIGNsaWNrQ29uZmlybShcIiNqcy1jb25maXJtXCIsIFwiLnAtcHJvZHVjdHMtc2VhcmNoXCIpO1xuICAvL+mWgOW4guafpeipolxuICB0b2dnbGVWaXNpYWJsZShcIi52LWRyb3Bkb3duLWJ0blwiLCBcIi52LWRyb3Bkb3duLW1lbnVcIiwgOTkyKTtcbiAgdG9nZ2xlVmlzaWFibGUoXCIuY2xvc2VcIiwgXCIudi1kcm9wZG93bi1tZW51XCIsIFwiXCIpO1xuICBjbGlja0NvbmZpcm0oXCIjanMtY29uZmlybVwiLCBcIi52LWRyb3Bkb3duLW1lbnVcIik7XG4gIC8vIGlmICgkKFwiLnYtZHJvcGRvd24tYnRuXCIpLmhhc0NsYXNzKFwianMtYWN0aXZlXCIpKSB7XG4gIC8vICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcImJvZHlcIikuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIGZ1bmN0aW9uICgpIHtcbiAgLy8gICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIudi1kcm9wZG93bi1idG5cIikuY2xhc3NMaXN0LnJlbW92ZShcImpzLWFjdGl2ZVwiKTtcbiAgLy8gICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIudi1kcm9wZG93bi1tZW51XCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1hY3RpdmVcIik7XG4gIC8vICAgfSk7XG4gIC8vIH1cblxufVxuXG5mdW5jdGlvbiBjb25maXJtSWZEb3VibGVNZW51T3BlbmVkKGVsLCBtZWRpYVF1ZXJ5KSB7XG4gIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA8IG1lZGlhUXVlcnkgJiYgJChlbCkuaGFzQ2xhc3MoXCJqcy1hY3RpdmVcIikgJiYgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTGlzdCA9PSBcIlwiKSB7XG4gICAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTGlzdC5hZGQoXCJqcy1tZW51T3BlbmVkXCIpO1xuICB9XG59XG4vLyDovKrmkq3nm7jpl5wtLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLVxuZnVuY3Rpb24gU2xpY2soZWwsIHNsaWRlc1RvU2hvdywgc2xpZGVzUGVyUm93LCByb3dzLCByZXNwb25zaXZlKSB7XG4gIHRoaXMuZWwgPSBlbDtcbiAgdGhpcy5zbGlkZXNUb1Nob3cgPSBzbGlkZXNUb1Nob3c7XG4gIHRoaXMuc2xpZGVzUGVyUm93ID0gc2xpZGVzUGVyUm93O1xuICB0aGlzLnJvd3MgPSByb3dzO1xuICB0aGlzLnJlc3BvbnNpdmUgPSByZXNwb25zaXZlO1xufTtcblxuU2xpY2sucHJvdG90eXBlLmluaXQgPSBmdW5jdGlvbiAoKSB7XG4gICQodGhpcy5lbCArIFwiIC52LXNsaWNrXCIpLnNsaWNrKHtcbiAgICBhcnJvd3M6IHRydWUsXG4gICAgc2xpZGVzVG9TaG93OiB0aGlzLnNsaWRlc1RvU2hvdyxcbiAgICBzbGlkZXNQZXJSb3c6IHRoaXMuc2xpZGVzUGVyUm93LFxuICAgIHJvd3M6IHRoaXMucm93cyxcbiAgICBwcmV2QXJyb3c6IGA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cInYtc2xpY2stYXJyb3dzLXByZXZcIj48aSBjbGFzcz1cImZhbCBmYS1hbmdsZS1sZWZ0XCI+PC9pPjwvYnV0dG9uPmAsXG4gICAgbmV4dEFycm93OiBgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJ2LXNsaWNrLWFycm93cy1uZXh0XCI+PGkgY2xhc3M9XCJmYWwgZmEtYW5nbGUtcmlnaHRcIj48L2k+PC9idXR0b24+YCxcbiAgICAvLyBhdXRvcGxheTogdHJ1ZSxcbiAgICAvLyBsYXp5TG9hZDogXCJwcm9ncmVzc2l2ZVwiLFxuICAgIHJlc3BvbnNpdmU6IHRoaXMucmVzcG9uc2l2ZSxcbiAgfSk7XG59O1xuXG5mdW5jdGlvbiBzZXRTdG9yZVRhYmxlSGVpZ2h0QXRNb2JpbGUoKSB7XG4gIHRyaWdnZXJzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnLmMtc2hvcFRhYmxlLXJ3ZC12ZXJBIHRib2R5IHRyJyk7XG4gIG5vZGVMaXN0VG9BcnJheSh0cmlnZ2VycykuZm9yRWFjaChmdW5jdGlvbiAodHJpZ2dlcikge1xuICAgIHZhciB0cmlnZ2VyVGQgPSB0cmlnZ2VyLnF1ZXJ5U2VsZWN0b3JBbGwoJ3RkJyk7XG4gICAgdmFyIG51bTAxID0gdHJpZ2dlclRkWzBdLmNsaWVudEhlaWdodDtcbiAgICB2YXIgbnVtMDIgPSB0cmlnZ2VyVGRbMV0uY2xpZW50SGVpZ2h0O1xuICAgIHZhciBudW0wMyA9IHRyaWdnZXJUZFsyXS5jbGllbnRIZWlnaHQ7XG4gICAgdmFyIHRvdGFsTnVtID0gbnVtMDEgKyBudW0wMiArIG51bTAzO1xuICAgIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA8IDEyMDAgJiYgdHJpZ2dlci5jbGFzc0xpc3QuY29udGFpbnMoJ2pzLWFjdGl2ZScpID09IGZhbHNlKSB7XG4gICAgICB0cmlnZ2VyLnN0eWxlLmhlaWdodCA9IGAke3RvdGFsTnVtfXB4YDtcbiAgICB9IGVsc2UgaWYgKHdpbmRvdy5pbm5lcldpZHRoID4gMTIwMCkge1xuICAgICAgdHJpZ2dlci5zdHlsZS5oZWlnaHQgPSBcIlwiO1xuICAgICAgdHJpZ2dlci5jbGFzc0xpc3QucmVtb3ZlKCdqcy1hY3RpdmUnKTtcbiAgICAgIHRyaWdnZXIucXVlcnlTZWxlY3RvcihcImlcIikuY2xhc3NMaXN0LnJlbW92ZShcImpzLXN1Ym1lbnVPcGVuZWRcIik7XG4gICAgfVxuICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwicmVzaXplXCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA8IDEyMDAgJiYgdHJpZ2dlci5jbGFzc0xpc3QuY29udGFpbnMoJ2pzLWFjdGl2ZScpID09IGZhbHNlKSB7XG4gICAgICAgIHRyaWdnZXIuc3R5bGUuaGVpZ2h0ID0gYCR7dG90YWxOdW19cHhgO1xuICAgICAgfSBlbHNlIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA+IDEyMDApIHtcbiAgICAgICAgdHJpZ2dlci5zdHlsZS5oZWlnaHQgPSBcIlwiO1xuICAgICAgICB0cmlnZ2VyLmNsYXNzTGlzdC5yZW1vdmUoJ2pzLWFjdGl2ZScpO1xuICAgICAgICB0cmlnZ2VyLnF1ZXJ5U2VsZWN0b3IoXCJpXCIpLmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1zdWJtZW51T3BlbmVkXCIpO1xuICAgICAgfVxuICAgIH0pO1xuICB9KTtcbn1cblxuZnVuY3Rpb24gdG9nZ2xlU3RvcmVUYWJsZUhlaWdodEF0TW9iaWxlKCkge1xuICB0cmlnZ2VycyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoJy5jLXNob3BUYWJsZS1yd2QtdmVyQSB0Ym9keSB0cicpO1xuICBub2RlTGlzdFRvQXJyYXkodHJpZ2dlcnMpLmZvckVhY2goZnVuY3Rpb24gKHRyaWdnZXIpIHtcbiAgICB2YXIgdHJpZ2dlclRkID0gdHJpZ2dlci5xdWVyeVNlbGVjdG9yQWxsKCd0ZCcpO1xuICAgIHZhciBudW0wMSA9IHRyaWdnZXJUZFswXS5jbGllbnRIZWlnaHQ7XG4gICAgdmFyIG51bTAyID0gdHJpZ2dlclRkWzFdLmNsaWVudEhlaWdodDtcbiAgICB2YXIgbnVtMDMgPSB0cmlnZ2VyVGRbMl0uY2xpZW50SGVpZ2h0O1xuICAgIHZhciB0b3RhbE51bSA9IG51bTAxICsgbnVtMDIgKyBudW0wMztcbiAgICB0cmlnZ2VyLnF1ZXJ5U2VsZWN0b3IoXCJ0ZDpmaXJzdC1jaGlsZFwiKS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uICgpIHtcbiAgICAgIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA8IDEyMDAgJiYgdHJpZ2dlci5zdHlsZS5oZWlnaHQgPT0gXCJcIikge1xuICAgICAgICB0cmlnZ2VyLnN0eWxlLmhlaWdodCA9IGAke3RvdGFsTnVtfXB4YDtcbiAgICAgICAgdHJpZ2dlci5jbGFzc0xpc3QucmVtb3ZlKCdqcy1hY3RpdmUnKTtcbiAgICAgICAgdHJpZ2dlci5xdWVyeVNlbGVjdG9yKFwiaVwiKS5jbGFzc0xpc3QucmVtb3ZlKFwianMtc3VibWVudU9wZW5lZFwiKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIHRyaWdnZXIuc3R5bGUuaGVpZ2h0ID0gXCJcIjtcbiAgICAgICAgdHJpZ2dlci5jbGFzc0xpc3QuYWRkKFwianMtYWN0aXZlXCIpO1xuICAgICAgICB0cmlnZ2VyLnF1ZXJ5U2VsZWN0b3IoXCJpXCIpLmNsYXNzTGlzdC5hZGQoXCJqcy1zdWJtZW51T3BlbmVkXCIpO1xuICAgICAgfVxuICAgIH0pO1xuICB9KTtcbn1cblxuLy8gVE9ETzog5pyJ56m655yL5LiA5LiL54K65LuA6bq8bmV356ys5LiJ5YCL5bCx5Ye65LqL5LqGXG5mdW5jdGlvbiBjcmVhdGVTbGlja3MoKSB7XG4gIGlmIChkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLnYtc2xpY2tcIikpIHtcbiAgICB2YXIgdGFyZ2V0cyA9IFtcIiNuZXdzXCIsIFwiI3JlY29tbWFuZGF0aW9uXCJdO1xuICAgIHRhcmdldHMuZm9yRWFjaChmdW5jdGlvbiAodGFyZ2V0KSB7XG4gICAgICBuZXcgU2xpY2sodGFyZ2V0LCA0LCAxLCAxLCBbe1xuICAgICAgICBicmVha3BvaW50OiA5OTIsXG4gICAgICAgIHNldHRpbmdzOiB7XG4gICAgICAgICAgc2xpZGVzVG9TaG93OiAyXG4gICAgICAgIH1cbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIGJyZWFrcG9pbnQ6IDU3NixcbiAgICAgICAgc2V0dGluZ3M6IHtcbiAgICAgICAgICBzbGlkZXNUb1Nob3c6IDIsXG4gICAgICAgICAgYXJyb3dzOiBmYWxzZSxcbiAgICAgICAgfVxuICAgICAgfSxcbiAgICAgIF0pLmluaXQoKTtcbiAgICB9KTtcbiAgICBuZXcgU2xpY2soXCIjdmlkZW9cIiwgMSwgMiwgMiwgW3tcbiAgICAgIGJyZWFrcG9pbnQ6IDk5MixcbiAgICAgIHNldHRpbmdzOiB7XG4gICAgICAgIHNsaWRlc1RvU2hvdzogMixcbiAgICAgICAgc2xpZGVzUGVyUm93OiAxLFxuICAgICAgICByb3dzOiAxLFxuICAgICAgfVxuICAgIH0sXG4gICAge1xuICAgICAgYnJlYWtwb2ludDogNTc2LFxuICAgICAgc2V0dGluZ3M6IHtcbiAgICAgICAgc2xpZGVzVG9TaG93OiAyLFxuICAgICAgICBzbGlkZXNQZXJSb3c6IDEsXG4gICAgICAgIHJvd3M6IDEsXG4gICAgICAgIGFycm93czogZmFsc2UsXG4gICAgICB9XG4gICAgfSxcbiAgICBdKS5pbml0KCk7XG4gIH1cbn1cblxuZnVuY3Rpb24gcmlvbmV0U2xpY2soKSB7XG4gIHZhciBlbCA9IFwiLnYtcmlvbmV0U2xpY2tcIjtcbiAgaWYgKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoZWwpKSB7XG4gICAgJChlbCkuc2xpY2soe1xuICAgICAgLy8gY2VudGVyTW9kZTogdHJ1ZSxcbiAgICAgIC8vIGNlbnRlclBhZGRpbmc6ICc2MHB4JyxcbiAgICAgIHNsaWRlc1RvU2hvdzogNCxcbiAgICAgIGFycm93czogdHJ1ZSxcbiAgICAgIHByZXZBcnJvdzogYDxidXR0b24gdHlwZT1cImJ1dHRvblwiIGNsYXNzPVwidi10YWJCdG5TbGljay1wcmV2XCI+PGkgY2xhc3M9XCJmYWwgZmEtYW5nbGUtbGVmdFwiPjwvaT48L2J1dHRvbj5gLFxuICAgICAgbmV4dEFycm93OiBgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJ2LXRhYkJ0blNsaWNrLW5leHRcIj48aSBjbGFzcz1cImZhbCBmYS1hbmdsZS1yaWdodFwiPjwvaT48L2J1dHRvbj5gLFxuICAgICAgbGF6eUxvYWQ6IFwicHJvZ3Jlc3NpdmVcIixcbiAgICAgIHJlc3BvbnNpdmU6IFt7XG4gICAgICAgIGJyZWFrcG9pbnQ6IDk5MixcbiAgICAgICAgc2V0dGluZ3M6IHtcbiAgICAgICAgICBzbGlkZXNUb1Nob3c6IDMsXG4gICAgICAgICAgZG90czogdHJ1ZSxcbiAgICAgICAgfVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgYnJlYWtwb2ludDogNTc2LFxuICAgICAgICBzZXR0aW5nczoge1xuICAgICAgICAgIHNsaWRlc1RvU2hvdzogMixcbiAgICAgICAgICBhcnJvd3M6IGZhbHNlLFxuICAgICAgICAgIC8vIHZhcmlhYmxlV2lkdGg6IHRydWUsXG4gICAgICAgICAgZG90czogdHJ1ZSxcbiAgICAgICAgfVxuICAgICAgfSxcbiAgICAgIF0sXG4gICAgfSk7XG4gICAgLy8gICAkKCcudi10YWJCdG5TbGljay1wcmV2Jykub24oJ2NsaWNrJywgZnVuY3Rpb24oKXtcbiAgICAvLyAgICAgJCgnLnYtdGFiQnRuU2xpY2snKS5zbGljaygnc2xpY2tQcmV2Jyk7XG4gICAgLy8gIH0pO1xuICAgIC8vICAgJCgnLnYtdGFiQnRuU2xpY2stbmV4dCcpLm9uKCdjbGljaycsIGZ1bmN0aW9uKCl7XG4gICAgLy8gICAgICQoJy52LXRhYkJ0blNsaWNrJykuc2xpY2soJ3NsaWNrTmV4dCcpO1xuICAgIC8vICB9KTtcbiAgfVxufVxuXG5mdW5jdGlvbiB0YWJCdG5TbGljaygpIHtcbiAgdmFyIGVsID0gXCIudi10YWJCdG5TbGlja1wiO1xuICBpZiAoZG9jdW1lbnQucXVlcnlTZWxlY3RvcihlbCkpIHtcbiAgICB2YXIgdGFiTGVuZ3RoID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihlbCkucXVlcnlTZWxlY3RvckFsbCgnLmMtdGFiLWNvbnRhaW5lcicpLmxlbmd0aDtcbiAgICB2YXIgc2xpZGVzVG9TaG93TnVtID0gKHRhYkxlbmd0aCA+IDYpID8gNiA6IHRhYkxlbmd0aDtcbiAgICAvLyBjb25zb2xlLmxvZyhcInNsaWRlc1RvU2hvd051bSA9IFwiICsgc2xpZGVzVG9TaG93TnVtKTtcbiAgICAkKGVsKS5zbGljayh7XG4gICAgICBzbGlkZXNUb1Nob3c6IHNsaWRlc1RvU2hvd051bSxcbiAgICAgIHNsaWRlc1RvU2Nyb2xsOiAxLFxuICAgICAgYXJyb3dzOiB0cnVlLFxuICAgICAgcHJldkFycm93OiBgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJ2LXRhYkJ0blNsaWNrLXByZXZcIj48aSBjbGFzcz1cImZhbCBmYS1hbmdsZS1sZWZ0XCI+PC9pPjwvYnV0dG9uPmAsXG4gICAgICBuZXh0QXJyb3c6IGA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cInYtdGFiQnRuU2xpY2stbmV4dFwiPjxpIGNsYXNzPVwiZmFsIGZhLWFuZ2xlLXJpZ2h0XCI+PC9pPjwvYnV0dG9uPmAsXG4gICAgICBsYXp5TG9hZDogXCJwcm9ncmVzc2l2ZVwiLFxuICAgICAgaW5maW5pdGU6IGZhbHNlLFxuICAgICAgcmVzcG9uc2l2ZTogW3tcbiAgICAgICAgYnJlYWtwb2ludDogMTIwMCxcbiAgICAgICAgc2V0dGluZ3M6IHtcbiAgICAgICAgICBzbGlkZXNUb1Nob3c6IDQsXG4gICAgICAgICAgdmFyaWFibGVXaWR0aDogZmFsc2UsXG4gICAgICAgICAgLy8gc2xpZGVzVG9TY3JvbGw6IDUsXG4gICAgICAgICAgLy8gaW5maW5pdGU6IHRydWUsXG4gICAgICAgICAgLy8gY2VudGVyTW9kZTogZmFsc2UsXG4gICAgICAgIH1cbiAgICAgIH0sIHtcbiAgICAgICAgYnJlYWtwb2ludDogOTkyLFxuICAgICAgICBzZXR0aW5nczoge1xuICAgICAgICAgIHNsaWRlc1RvU2hvdzogNCxcbiAgICAgICAgICB2YXJpYWJsZVdpZHRoOiBmYWxzZSxcbiAgICAgICAgICAvLyBpbmZpbml0ZTogdHJ1ZSxcbiAgICAgICAgICAvLyBjZW50ZXJNb2RlOiBmYWxzZSxcbiAgICAgICAgfVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgYnJlYWtwb2ludDogNzY4LFxuICAgICAgICBzZXR0aW5nczoge1xuICAgICAgICAgIHNsaWRlc1RvU2hvdzogMSxcbiAgICAgICAgICB2YXJpYWJsZVdpZHRoOiB0cnVlLFxuICAgICAgICAgIC8vIGluZmluaXRlOiBmYWxzZSxcbiAgICAgICAgICBhcnJvd3M6IGZhbHNlLFxuICAgICAgICB9XG4gICAgICB9LFxuICAgICAgXSxcbiAgICB9KTtcbiAgfVxufVxuXG5mdW5jdGlvbiB0YWJCdG5TbGlja0ZpeGVkKCkge1xuICB2YXIgZWwgPSBcIi52LXRhYkJ0blNsaWNrXCI7XG4gIGlmIChkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGVsKSkge1xuICAgIHZhciB0YWJMZW5ndGggPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGVsKS5xdWVyeVNlbGVjdG9yQWxsKCcuYy10YWItY29udGFpbmVyJykubGVuZ3RoO1xuICAgIC8vIHZhciBzbGlkZXNUb1Nob3dOdW0gPSAodGFiTGVuZ3RoID4gNikgPyA2IDogdGFiTGVuZ3RoO1xuICAgIC8vIGNvbnNvbGUubG9nKHRhYkxlbmd0aCk7XG4gICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoID49IDEyMDAgJiYgdGFiTGVuZ3RoIDw9IDYpIHtcbiAgICAgICQoXCIuc2xpY2stdHJhY2tcIikuYWRkQ2xhc3MoXCJ0ZXh0LW1kLWNlbnRlclwiKTtcbiAgICAgIC8vIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zbGljay10cmFjaycpLmNsYXNzTGlzdC5hZGQoJ3RleHQtbWQtY2VudGVyJyk7XG4gICAgfVxuICAgIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA+PSA3NjggJiYgd2luZG93LmlubmVyV2lkdGggPCAxMjAwICYmIHRhYkxlbmd0aCA8IDUpIHtcbiAgICAgICQoXCIuc2xpY2stdHJhY2tcIikuYWRkQ2xhc3MoXCJ0ZXh0LW1kLWNlbnRlclwiKTtcbiAgICAgIC8vIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zbGljay10cmFjaycpLmNsYXNzTGlzdC5hZGQoJ3RleHQtbWQtY2VudGVyJyk7XG4gICAgfVxuICAgIC8vIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA8IDc2OCkge1xuICAgIC8vIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJy5zbGljay10cmFjaycpLmNsYXNzTGlzdC5yZW1vdmUoJ3RleHQtbWQtY2VudGVyJyk7XG4gICAgLy8gfVxuICB9XG59XG5cbmZ1bmN0aW9uIHRhYkJ0blNsaWNrTWVtYmVycygpIHtcbiAgdmFyIGVsID0gXCIudi10YWJCdG5TbGljay1tZW1iZXJzXCI7XG4gIGlmIChkb2N1bWVudC5xdWVyeVNlbGVjdG9yKGVsKSkge1xuICAgICQoZWwpLnNsaWNrKHtcbiAgICAgIHNsaWRlc1RvU2hvdzogNixcbiAgICAgIHNsaWRlc1RvU2Nyb2xsOiAxLFxuICAgICAgYXJyb3dzOiB0cnVlLFxuICAgICAgcHJldkFycm93OiBgPGJ1dHRvbiB0eXBlPVwiYnV0dG9uXCIgY2xhc3M9XCJ2LXRhYkJ0blNsaWNrLXByZXZcIj48aSBjbGFzcz1cImZhbCBmYS1hbmdsZS1sZWZ0XCI+PC9pPjwvYnV0dG9uPmAsXG4gICAgICBuZXh0QXJyb3c6IGA8YnV0dG9uIHR5cGU9XCJidXR0b25cIiBjbGFzcz1cInYtdGFiQnRuU2xpY2stbmV4dFwiPjxpIGNsYXNzPVwiZmFsIGZhLWFuZ2xlLXJpZ2h0XCI+PC9pPjwvYnV0dG9uPmAsXG4gICAgICBsYXp5TG9hZDogXCJwcm9ncmVzc2l2ZVwiLFxuICAgICAgaW5maW5pdGU6IGZhbHNlLFxuICAgICAgcmVzcG9uc2l2ZTogW3tcbiAgICAgICAgYnJlYWtwb2ludDogOTkyLFxuICAgICAgICBzZXR0aW5nczoge1xuICAgICAgICAgIHNsaWRlc1RvU2hvdzogNSxcbiAgICAgICAgICB2YXJpYWJsZVdpZHRoOiBmYWxzZSxcbiAgICAgICAgfVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgYnJlYWtwb2ludDogNzY4LFxuICAgICAgICBzZXR0aW5nczoge1xuICAgICAgICAgIHNsaWRlc1RvU2hvdzogMSxcbiAgICAgICAgICB2YXJpYWJsZVdpZHRoOiB0cnVlLFxuICAgICAgICAgIGFycm93czogZmFsc2UsXG4gICAgICAgIH1cbiAgICAgIH0sXG4gICAgICBdLFxuICAgIH0pO1xuICB9XG59XG5cbmZ1bmN0aW9uIHRhYkJ0blNsaWNrRml4ZWRNZW1iZXJzKCkge1xuICB2YXIgZWwgPSBcIi52LXRhYkJ0blNsaWNrLW1lbWJlcnNcIjtcbiAgaWYgKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoZWwpKSB7XG4gICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoID49IDc2OCkge1xuICAgICAgJChcIi52LXRhYkJ0blNsaWNrLW1lbWJlcnMgLnNsaWNrLXRyYWNrXCIpLmFkZENsYXNzKFwidGV4dC1tZC1jZW50ZXJcIik7XG4gICAgfVxuICB9XG59XG4vLyBFbmQg6Lyq5pKt55u46ZecLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS0tLS1cbmZ1bmN0aW9uIHdhcnJhbnR5TGluaygpIHtcbiAgaWYgKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCIucC1pbmRleC13YXJyYW50eS1saW5rXCIpLmxlbmd0aCkge1xuICAgIHZhciB0cmlnZ2VycyA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3JBbGwoXCIucC1pbmRleC13YXJyYW50eS1saW5rXCIpO1xuICAgIG5vZGVMaXN0VG9BcnJheSh0cmlnZ2VycykuZm9yRWFjaChmdW5jdGlvbiAodHJpZ2dlcikge1xuICAgICAgdHJpZ2dlci5hZGRFdmVudExpc3RlbmVyKFwibW91c2VvdmVyXCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdHJpZ2dlci5xdWVyeVNlbGVjdG9yKFwiLmMtYnRuXCIpLmNsYXNzTGlzdC5hZGQoXCJqcy1idG5Ib3ZlclwiKTtcbiAgICAgIH0pO1xuICAgICAgdHJpZ2dlci5hZGRFdmVudExpc3RlbmVyKFwibW91c2VvdXRcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgICB0cmlnZ2VyLnF1ZXJ5U2VsZWN0b3IoXCIuYy1idG5cIikuY2xhc3NMaXN0LnJlbW92ZShcImpzLWJ0bkhvdmVyXCIpO1xuICAgICAgfSk7XG4gICAgfSk7XG4gIH1cbn1cbi8v5omL5qmf54mI5ryi5aCh6YG45ZauXG5mdW5jdGlvbiB0b2dnbGVNb2JpbGVNZW51KG1lZGlhUXVlcnkpIHtcbiAgdmFyIHRyaWdnZXIgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiI2hhbWJ1cmdlclwiKTtcbiAgdmFyIHRhcmdldCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIjbWVudVwiKTtcblxuICB0cmlnZ2VyLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBmdW5jdGlvbiAoKSB7XG4gICAgdGhpcy5jbGFzc0xpc3QudG9nZ2xlKFwianMtbWVudU9wZW5lZFwiKTtcbiAgICB0YXJnZXQuY2xhc3NMaXN0LnRvZ2dsZShcImpzLW1lbnVPcGVuZWRcIik7XG4gICAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTGlzdC50b2dnbGUoXCJqcy1tZW51T3BlbmVkXCIpO1xuICB9KTtcblxuICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcInJlc2l6ZVwiLCBmdW5jdGlvbiAoKSB7XG4gICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoID49IG1lZGlhUXVlcnkpIHtcbiAgICAgIHRyaWdnZXIuY2xhc3NMaXN0LnJlbW92ZShcImpzLW1lbnVPcGVuZWRcIik7XG4gICAgICB0YXJnZXQuY2xhc3NMaXN0LnJlbW92ZShcImpzLW1lbnVPcGVuZWRcIik7XG4gICAgICBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZShcImpzLW1lbnVPcGVuZWRcIik7XG4gICAgfVxuICB9KTtcbn1cbi8v5omL5qmf54mI6aKo55C05oqY55aK6YG45ZauXG5mdW5jdGlvbiB0b2dnbGVNb2JpbGVTdWJtZW51KG1lZGlhUXVlcnkpIHtcbiAgdmFyIHRyaWdnZXJzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIi5sLWhlYWRlci1tZW51LWxpbmtcIik7XG4gIHZhciB0YXJnZXRzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIi5sLWhlYWRlci1zdWJtZW51LS1oaWRkZW5cIik7XG4gIG5vZGVMaXN0VG9BcnJheSh0cmlnZ2VycykuZm9yRWFjaChmdW5jdGlvbiAodHJpZ2dlcikge1xuICAgIGlmICh0cmlnZ2VyLm5leHRFbGVtZW50U2libGluZyAhPT0gbnVsbCkge1xuICAgICAgdHJpZ2dlci5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgZnVuY3Rpb24gKGUpIHtcbiAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICB2YXIgdGFyZ2V0ID0gdHJpZ2dlci5uZXh0RWxlbWVudFNpYmxpbmc7XG4gICAgICAgIGlmICh3aW5kb3cuaW5uZXJXaWR0aCA8IG1lZGlhUXVlcnkpIHtcbiAgICAgICAgICB0YXJnZXQuY2xhc3NMaXN0LnRvZ2dsZShcImpzLXN1Ym1lbnVPcGVuZWRcIik7XG4gICAgICAgICAgdHJpZ2dlci5xdWVyeVNlbGVjdG9yKFwiaVwiKS5jbGFzc0xpc3QudG9nZ2xlKFwianMtc3VibWVudU9wZW5lZFwiKTtcbiAgICAgICAgICAvL+eCuuS6humWi+S4gOWAi+mXnOS4gOWAi++8jOeUqGpRdWVyeeWKoOWvq1xuICAgICAgICAgICQodGFyZ2V0KS5wYXJlbnRzKCkuc2libGluZ3MoKS5maW5kKFwiLmwtaGVhZGVyLXN1Ym1lbnUgLCBpXCIpLnJlbW92ZUNsYXNzKFwianMtc3VibWVudU9wZW5lZFwiKTtcbiAgICAgICAgfVxuICAgICAgfSk7XG4gICAgfVxuICB9KTtcblxuICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcihcInJlc2l6ZVwiLCBmdW5jdGlvbiAoKSB7XG4gICAgaWYgKHdpbmRvdy5pbm5lcldpZHRoID49IG1lZGlhUXVlcnkpIHtcbiAgICAgIG5vZGVMaXN0VG9BcnJheSh0cmlnZ2VycykuZm9yRWFjaChmdW5jdGlvbiAodHJpZ2dlcikge1xuICAgICAgICB2YXIgdGFyZ2V0ID0gdHJpZ2dlci5uZXh0RWxlbWVudFNpYmxpbmc7XG4gICAgICAgIHRhcmdldC5jbGFzc0xpc3QucmVtb3ZlKFwianMtc3VibWVudU9wZW5lZFwiKTtcbiAgICAgICAgdHJpZ2dlci5xdWVyeVNlbGVjdG9yKFwiaVwiKS5jbGFzc0xpc3QucmVtb3ZlKFwianMtc3VibWVudU9wZW5lZFwiKTtcbiAgICAgIH0pO1xuICAgIH1cbiAgfSk7XG59XG5cbmZ1bmN0aW9uIHNpYmluZ01vYmlsZVN1Ym1lbnUoKSB7XG4gICQoXCIubC1oZWFkZXItbWVudS1pdGVtXCIpLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24gKCkge1xuICAgICQodGhpcykuc2libGluZ3MoKS5maW5kKFwiLmwtaGVhZGVyLXN1Ym1lbnVcIikucmVtb3ZlQ2xhc3MoXCJqcy1zdWJtZW51T3BlbmVkXCIpO1xuICB9KTtcbn1cblxuZnVuY3Rpb24gdG9nZ2xlUGNIb3ZlclN0YXRlKG1lZGlhUXVlcnkpIHtcbiAgdmFyIHRyaWdnZXJzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIi5sLWhlYWRlci1tZW51LWl0ZW1cIik7XG4gIG5vZGVMaXN0VG9BcnJheSh0cmlnZ2VycykuZm9yRWFjaChmdW5jdGlvbiAodHJpZ2dlcikge1xuICAgIGlmICh0cmlnZ2VyLnF1ZXJ5U2VsZWN0b3IoXCIubC1oZWFkZXItc3VibWVudVwiKSAhPT0gbnVsbCkge1xuICAgICAgdHJpZ2dlci5hZGRFdmVudExpc3RlbmVyKFwibW91c2VvdmVyXCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdHJpZ2dlci5xdWVyeVNlbGVjdG9yKFwiLmwtaGVhZGVyLW1lbnUtbGlua1wiKS5jbGFzc0xpc3QuYWRkKFwianMtaG92ZXJcIik7XG4gICAgICB9KTtcbiAgICAgIHRyaWdnZXIuYWRkRXZlbnRMaXN0ZW5lcihcIm1vdXNlb3V0XCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdHJpZ2dlclxuICAgICAgICAgIC5xdWVyeVNlbGVjdG9yKFwiLmwtaGVhZGVyLW1lbnUtbGlua1wiKVxuICAgICAgICAgIC5jbGFzc0xpc3QucmVtb3ZlKFwianMtaG92ZXJcIik7XG4gICAgICB9KTtcbiAgICB9XG4gIH0pO1xuXG4gIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKFwicmVzaXplXCIsIGZ1bmN0aW9uICgpIHtcbiAgICBpZiAod2luZG93LmlubmVyV2lkdGggPCBtZWRpYVF1ZXJ5KSB7XG4gICAgICBBcnJheS5wcm90b3R5cGUuc2xpY2VcbiAgICAgICAgLmNhbGwoZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIi5sLWhlYWRlci1tZW51LWxpbmtcIikpXG4gICAgICAgIC5mb3JFYWNoKGZ1bmN0aW9uIChpdGVtKSB7XG4gICAgICAgICAgaXRlbS5jbGFzc0xpc3QucmVtb3ZlKFwianMtaG92ZXJcIik7XG4gICAgICAgIH0pO1xuICAgIH1cbiAgfSk7XG59XG5cbmZ1bmN0aW9uIGhlYWRlckZ1bmN0aW9uKCkge1xuICB2YXIgYnJlYWtwb2ludCA9IDEyMDA7XG4gIHRvZ2dsZU1vYmlsZU1lbnUoYnJlYWtwb2ludCk7XG4gIHRvZ2dsZU1vYmlsZVN1Ym1lbnUoYnJlYWtwb2ludCk7XG4gIHRvZ2dsZVBjSG92ZXJTdGF0ZShicmVha3BvaW50KTtcbn1cblxuZnVuY3Rpb24gbGF6eUxvYWQoKSB7XG4gIGlmIChkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiaW1nW2RhdGEtc3JjXVwiKSkge1xuICAgIHZhciBvYnNlcnZlciA9IGxvemFkKCk7XG4gICAgb2JzZXJ2ZXIub2JzZXJ2ZSgpO1xuICAgIGlmIChcbiAgICAgIC0xICE9PSBuYXZpZ2F0b3IudXNlckFnZW50LmluZGV4T2YoXCJNU0lFXCIpIHx8XG4gICAgICBuYXZpZ2F0b3IuYXBwVmVyc2lvbi5pbmRleE9mKFwiVHJpZGVudC9cIikgPiAwXG4gICAgKSB7XG4gICAgICB2YXIgaWZyYW1lID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcImlmcmFtZVwiKTtcbiAgICAgIGlmcmFtZS5zZXRBdHRyaWJ1dGUoXCJzcmNcIiwgXCJodHRwczovL3d3dy55b3V0dWJlLmNvbS9lbWJlZC9PN3BOcFIzUHk2OFwiKTtcbiAgICB9XG4gIH1cbn1cbi8v6KiI566X44CM5pyA5paw5raI5oGv44CN44CB44CM5Lq65rCj5o6o6Jam44CN6YCZ6aGe55qE5Y2h54mH5YiX6KGo55qE5qiZ6aGM5a2X5pW477yM6K6T5aSn5a6255qE6auY5bqm5LiA5qijXG5mdW5jdGlvbiBhdXRvRml4SGVpZ2h0KGNvbikge1xuICB2YXIgZWwgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKGNvbik7XG4gIHZhciB0aGlzSGVpZ2h0ID0gLTE7XG4gIHZhciBtYXhIZWlnaHQgPSAtMTtcbiAgdmFyIGJyZWFrcG9pbnQgPSA3Njg7XG4gIC8v54K65LqGaWXkuI3mlK/mj7Rub2RlbGlzdOeahGZvckVhY2jkv67mraNcbiAgbm9kZUxpc3RUb0FycmF5KGVsKS5mb3JFYWNoKGZ1bmN0aW9uIChpdGVtKSB7XG4gICAgaXRlbS5zdHlsZS5oZWlnaHQgPSBcIlwiOyAvL+a4heepuuS5i+WJjeeahHN0eWxlXG4gICAgdGhpc0hlaWdodCA9IGl0ZW0uY2xpZW50SGVpZ2h0OyAvL+WPluW+l+W3sue2k+a4m+mBjumrmOW6pueahFxuICAgIG1heEhlaWdodCA9IG1heEhlaWdodCA+IHRoaXNIZWlnaHQgPyBtYXhIZWlnaHQgOiB0aGlzSGVpZ2h0O1xuICB9KTtcbiAgaWYgKGRvY3VtZW50LmJvZHkuY2xpZW50V2lkdGggPiBicmVha3BvaW50KSB7XG4gICAgbm9kZUxpc3RUb0FycmF5KGVsKS5mb3JFYWNoKGZ1bmN0aW9uIChpdGVtKSB7XG4gICAgICBpdGVtLnN0eWxlLmhlaWdodCA9IGAke21heEhlaWdodH1weGA7XG4gICAgfSk7XG4gIH1cbn1cblxuZnVuY3Rpb24gdGV4dEhpZGUobnVtLCBjb24pIHtcbiAgdmFyIGVsID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChjb24pO1xuICAvL2ll5omN5Z+36KGM6YCZ5YCL6LaF6YGO5a2X5pW45aKe5Yqg5Yiq56+A6Jmf77yM5YW25LuW54CP6Ka95Zmo6Z2gY3Nz6Kqe5rOV5Y2z5Y+vXG4gIC8vIGlmIChcbiAgLy8gICBuYXZpZ2F0b3IudXNlckFnZW50LmluZGV4T2YoXCJNU0lFXCIpICE9PSAtMSB8fFxuICAvLyAgIG5hdmlnYXRvci5hcHBWZXJzaW9uLmluZGV4T2YoXCJUcmlkZW50L1wiKSA+IDBcbiAgLy8gKSB7XG4gIC8vICAgQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwoZWwpLmZvckVhY2goZnVuY3Rpb24gKGl0ZW0pIHtcbiAgLy8gICAgIHZhciB0eHQgPSBpdGVtLmlubmVyVGV4dDtcbiAgLy8gICAgIGlmICh0eHQubGVuZ3RoID4gbnVtKSB7XG4gIC8vICAgICAgIHR4dENvbnRlbnQgPSB0eHQuc3Vic3RyaW5nKDAsIG51bSAtIDEpICsgXCIuLi5cIjtcbiAgLy8gICAgICAgaXRlbS5pbm5lckhUTUwgPSB0eHRDb250ZW50O1xuICAvLyAgICAgfVxuICAvLyAgIH0pO1xuICAvLyB9XG4gIG5vZGVMaXN0VG9BcnJheShlbCkuZm9yRWFjaChmdW5jdGlvbiAoaXRlbSkge1xuICAgIHZhciB0eHQgPSBpdGVtLmlubmVyVGV4dDtcbiAgICBpZiAodHh0Lmxlbmd0aCA+IG51bSkge1xuICAgICAgdHh0Q29udGVudCA9IHR4dC5zdWJzdHJpbmcoMCwgbnVtIC0gMSkgKyBcIi4uLlwiO1xuICAgICAgaXRlbS5pbm5lckhUTUwgPSB0eHRDb250ZW50O1xuICAgIH0gZWxzZSB7XG4gICAgICB0eHRDb250ZW50ID0gdHh0LnN1YnN0cmluZygwLCBudW0pICsgXCIuLi5cIjtcbiAgICAgIGl0ZW0uaW5uZXJIVE1MID0gdHh0Q29udGVudDtcbiAgICB9XG4gIH0pO1xuICBhdXRvRml4SGVpZ2h0KGNvbik7XG59XG5cbmZ1bmN0aW9uIGNsZWFyQ2hlY2tCb3goZWwsIHRhcmdldCkge1xuICBpZiAoZG9jdW1lbnQucXVlcnlTZWxlY3RvcihlbCkpIHtcbiAgICB2YXIgdHJpZ2dlciA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoZWwpO1xuICAgIHZhciB0YXJnZXRzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCh0YXJnZXQpO1xuICAgIHRyaWdnZXIuYWRkRXZlbnRMaXN0ZW5lcihcImNsaWNrXCIsIGZ1bmN0aW9uICgpIHtcbiAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICB0cmlnZ2VyLmJsdXIoKTtcbiAgICAgIEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKHRhcmdldHMpLmZvckVhY2goZnVuY3Rpb24gKHRyaWdnZXIpIHtcbiAgICAgICAgdHJpZ2dlci5jaGVja2VkID0gZmFsc2U7XG4gICAgICB9KTtcbiAgICB9KTtcbiAgfVxufVxuXG5mdW5jdGlvbiBjYXJkQ29udGVudEZ1bmN0aW9uKCkge1xuICBpZiAoZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi5jLWNhcmQtYm9keS10aXRsZVwiKSAhPT0gbnVsbCkge1xuICAgIGF1dG9GaXhIZWlnaHQoXCIuYy1jYXJkLWJvZHktdGl0bGVcIik7XG4gIH1cbiAgLy/kurrmsKPmjqjolqblhafmloflrZfmlbjpmZDliLZcbiAgaWYgKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIuYy1jYXJkLWJvZHktdHh0XCIpICE9PSBudWxsKSB7XG4gICAgdGV4dEhpZGUoNDgsIFwiLmMtY2FyZC1ib2R5LXR4dFwiKTtcbiAgfVxufVxuXG5mdW5jdGlvbiB0b29sVGlwcygpIHtcbiAgJCgnW2RhdGEtdG9nZ2xlPVwidG9vbHRpcFwiXScpLnRvb2x0aXAoe1xuICAgIHBsYWNlbWVudDogJ3JpZ2h0JyxcbiAgICB0cmlnZ2VyOiAnaG92ZXInLFxuICAgIG9mZnNldDogMzAsXG4gIH0pO1xuICAkKCdbZGF0YS10b2dnbGU9XCJ0b29sdGlwXCJdJykub24oJ2luc2VydGVkLmJzLnRvb2x0aXAnLCBmdW5jdGlvbiAoKSB7XG4gICAgdmFyIGdldFRvb3RpcHNJZCA9IFwiI1wiICsgJCh0aGlzKS5hdHRyKFwiYXJpYS1kZXNjcmliZWRieVwiKTtcbiAgICAvLyBjb25zb2xlLmxvZyhnZXRUb290aXBzSWQpO1xuICAgIC8vIGNvbnNvbGUubG9nKCQodGhpcykuYXR0cihcImRhdGEtb3JpZ2luYWwtdGl0bGVcIikgPT0gXCLlvrflnIvolKHlj7jmlbjkvY3lvbHlg4/ns7vntbFcIik7XG4gICAgaWYgKCQodGhpcykuYXR0cihcImRhdGEtb3JpZ2luYWwtdGl0bGVcIikgPT0gXCLlvrflnIvolKHlj7jmlbjkvY3lvbHlg4/ns7vntbFcIikge1xuICAgICAgJChnZXRUb290aXBzSWQpLmZpbmQoJy50b29sdGlwLWlubmVyJykuaHRtbChcIuW+t+Wci+iUoeWPuDxicj7mlbjkvY3lvbHlg4/ns7vntbFcIik7XG4gICAgfVxuICAgIGlmICgkKHRoaXMpLmF0dHIoXCJkYXRhLW9yaWdpbmFsLXRpdGxlXCIpID09IFwi5rOV5ZyL5L6d6KaW6Lev5YWo5pa55L2N6KaW6Ka65qqi5ris57O757WxXCIpIHtcbiAgICAgICQoZ2V0VG9vdGlwc0lkKS5maW5kKCcudG9vbHRpcC1pbm5lcicpLmh0bWwoXCLms5XlnIvkvp3oppbot688YnI+5YWo5pa55L2N6KaW6Ka65qqi5ris57O757WxXCIpO1xuICAgIH1cbiAgfSlcbn1cblxuZnVuY3Rpb24gYW9zKCkge1xuICBpZiAoZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIltkYXRhLWFvc11cIikpIHtcbiAgICBBT1MuaW5pdCh7XG4gICAgICBvbmNlOiB0cnVlLFxuICAgIH0pO1xuICB9XG59XG5cbmZ1bmN0aW9uIHl0SGFuZGxlcigpIHtcbiAgaWYgKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJbZGF0YS11cmxdXCIpKSB7XG4gICAgdmFyIHRyaWdnZXJzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbChcIltkYXRhLXVybF1cIik7XG4gICAgdmFyIHRhcmdldCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJpZnJhbWVcIik7XG5cbiAgICBBcnJheS5wcm90b3R5cGUuc2xpY2UuY2FsbCh0cmlnZ2VycykuZm9yRWFjaChmdW5jdGlvbiAodHJpZ2dlcikge1xuICAgICAgdHJpZ2dlci5vbmNsaWNrID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB0YXJnZXQuc2V0QXR0cmlidXRlKFwic3JjXCIsIHRyaWdnZXIuZ2V0QXR0cmlidXRlKFwiZGF0YS11cmxcIikpO1xuICAgICAgICBhbGVydCh0cmlnZ2VyLmdldEF0dHJpYnV0ZShcImRhdGEtdXJsXCIpKTtcbiAgICAgICAgdHJpZ2dlci5jbGFzc0xpc3QuYWRkKFwianMtYWN0aXZlXCIpO1xuXG4gICAgICAgIEFycmF5LnByb3RvdHlwZS5zbGljZVxuICAgICAgICAgIC5jYWxsKHRyaWdnZXJzKVxuICAgICAgICAgIC5maWx0ZXIoZnVuY3Rpb24gKGl0ZW0pIHtcbiAgICAgICAgICAgIHJldHVybiBpdGVtICE9PSB0cmlnZ2VyO1xuICAgICAgICAgIH0pXG4gICAgICAgICAgLmZvckVhY2goZnVuY3Rpb24gKGl0ZW0pIHtcbiAgICAgICAgICAgIGl0ZW0uY2xhc3NMaXN0LnJlbW92ZShcImpzLWFjdGl2ZVwiKTtcbiAgICAgICAgICB9KTtcbiAgICAgIH07XG4gICAgfSk7XG4gIH1cbn1cblxuZnVuY3Rpb24gY2FyZEp1c3RpZnlDb250ZW50KCkge1xuICB2YXIgZWwgPSAkKFwiLmpzLWNhcmRKdXN0aWZ5Q29udGVudFwiKTtcbiAgaWYgKGVsKSB7XG4gICAgZm9yICh2YXIgaSA9IDA7IGkgPCBlbC5sZW5ndGg7IGkrKykge1xuICAgICAgdmFyIGVsQ2hpbGRyZW5MZW5ndGggPSBlbC5lcShpKS5jaGlsZHJlbignZGl2JykubGVuZ3RoO1xuICAgICAgLy8gY29uc29sZS5sb2coZWxDaGlsZHJlbkxlbmd0aCk7XG4gICAgICBpZiAoZWxDaGlsZHJlbkxlbmd0aCA8PSAyKSB7XG4gICAgICAgIGVsLmVxKGkpLmFkZENsYXNzKFwianVzdGlmeS1jb250ZW50LWNlbnRlclwiKTtcbiAgICAgIH0gZWxzZSB7XG4gICAgICAgIGVsLmVxKGkpLnJlbW92ZUNsYXNzKFwianVzdGlmeS1jb250ZW50LWNlbnRlclwiKTtcbiAgICAgIH1cbiAgICB9XG4gIH1cbn1cblxuZnVuY3Rpb24gc3RvcmVGaWx0ZXJOb3RpZmljYXRpb24oaW5wdXRDb250YWluZXIsIHRhcmdldEVsKSB7XG4gIHZhciBlbCA9IGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoaW5wdXRDb250YWluZXIpO1xuICB2YXIgdGFyZ2V0ID0gZG9jdW1lbnQucXVlcnlTZWxlY3Rvcih0YXJnZXRFbCk7XG4gIGlmIChlbCkge1xuICAgIC8vIGNvbnNvbGUubG9nKGlucHV0Q29udGFpbmVyICsgXCIgKyBcIiArIHRhcmdldCk7XG4gICAgdmFyIHRyaWdnZXJzID0gZWwucXVlcnlTZWxlY3RvckFsbChcImlucHV0W3R5cGU9J2NoZWNrYm94J11cIik7XG4gICAgLy8gY29uc29sZS5sb2codHJpZ2dlcnMpO1xuICAgIEFycmF5LnByb3RvdHlwZS5zbGljZS5jYWxsKHRyaWdnZXJzKS5mb3JFYWNoKGZ1bmN0aW9uICh0cmlnZ2VyKSB7XG4gICAgICB0cmlnZ2VyLmFkZEV2ZW50TGlzdGVuZXIoXCJjbGlja1wiLCBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBjaGVja2VkTnVtID0gZWwucXVlcnlTZWxlY3RvckFsbChcImlucHV0W3R5cGU9Y2hlY2tib3hdOmNoZWNrZWRcIikubGVuZ3RoO1xuICAgICAgICAvLyBjb25zb2xlLmxvZyhjaGVja2VkTnVtKTtcbiAgICAgICAgaWYgKGNoZWNrZWROdW0gPiAwKSB7XG4gICAgICAgICAgdGFyZ2V0LmNsYXNzTGlzdC5hZGQoXCJqcy1pbnB1dENoZWNrZWRcIik7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgdGFyZ2V0LmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1pbnB1dENoZWNrZWRcIik7XG4gICAgICAgIH1cbiAgICAgIH0pO1xuICAgIH0pO1xuICAgIHZhciBjbGVhckFsbEJ0bkVsID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIiNqcy1jbGVhckNoZWNrQm94ZXNcIik7XG4gICAgY2xlYXJBbGxCdG5FbC5hZGRFdmVudExpc3RlbmVyKFwiY2xpY2tcIiwgZnVuY3Rpb24gKCkge1xuICAgICAgdGFyZ2V0LmNsYXNzTGlzdC5yZW1vdmUoXCJqcy1pbnB1dENoZWNrZWRcIik7XG4gICAgfSk7XG4gIH1cbn1cbi8v5ru+6Lu45byP5pel5pyf6YG45ZauXG5mdW5jdGlvbiBkYXRlTW9iaXNjcm9sbCgpIHtcbiAgaWYgKCQoJyNkYXRlJykpIHtcbiAgICAkKCcjZGF0ZScpLm1vYmlzY3JvbGwoKS5kYXRlKHtcbiAgICAgIHRoZW1lOiAkLm1vYmlzY3JvbGwuZGVmYXVsdHMudGhlbWUsIC8vIFNwZWNpZnkgdGhlbWUgbGlrZTogdGhlbWU6ICdpb3MnIG9yIG9taXQgc2V0dGluZyB0byB1c2UgZGVmYXVsdCBcbiAgICAgIG1vZGU6ICdtaXhlZCcsIC8vIFNwZWNpZnkgc2Nyb2xsZXIgbW9kZSBsaWtlOiBtb2RlOiAnbWl4ZWQnIG9yIG9taXQgc2V0dGluZyB0byB1c2UgZGVmYXVsdCBcbiAgICAgIGRpc3BsYXk6ICdtb2RhbCcsIC8vIFNwZWNpZnkgZGlzcGxheSBtb2RlIGxpa2U6IGRpc3BsYXk6ICdib3R0b20nIG9yIG9taXQgc2V0dGluZyB0byB1c2UgZGVmYXVsdCBcbiAgICAgIGxhbmc6ICd6aCcgLy8gU3BlY2lmeSBsYW5ndWFnZSBsaWtlOiBsYW5nOiAncGwnIG9yIG9taXQgc2V0dGluZyB0byB1c2UgZGVmYXVsdCBcbiAgICB9KTtcbiAgfVxuXG59XG4vL+S4i+aLiemBuOWWrui3s+i9iei2hemAo+e1kFxuZnVuY3Rpb24gc2VsZWN0VVJMKCkge1xuICB2YXIgZWwgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLmpzLXNlbGVjdFVSTFwiKTtcbiAgaWYgKGVsKSB7XG4gICAgZWwuYWRkRXZlbnRMaXN0ZW5lcihcImNoYW5nZVwiLCBmdW5jdGlvbiAoKSB7XG4gICAgICAvLyB3aW5kb3cub3Blbih0aGlzLm9wdGlvbnNbdGhpcy5zZWxlY3RlZEluZGV4XS52YWx1ZSk7XG4gICAgICBsb2NhdGlvbi5ocmVmID0gdGhpcy5vcHRpb25zW3RoaXMuc2VsZWN0ZWRJbmRleF0udmFsdWU7XG4gICAgICAvLyBjb25zb2xlLmxvZyh0aGlzLm9wdGlvbnNbdGhpcy5zZWxlY3RlZEluZGV4XS52YWx1ZSk7XG4gICAgfSk7XG4gIH1cbn1cbi8v5ZG85Y+rZnVuY3Rpb24t57ay6aCB6LyJ5YWl5a6M5oiQ5b6MXG4kKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7XG4gIHNlbGVjdFVSTCgpO1xuICB0b29sc0xpc3RlbmVyKCk7XG4gIGNyZWF0ZVNsaWNrcygpO1xuICB0YWJCdG5TbGljaygpO1xuICB0YWJCdG5TbGlja0ZpeGVkKCk7XG4gIC8v5pyD5ZOh5qyK55uK5bCI55SoXG4gIHRhYkJ0blNsaWNrTWVtYmVycygpO1xuICB0YWJCdG5TbGlja0ZpeGVkTWVtYmVycygpO1xuICByaW9uZXRTbGljaygpO1xuICB3YXJyYW50eUxpbmsoKTtcbiAgbGF6eUxvYWQoKTtcbiAgY3JlYXRlQWNjb3JkaW9uKCk7XG4gIC8vIGNyZWF0ZVZpZGVvU3dpcGVyKCk7XG4gIGFvcygpO1xuICB5dEhhbmRsZXIoKTtcbiAgdG9nZ2xlcEZpbHRlclN1Ym1lbnUoKTtcbiAgY2xlYXJDaGVja0JveChcIiNqcy1jbGVhckNoZWNrQm94ZXNcIiwgXCJpbnB1dFt0eXBlPSdjaGVja2JveCddXCIpO1xuICBzZXRTdG9yZVRhYmxlSGVpZ2h0QXRNb2JpbGUoKTtcbiAgLy8gJCgnW2RhdGEtdG9nZ2xlPVwicG9wb3ZlclwiXScpLnBvcG92ZXIoKTtcbiAgdG9vbFRpcHMoKTtcbiAgY2FyZEp1c3RpZnlDb250ZW50KCk7XG4gIHN0b3JlRmlsdGVyTm90aWZpY2F0aW9uKFwiLnYtZHJvcGRvd24tbWVudVwiLCBcIi52LWRyb3Bkb3duLWJ0blwiKTtcbiAgc3RvcmVGaWx0ZXJOb3RpZmljYXRpb24oXCIucC1wcm9kdWN0cy1zZWFyY2hcIiwgXCIucC1wcm9kdWN0cy1zZWFyY2gtYnRuXCIpO1xuICB0b2dnbGVTdG9yZVRhYmxlSGVpZ2h0QXRNb2JpbGUoKTtcbiAgZGF0ZU1vYmlzY3JvbGwoKTtcbiAgZ29Ub0FuY2hvcigpO1xuICAvLyBnb1RvcFN0aWNreSgpO1xufSk7XG53aW5kb3cub25sb2FkID0gZnVuY3Rpb24gKCkge1xuICBjYXJkQ29udGVudEZ1bmN0aW9uKCk7XG4gIHNob3dBbGxUYWIoKTtcbn07XG4vL+WRvOWPq2Z1bmN0aW9uLeimlueql+Wkp+Wwj+iuiuabtFxuJCh3aW5kb3cpLnJlc2l6ZShmdW5jdGlvbiAoKSB7XG4gIGNhcmRDb250ZW50RnVuY3Rpb24oKTtcbiAgc2hvd0FsbFRhYigpO1xuICAvLyBzZXRTdG9yZVRhYmxlSGVpZ2h0QXRNb2JpbGUoKTtcbiAgY29uZmlybUlmRG91YmxlTWVudU9wZW5lZChcIi5wLXByb2R1Y3RzLXNlYXJjaFwiLCA5OTIpO1xuICB0YWJCdG5TbGlja0ZpeGVkKCk7XG4gIC8vIGdvVG9BbmNob3IoKTtcbn0pO1xuLy/lkbzlj6tmdW5jdGlvbi3mjbLli5VcbiQod2luZG93KS5zY3JvbGwoZnVuY3Rpb24gKCkge1xuICBnb1RvcFN0aWNreSgpO1xuICAvLyBzaWRlTGlua1N0aWNreSgpO1xufSk7Il0sInNvdXJjZVJvb3QiOiIifQ==