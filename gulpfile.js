const gulp = require("gulp");
const livereload = require("gulp-livereload");
const autoprefixer = require("autoprefixer");
const plumber = require("gulp-plumber");
const sourcemaps = require("gulp-sourcemaps");
const sass = require("gulp-sass");
const imagemin = require("gulp-imagemin");
const sriHash = require("gulp-sri-hash");
const gulpif = require("gulp-if");
const clean = require("gulp-clean");
const cleanCSS = require("gulp-clean-css");
const postcss = require("gulp-postcss");
const purgecss = require("postcss-purgecss");
const webpack = require("webpack-stream");

// 開發環境設定
const minimist = require("minimist");

const envOptions = {
  string: "env",
  default: { env: "develop" },
};

const options = minimist(process.argv.slice(2), envOptions);
const ENV = {
  production: "zip",
  backend: "unzip",
  security: "weak",
};

//根目錄
const SOURCE_ROOT_PATH = "src@4.0/";
let DEST_ROOT_PATH = "public/";
const destOptions = {
  dev: "public/",
  production: "dist/",
};
const destCheck = () => {
  if (
    options.env === ENV.security ||
    options.env === ENV.production ||
    options.env === ENV.backend
  ) {
    DEST_ROOT_PATH = destOptions.production;
  }
};

// 目標位置
const HTML_PATH = SOURCE_ROOT_PATH + "**/*.html";
const PLUGINS_PATH = SOURCE_ROOT_PATH + "assets/plugins/**/*";
const BOOTSTRAP_PATH = SOURCE_ROOT_PATH + "assets/plugins/bootstrap@4.5.3/";
//all third party plugins
const STYLES_PATH = SOURCE_ROOT_PATH + "assets/sass/**/*.scss";
//css
const SCRIPTS_PATH = SOURCE_ROOT_PATH + "assets/js/**/*.js";
//js
const IMAGES_PATH = SOURCE_ROOT_PATH + "assets/img/**/*";

//GULP TASKS

//HTMLs複製
gulp.task("htmls", () => {
  console.log("Starting HTMLs Task");
  destCheck();
  const HTML_DEST_PATH = `${DEST_ROOT_PATH}`;
  return gulp.src(HTML_PATH).pipe(gulp.dest(HTML_DEST_PATH));
});

// 新增SRI HASH
gulp.task("sri", () => {
  console.log("Starting SRI HASH Task");
  destCheck();
  const HTML_DEST_PATH = `${DEST_ROOT_PATH}`;
  return gulp
    .src(HTML_PATH)
    .pipe(gulp.dest(HTML_DEST_PATH))
    .pipe(gulpif(options.env === ENV.security, sriHash({ algo: "sha384" })))
    .pipe(gulp.dest(HTML_DEST_PATH));
});

// Bootstrap minify
gulp.task("BSminify", () => {
  console.log("Starting Bootstrap Minify Task");
  destCheck();
  const BOOTSTRAP_DEST_PATH = `${DEST_ROOT_PATH}assets/plugins/bootstrap@4.5.3/`;
  const HTML_DEST_PATH = `${DEST_ROOT_PATH}`;
  return gulp
    .src(BOOTSTRAP_DEST_PATH + "**/*.css")
    .pipe(sourcemaps.init({ loadMaps: true }))
    .pipe(
      postcss([
        purgecss({
          content: [
            HTML_DEST_PATH + "**/*.html",
            BOOTSTRAP_DEST_PATH + "**/*.js",
          ],
        }),
      ])
    )
    .pipe(sourcemaps.write("."))
    .pipe(gulp.dest(BOOTSTRAP_DEST_PATH));
});

//Plugins
gulp.task("plugins", () => {
  console.log("Starting Plugins Task");
  destCheck();
  const PLUGINS_DEST_PATH = `${DEST_ROOT_PATH}assets/plugins/`;
  return gulp.src(PLUGINS_PATH).pipe(gulp.dest(PLUGINS_DEST_PATH));
});

//Style for SCSS
gulp.task("style", () => {
  console.log("Starting SCSS Style Task");
  destCheck();
  const HTML_DEST_PATH = `${DEST_ROOT_PATH}`;
  const STYLES_DEST_PATH = `${DEST_ROOT_PATH}assets/css/`;
  const SCRIPTS_DEST_PATH = `${DEST_ROOT_PATH}assets/js/`;
  const PLUGINS_DEST_PATH = `${DEST_ROOT_PATH}assets/plugins/`;
  let sourcemapMode = null;
  if (
    options.env === ENV.security ||
    options.env === ENV.production ||
    options.env === ENV.backend
  ) {
    sourcemapMode = ".";
  }
  return gulp
    .src(STYLES_PATH)
    .pipe(
      plumber(function (err) {
        console.log("Style Task Error");
        console.log(err);
        this.emit("end");
      })
    )

    .pipe(sourcemaps.init())
    .pipe(sass().on("error", sass.logError))
    .pipe(postcss([autoprefixer({ grid: "autoreplace" })]))
    .pipe(gulp.dest(STYLES_DEST_PATH))
    .pipe(
      gulpif(
        options.env === ENV.production || options.env === ENV.security,
        cleanCSS()
      )
    )
    .pipe(
      gulpif(
        options.env === ENV.production ||
          options.env === ENV.security ||
          options.env === ENV.backend,
        postcss([
          purgecss({
            content: [
              HTML_DEST_PATH + "**/*.html",
              SCRIPTS_DEST_PATH + "**/*.js",
              PLUGINS_DEST_PATH + "**/*.js",
            ],
            whitelistPatterns: [/js-/],
          }),
        ])
      )
    )
    .pipe(sourcemaps.write(sourcemapMode))
    .pipe(gulp.dest(STYLES_DEST_PATH));
});

//favicon
gulp.task("favicon", () => {
  console.log("Starting Favicon Task");
  destCheck();
  return gulp.src(`${SOURCE_ROOT_PATH}*.ico`).pipe(gulp.dest(DEST_ROOT_PATH));
});

//Script ES6語法 let const arrow function classes
gulp.task("script", () => {
  console.log("Starting Script Task");
  destCheck();
  const SCRIPTS_DEST_PATH = `${DEST_ROOT_PATH}assets/js/`;
  let sourcemapMode = "inline-source-map";
  let webpackMode = "development";
  if (options.env === ENV.security || options.env === ENV.production) {
    sourcemapMode = "source-map";
    webpackMode = "production";
  } else if (options.env === ENV.backend) {
    sourcemapMode = "source-map";
  }
  return gulp
    .src(SCRIPTS_PATH)
    .pipe(
      webpack({
        mode: webpackMode,
        devtool: sourcemapMode,
        output: {
          filename: "main.js",
        },
        module: {
          rules: [
            {
              test: /\.js$/,
              exclude: /node_modules/,
              use: {
                loader: "babel-loader",
                options: {
                  presets: ["@babel/preset-env"],
                  plugins: ["@babel/plugin-transform-runtime"],
                },
              },
            },
          ],
        },
      })
    )
    .pipe(gulp.dest(SCRIPTS_DEST_PATH));
});

//Images
gulp.task("image-min", () => {
  console.log("Starting Image Min Task");
  destCheck();
  const IMAGES_DEST_PATH = `${DEST_ROOT_PATH}assets/img/`;
  return (
    gulp
      .src(IMAGES_PATH)
      //照片壓縮
      .pipe(
        gulpif(
          options.env === ENV.production ||
            options.env === ENV.security ||
            options.env === ENV.backend,
          imagemin()
        )
      )
      .pipe(gulp.dest(IMAGES_DEST_PATH))
  );
});

// 清除dist
gulp.task("clean", () => {
  console.log("Starting Clean Task");
  return gulp
    .src(["./.tmp", "./dist"], { read: false, allowEmpty: true })
    .pipe(clean());
});

gulp.task(
  "default",
  gulp.series(
    function start(done) {
      // Build the website.
      console.log("Starting Default Task");
      done();
    },
    gulp.parallel("plugins", "script", "image-min", "favicon", "htmls"),
    "style",
    function watch(done) {
      console.log("Starting watch Task");
      livereload.listen();
      gulp.watch(HTML_PATH, gulp.series("htmls"));
      gulp.watch(STYLES_PATH, gulp.series("style"));
      gulp.watch(PLUGINS_PATH, gulp.series("plugins"));
      gulp.watch(SCRIPTS_PATH, gulp.series("script"));
      gulp.watch(IMAGES_PATH, gulp.series("image-min"));
      done();
    }
  )
);

gulp.task(
  "build",
  gulp.series(
    function start(done) {
      // Build the website.
      console.log("Starting Build Task");
      done();
    },
    "clean",
    gulp.parallel("plugins", "script", "image-min", "favicon", "htmls"),
    "style",
    "BSminify",
    "sri"
  )
);
